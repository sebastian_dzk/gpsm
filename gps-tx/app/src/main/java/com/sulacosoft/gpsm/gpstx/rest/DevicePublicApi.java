package com.sulacosoft.gpsm.gpstx.rest;



import com.sulacosoft.gpsm.gpstx.json.DeviceApkPositionJson;
import com.sulacosoft.gpsm.gpstx.json.DeviceLogJson;
import com.sulacosoft.gpsm.gpstx.json.DeviceStatusJson;
import com.sulacosoft.gpsm.gpstx.json.DeviceUidJson;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

// https://code.tutsplus.com/tutorials/sending-data-with-retrofit-2-http-client-for-android--cms-27845

public interface DevicePublicApi {

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST("get-status")
    Call<DeviceStatusJson> getStatus(@Body DeviceUidJson apkDeviceIdJson);

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST("add-log")
    Call<Void> addLog(@Body DeviceLogJson deviceLogJson);

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST("add-position")
    Call<Void> addPosition(@Body DeviceApkPositionJson gpsPositionJson);

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST("send-alarm")
    Call<Void> sendAlarm(@Body DeviceApkPositionJson gpsPositionJson);

}
