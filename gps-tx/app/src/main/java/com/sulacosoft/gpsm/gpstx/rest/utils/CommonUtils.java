package com.sulacosoft.gpsm.gpstx.rest.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.telephony.TelephonyManager;

public class CommonUtils {

    @SuppressLint("MissingPermission")
    public static String getDeviceUid(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        return telephonyManager.getImei();
    }

    public static String getDeviceUidForUser(Context context) {
        String uid[] = getDeviceUid(context).split("-");
        return uid[uid.length - 1];
    }

    /*
    private static void saveParam(Context context, String key, String value) {
        SharedPreferences.Editor edit = getSettings(context).edit();
        edit.putString(key, value);
        edit.apply();
    }

    private static String readParam(Context context, String key) {
        return getSettings(context).getString(key, null);
    }

    private static String getRandomUid() {
        return UUID.randomUUID().toString();
    }


    private static SharedPreferences getSettings(Context context) {
        return context.getSharedPreferences("gpsm", Context.MODE_PRIVATE);
    }
    */
}
