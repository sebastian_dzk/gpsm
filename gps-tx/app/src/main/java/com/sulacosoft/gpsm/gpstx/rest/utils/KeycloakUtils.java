package com.sulacosoft.gpsm.gpstx.rest.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sulacosoft.gpsm.gpstx.Config;
import com.sulacosoft.gpsm.gpstx.json.KeycloakTokenResponseJson;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class KeycloakUtils {

    public static KeycloakTokenResponseJson authenticate(String login, String password) throws Exception {
        String url = String.format("%sauth/realms/%s/protocol/openid-connect/token", Config.getKeycloakUrl(), Config.getKeycloakRealm());
        String data = String.format("username=%s&password=%s&grant_type=password&client_id=%s&grant_type=%s", login, password, Config.getKeycloakClientId(), "password");
        String response = sendPost(url, data);
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(response, KeycloakTokenResponseJson.class);
    }

    private static String sendPost(String url, String data) throws Exception {
        URL obj = new URL(url);
        HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());

        if (data != null)
            wr.writeBytes(data);

        wr.flush();
        wr.close();

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        //System.out.println(response);
        in.close();

        return response.toString();
    }

}
