package com.sulacosoft.gpsm.gpstx;

public class Config {

    public final static String VERSION = "v0.1.22";

    public final static String TAG = "gpsm";

    public final static String getKeycloakUrl() {
        return "https://pilnuj.com/";
    }

    public final static String getKeycloakRealm() {
        return "pilnuj.com";
    }

    public final static String getKeycloakClientId() {
        return "application-cli";
    }
}
