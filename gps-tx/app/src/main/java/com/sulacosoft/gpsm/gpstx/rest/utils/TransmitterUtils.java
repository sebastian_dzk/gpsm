package com.sulacosoft.gpsm.gpstx.rest.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.location.Location;
import android.os.Build;
import android.util.Log;

import com.sulacosoft.gpsm.gpstx.Config;
import com.sulacosoft.gpsm.gpstx.json.BindDeviceRequestJson;
import com.sulacosoft.gpsm.gpstx.json.DeviceApkPositionJson;
import com.sulacosoft.gpsm.gpstx.json.DeviceLogJson;
import com.sulacosoft.gpsm.gpstx.json.GpsPointJson;
import com.sulacosoft.gpsm.gpstx.json.KeycloakTokenResponseJson;
import com.sulacosoft.gpsm.gpstx.rest.DevicePrivateApi;
import com.sulacosoft.gpsm.gpstx.rest.DevicePublicApi;
import com.sulacosoft.gpsm.gpstx.rest.RestApiFactory;

import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TransmitterUtils {

    public static void bindDevice(Activity activity, String login, String password) throws Exception {
        KeycloakTokenResponseJson keycloakResponse = KeycloakUtils.authenticate(login, password);
        DevicePrivateApi api = RestApiFactory.createPrivateApi(keycloakResponse.getAccessToken());

        BindDeviceRequestJson request = new BindDeviceRequestJson();
        request.setUid(CommonUtils.getDeviceUid(activity).intern());
        request.setName(getBuildManufacturer() + " " + getBuildModel());
        request.setBuildDevice(getBuildDevice());
        request.setBuildModel(getBuildModel());
        request.setBuildBoard(getBuildBoard());
        request.setBuildBrand(getBuildBrand());
        request.setBuildDisplay(getBuildDisplay());
        request.setBuildHardware(getBuildHardware());
        request.setBuildManufacturer(getBuildManufacturer());
        request.setBuildProduct(getBuildProduct());
        request.setBuildType(getBuildType());

        Call<Void> response = api.bindDevice(request);

        response.enqueue(new Callback<Void>() {

            private Activity activity;

            public Callback<Void> init(Activity activity) {
                this.activity = activity;
                return this;
            }

            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.i("gpsm", "Device added to server OK");
                //TODO tu startujemy serwis
                //GpsServiceFacade.start(activity);
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setMessage("Wystąpił błąd podczas łączenia urządzenia z kontem. Jeżeli błąd się powtarza skontaktuj się z administratorem serwisu pilnuj.com. Aplikacja zostanie zamknięta.")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //TODO activity.finishAndRemoveTask();
                            }
                        });
                AlertDialog alert = builder.create();
            }
        }.init(activity));
    }

    public static void addLog(Context context, String message) {
        message = Config.VERSION + " " + message;
        DevicePublicApi api = getPublicApi();
        DeviceLogJson request = new DeviceLogJson();
        request.setUid(CommonUtils.getDeviceUid(context));
        request.setMessage(message);
        Call<Void> response = api.addLog(request);

        response.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
            }
        });
    }

    private static DevicePublicApi publicApi;

    private static DevicePublicApi getPublicApi() {
        if (publicApi == null)
            publicApi = RestApiFactory.createPublicApi();

        return publicApi;
    }

    private static String getBuildDevice() {
        return Build.DEVICE;
    }

    private static String getBuildModel() {
        return Build.MODEL;
    }

    private static String getBuildBoard() {
        return Build.BOARD;
    }

    private static String getBuildBrand() {
        return Build.BRAND;
    }

    private static String getBuildDisplay() {
        return Build.DISPLAY;
    }

    private static String getBuildHardware() {
        return Build.HARDWARE;
    }

    private static String getBuildManufacturer() {
        return Build.MANUFACTURER;
    }

    private static String getBuildProduct() {
        return Build.PRODUCT;
    }

    private static String getBuildType() {
        return Build.TYPE;
    }

    public static void sendLoaction(Context context, Location location, int batteryLevel) {
        DeviceApkPositionJson request = new DeviceApkPositionJson();
        request.setDeviceUid(CommonUtils.getDeviceUid(context));
        request.setMeasureDate(new Date());
        request.setHeight(location.getAltitude());
        request.setVelocity(Double.valueOf(3.6*location.getSpeed()));
        GpsPointJson gpsPoint = new GpsPointJson();
        gpsPoint.setLatitude(location.getLatitude());
        gpsPoint.setLongitude(location.getLongitude());
        request.setGpsPoint(gpsPoint);
        request.setLatitude(location.getLatitude());
        request.setLongitude(location.getLongitude());
        request.setBatteryLevel(batteryLevel);
        request.setAlarm(false);

        getPublicApi().addPosition(request);

        //final GpsPositionMsg msg = new GpsPositionMsg();
        //msg.setDeviceApkPositionJson(request);

        Call<Void> response = getPublicApi().addPosition(request);
        response.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                //msg.setStatus("OK");
                //sendMessage(msg);
                //IconNotificationUtils.showEyeOn(GpsService.this);
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                //msg.setStatus("FAIL");
                //sendMessage(msg);
                //IconNotificationUtils.showEyeOff(GpsService.this);
            }
        });
    }
}
