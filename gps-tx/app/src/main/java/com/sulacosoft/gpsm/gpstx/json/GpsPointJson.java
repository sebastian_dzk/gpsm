package com.sulacosoft.gpsm.gpstx.json;

public class GpsPointJson {

    private double longitude;
    private double latitude;

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    @Override
    public String toString() {
        return "GpsPointJson{" +
                "longitude=" + longitude +
                ", latitude=" + latitude +
                '}';
    }
}
