package com.sulacosoft.gpsm.gpstx.json;

public class DeviceStatusJson {

    private boolean deviceExist;

    public boolean isDeviceExist() {
        return deviceExist;
    }

    public void setDeviceExist(boolean deviceExist) {
        this.deviceExist = deviceExist;
    }

}
