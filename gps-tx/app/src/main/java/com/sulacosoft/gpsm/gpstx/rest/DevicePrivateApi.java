package com.sulacosoft.gpsm.gpstx.rest;


import com.sulacosoft.gpsm.gpstx.json.BindDeviceRequestJson;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface DevicePrivateApi {

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST("bind-device")
    Call<Void> bindDevice(@Body BindDeviceRequestJson request);

}
