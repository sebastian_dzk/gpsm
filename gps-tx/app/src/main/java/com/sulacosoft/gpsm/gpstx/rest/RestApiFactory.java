package com.sulacosoft.gpsm.gpstx.rest;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestApiFactory {

    private final static boolean DEBUG_HTTP = false; // Ustawić na true w razie potrzeby debugowania

    private final static String REST_API_URL = "https://pilnuj.com/app/api/device/apk/";

    public static DevicePublicApi createPublicApi() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        OkHttpClient client = createOkHttpClient(httpClient);
        Retrofit builder = createRetrofit(client);
        return builder.create(DevicePublicApi.class);
    }

    public static DevicePrivateApi createPrivateApi(final String authToken) {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        // https://stackoverflow.com/questions/32605711/adding-header-to-all-request-with-retrofit-2
        httpClient.addNetworkInterceptor(new Interceptor() {
            @Override
            public Response intercept(Interceptor.Chain chain) throws IOException {
                Request.Builder builder = chain.request().newBuilder();
                builder.addHeader("Authorization", "Bearer " + authToken);
                return chain.proceed(builder.build());
            }
        });

        OkHttpClient client = createOkHttpClient(httpClient);

        Retrofit builder = createRetrofit(client);
        return builder.create(DevicePrivateApi.class);
    }

    private static Retrofit createRetrofit(OkHttpClient client) {
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();

        return new Retrofit.Builder()
                .baseUrl(REST_API_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
    }

    private static OkHttpClient createOkHttpClient(OkHttpClient.Builder httpClient) {
        if (DEBUG_HTTP) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            return httpClient
                    .addInterceptor(logging)
                    .build();
        } else {
            return httpClient.build();
        }
    }
}
