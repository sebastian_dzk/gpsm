package com.sulacosoft.gpsm.gpstx.json;


public class DeviceUidJson {

    private String uid;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

}
