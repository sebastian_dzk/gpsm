package com.sulacosoft.gpsm.gpstx.json;

public class BindDeviceRequestJson {
    private String name;
    private String uid;
    private String buildDevice;
    private String buildModel;
    private String buildBoard;
    private String buildBrand;
    private String buildDisplay;
    private String buildHardware;
    private String buildManufacturer;
    private String buildProduct;
    private String buildType;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getBuildDevice() {
        return buildDevice;
    }

    public void setBuildDevice(String buildDevice) {
        this.buildDevice = buildDevice;
    }

    public String getBuildModel() {
        return buildModel;
    }

    public void setBuildModel(String buildModel) {
        this.buildModel = buildModel;
    }

    public String getBuildBoard() {
        return buildBoard;
    }

    public void setBuildBoard(String buildBoard) {
        this.buildBoard = buildBoard;
    }

    public String getBuildBrand() {
        return buildBrand;
    }

    public void setBuildBrand(String buildBrand) {
        this.buildBrand = buildBrand;
    }

    public String getBuildDisplay() {
        return buildDisplay;
    }

    public void setBuildDisplay(String buildDisplay) {
        this.buildDisplay = buildDisplay;
    }

    public String getBuildHardware() {
        return buildHardware;
    }

    public void setBuildHardware(String buildHardware) {
        this.buildHardware = buildHardware;
    }

    public String getBuildManufacturer() {
        return buildManufacturer;
    }

    public void setBuildManufacturer(String buildManufacturer) {
        this.buildManufacturer = buildManufacturer;
    }

    public String getBuildProduct() {
        return buildProduct;
    }

    public void setBuildProduct(String buildProduct) {
        this.buildProduct = buildProduct;
    }

    public String getBuildType() {
        return buildType;
    }

    public void setBuildType(String buildType) {
        this.buildType = buildType;
    }
}
