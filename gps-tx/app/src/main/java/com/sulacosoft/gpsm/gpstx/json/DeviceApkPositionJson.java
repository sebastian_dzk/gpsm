package com.sulacosoft.gpsm.gpstx.json;


import java.util.Date;

public class DeviceApkPositionJson {

    private String deviceUid;

    private Date measureDate;

    private double longitude;

    private double latitude;

    private Double velocity, height;

    private Integer batteryLevel;

    private boolean alarm;

    private GpsPointJson gpsPoint;

    public String getDeviceUid() {
        return deviceUid;
    }

    public void setDeviceUid(String deviceUid) {
        this.deviceUid = deviceUid;
    }

    public Date getMeasureDate() {
        return measureDate;
    }

    public void setMeasureDate(Date measureDate) {
        this.measureDate = measureDate;
    }

    public Double getVelocity() {
        return velocity;
    }

    public void setVelocity(Double velocity) {
        this.velocity = velocity;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public GpsPointJson getGpsPoint() {
        return gpsPoint;
    }

    public void setGpsPoint(GpsPointJson gpsPoint) {
        this.gpsPoint = gpsPoint;
    }


    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public Integer getBatteryLevel() {
        return batteryLevel;
    }

    public void setBatteryLevel(Integer batteryLevel) {
        this.batteryLevel = batteryLevel;
    }

    public boolean isAlarm() {
        return alarm;
    }

    public void setAlarm(boolean alarm) {
        this.alarm = alarm;
    }

    @Override
    public String toString() {
        return "DeviceApkPositionJson{" +
                "deviceUid='" + deviceUid + '\'' +
                ", measureDate=" + measureDate +
                ", velocity=" + velocity +
                ", height=" + height +
                ", gpsPoint=" + gpsPoint +
                '}';
    }
}
