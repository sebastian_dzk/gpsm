Zawartość katalogów:
/auth - przykład autoryzacji i odświeżania tokenów przez Keycloak dla REST API CXF
/core - główna cześć serwerowa aplikacji, udostępnia api do komunikacji przez front
/doc - dokumentacja projektu 
/docker-postgres - konfiguracja bazy wykorzystywanej przez modul "core"

