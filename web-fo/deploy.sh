#!/bin/bash

# Read Password
echo -n Password: 
read -s password
echo

echo "############### budowanie WEB-FO ###############"
rm -fr dist/
ng build --base-href /web-fo/
cd dist
tar -zcvf web-fo.tar.gz web-fo
cd ..

echo "############## kopiowanie  ##############"
sshpass -p "$password" scp -r ./dist/web-fo.tar.gz root@pilnuj.com:/var/www/html/

echo "############## uruchamianie #############"
sshpass -p "$password" ssh -t root@pilnuj.com "rm -f -r /var/www/html/web-fo/"
sshpass -p "$password" ssh -t root@pilnuj.com "tar -xvzf /var/www/html/web-fo.tar.gz -C /var/www/html"
sshpass -p "$password" ssh -t root@pilnuj.com "rm -f /var/www/html/web-fo.tar.gz"
echo "############### SUKCES ###############"

