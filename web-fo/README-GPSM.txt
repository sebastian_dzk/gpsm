Uruchomienie aplikacji w trybue developerskim:
ng serve --host 0.0.0.0 --open --proxy-config proxy.conf.json --poll=2000

// https://alligator.io/angular/internationalization/
// https://dzone.com/articles/localization-in-angular-using-i18n-tools

Budowanie paczki:
ng build --base-href /web-fo/


Jak nie działa projekt:
npm install
npm audit fix --force
ng serve


Angular Keycloak:
https://www.npmjs.com/package/keycloak-angular
https://github.com/mauriciovigolo/keycloak-angular

Angular Routing:
https://www.tutorialspoint.com/angular6/angular6_routing.htm

Angular rest client (service):
https://www.djamware.com/post/5b87894280aca74669894414/angular-6-httpclient-consume-restful-api-example

Integracja ngx-leaflet z Angular-cli:
https://asymmetrik.com/ngx-leaflet-tutorial-angular-cli/

Demo onlie dynamicznych punktów:
https://stackblitz.com/edit/angular-ngx-leaflet-tests

Budowanie paczki z dodatkowymi parametrami:
https://stackoverflow.com/questions/37348045/how-to-change-the-dist-folder-path-in-angular-cli-after-ng-build

Błędy 404:
https://stackoverflow.com/questions/35284988/angular-2-404-error-occur-when-i-refresh-through-the-browser


//////////////////////////////////
https://www.syncfusion.com/kb/9968/how-to-create-an-angular-7-datepicker-component
https://stackblitz.com/edit/angular-ngx-leaflet-tests
http://tombatossals.github.io/angular-leaflet-directive/examples/0000-viewer.html#/basic/lf-center-example

https://angular-ui.github.io/ui-leaflet
https://github.com/coryasilva/Leaflet.ExtraMarkers/blob/master/package.json
https://github.com/bhaskarvk/leaflet/blob/master/inst/examples/awesomeMarkers.R

https://offering.solutions/blog/articles/2016/02/01/consuming-a-rest-api-with-angular-http-service-in-typescript/
https://ej2.syncfusion.com/angular/documentation/datepicker/keyboard-interaction/

https://mdbootstrap.com/docs/jquery/modals/basic/
https://www.sitepoint.com/understanding-bootstrap-modals/
https://www.sitepoint.com/css-grid-generators/

dostępne kolory markerów:
blue
green
orange
red
violet
yellow
black
grey

translate:
https://www.codeandweb.com/babeledit/tutorials/how-to-translate-your-angular8-app-with-ngx-translate
https://stackoverflow.com/questions/36914151/browser-language-detection
