import { KeycloakConfig } from 'keycloak-angular';

// Add here your keycloak setup infos
let keycloakConfig: KeycloakConfig = {
  url: 'https://pilnuj.com/auth',
  realm: 'gpsm',
  clientId: 'gpsm-apk'
};

export const environment = {
  production: true,
  keycloak: keycloakConfig,
  serverEndpoint: 'https://pilnuj.com/app/api/'
};
