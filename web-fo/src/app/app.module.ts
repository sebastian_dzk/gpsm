import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { DatePipe } from '@angular/common';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';

import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';

import { KeycloakAngularModule, KeycloakService } from 'keycloak-angular';
import { initializer } from './utils/app-init';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import { DevicesComponent } from './devices/devices.component';
import { MapComponent } from './map/map.component';
import { AccountComponent } from './account/account.component';
import { PaymentComponent } from './payment/payment.component';

import { LeafletModule } from '@asymmetrik/ngx-leaflet';

import { HTMLMarkerComponent } from './map/html-marker.component';
import { DataService } from './map/data.service';
import { AlertModule } from 'ngx-bootstrap';
import { ButtonsModule } from 'ngx-bootstrap';

import { DatePickerModule } from '@syncfusion/ej2-angular-calendars';

import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    DevicesComponent,
    MapComponent,
    AccountComponent,
    PaymentComponent,
    HTMLMarkerComponent,
    AccountComponent
  ],
  imports: [
    BrowserModule,
    DatePickerModule,
    AppRoutingModule,
    KeycloakAngularModule,
    HttpClientModule,
    FormsModule,
    LeafletModule.forRoot(),
    AlertModule.forRoot(),
    ButtonsModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    DataService,
    {
      provide: APP_INITIALIZER,
      useFactory: initializer,
      multi: true,
      deps: [KeycloakService],
    },
    DatePipe,
    {provide: LocationStrategy, useClass: HashLocationStrategy}
  ],
  entryComponents: [HTMLMarkerComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http,  './assets/i18n/', '.json');
}
