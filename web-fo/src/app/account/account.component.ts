import {Injectable, Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
@Injectable()
export class AccountComponent implements OnInit {

  constructor(public rest: RestService) {
  }

  userInfo : any;

  ngOnInit() {
    this.getUserInfo();
  }

  getUserInfo() {
    this.rest.getUserInfo().subscribe((data: {}) => {
      console.log(data);
      this.userInfo = data;
    });
  }
}
