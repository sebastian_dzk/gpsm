import { Injectable } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class GpsmCommonService {

  constructor(private translate: TranslateService) { }

  useLanguage(language: string) {

    this.translate.use(language);
  }

}
