import {Injectable, Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';
import { environment } from '../../environments/environment';


@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
@Injectable()
export class PaymentComponent implements OnInit {

  env = environment;
  links:any = [];

  constructor(public rest: RestService) {
  }

  ngOnInit() {
    this.getLinks();
  }

  getLinks() {
    this.rest.getPaymentLinks().subscribe((data: {}) => {
      console.log(data);
      this.links = data;
    });
  }

}
