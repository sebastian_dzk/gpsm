import { Component, OnInit } from '@angular/core';
import { KeycloakProfile } from 'keycloak-js';
import { KeycloakService } from 'keycloak-angular';
import {GpsmCommonService} from '../gpsm-common.service'

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  userDetails: KeycloakProfile;

  constructor(public gpsmCommon: GpsmCommonService, private keycloakService: KeycloakService) {
  //  route.params.subscribe(params => console.log("side menu id parameter",params['id']));
  }

  async ngOnInit() {
    if (await this.keycloakService.isLoggedIn()) {
      this.userDetails = await this.keycloakService.loadUserProfile();
    }
  }

  async doLogout() {
    await this.keycloakService.logout('https://pilnuj.com/web-fo');
  }

}
