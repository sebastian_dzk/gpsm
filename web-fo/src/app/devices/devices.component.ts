import {Component, OnInit, ViewChild} from '@angular/core';
import { RestService } from '../rest.service';

@Component({
  selector: 'app-devices',
  templateUrl: './devices.component.html',
  styleUrls: ['./devices.component.scss']
})
export class DevicesComponent implements OnInit {

  @ViewChild('inputName') inputName;
  @ViewChild('inputImei') inputImei;
  @ViewChild('addDevicePanel') addDevicePanel;

  devices:any = [];

  constructor(public rest:RestService) { }

  ngOnInit() {
    this.getDevices();
  }

  getDevices() {
    this.devices = [];
    this.rest.getDevices().subscribe((data: {}) => {
      console.log(data);
      this.devices = data;
    });
  }

  addImei()  {
    this.rest.addDevice(this.inputName.nativeElement.value, this.inputImei.nativeElement.value).subscribe((data: {}) => {
      this.inputName.nativeElement.value = '';
      this.inputImei.nativeElement.value = '';
      this.getDevices();
    });
  }

  deleteDevice(id) {
    this.rest.deleteDevice(id).subscribe((data: {}) => {
      this.getDevices();
    });
  }
}
