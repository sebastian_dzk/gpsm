import {Injectable} from '@angular/core';

import {HttpClient, HttpHeaders, HttpErrorResponse} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {map, catchError, tap} from 'rxjs/operators';
import { environment } from '../environments/environment';

const endpoint = environment.serverEndpoint;

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(private http: HttpClient) {
  }

  private extractData(res: Response) {
    let body = res;
    return body || { };
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  getDevices(): Observable<any> {
    return this.http.get(endpoint + 'devices/user-devices/list').pipe(
      map(this.extractData));
  }

  getDevicesLastPositions(): Observable<any> {
    return this.http.get(endpoint + 'devices/user-devices/last-positions').pipe(
      map(this.extractData));
  }

  getPublicDevicesList(): Observable<any> {
    return this.http.get(endpoint + 'public/device/list').pipe(
      map(this.extractData));
  }

  getPaymentLinks(): Observable<any> {
    return this.http.get(endpoint + 'payments').pipe(
      map(this.extractData));
  }

  getUserInfo(): Observable<any> {
    return this.http.get(endpoint + 'user/info').pipe(
      map(this.extractData));
  }

  getPointsByDeviceAndDate(deviceUid, date): Observable<any> {
    return this.http.get(endpoint + '/api/devices/device/' + deviceUid + '/points/' + date).pipe(
      map(this.extractData));
  }

  getDevicePositionsLast(): Observable<any> {
    return this.http.get(endpoint + 'v1/device/positions/last').pipe(
      map(this.extractData));
  }

  getDevicePositions(sinceDate, sinceTime, tillTime): Observable<any> {
    return this.http.get(endpoint + 'v1/device/positions/' + sinceDate + '/000000/235959').pipe(
      map(this.extractData));
  }

  addDevice(name, imei): Observable<any> {
    const options = {headers: {'Content-Type': 'application/json'}};
    return this.http.post(endpoint + 'v1/device', JSON.stringify({'name' : name, 'uid': imei}), options).pipe(
      map(this.extractData));
  }

  deleteDevice(id): Observable<any> {
    return this.http.delete(endpoint + 'v1/device/' + id).pipe(
      map(this.extractData));
  }
}
