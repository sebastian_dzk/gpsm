import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MapComponent } from './map/map.component';
import { DevicesComponent } from './devices/devices.component';
import { AccountComponent } from './account/account.component';
import { PaymentComponent } from './payment/payment.component';

const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forRoot(
   [
      {
        path: 'devices',
        component: DevicesComponent
      },
      {
       path: 'map',
       component: MapComponent
      },
     {
       path: 'account',
       component: AccountComponent
     },
      {
        path: 'payment',
        component: PaymentComponent
      },
      {
        path: '**',
        component: DevicesComponent
      }
    ])
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
