import { TestBed } from '@angular/core/testing';

import { GpsmCommonService } from './gpsm-common.service';

describe('GpsmCommonService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GpsmCommonService = TestBed.get(GpsmCommonService);
    expect(service).toBeTruthy();
  });
});
