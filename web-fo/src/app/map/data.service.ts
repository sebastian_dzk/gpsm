import { Injectable } from '@angular/core';
import { LatLngExpression} from 'leaflet';
import { RestService } from '../rest.service';

export class Marker {
  id: number;
  name: String;
  description: String;
  position:  LatLngExpression;
}

@Injectable()
export class DataService {

  constructor(public rest:RestService) { }

  markers: Marker[] = [];

  getMarkers() {

    this.getDevicesLastPositions();

    return this.markers;
  }

  getMarkerById(id) {
    return this.markers.filter((entry) => entry.id === id)[0];
  }

  changeMarkerData() {
    for(let marker of this.markers) {
      // just add a random number at the end
      marker.description = `Some random value ${Math.random() * 100}`;
    }
  }

  getPointsByDeviceAndDate(deviceUid, date) {
    return this.rest.getPointsByDeviceAndDate(deviceUid, date);
  }

  getDevicesLastPositions() {
    this.rest.getDevicesLastPositions().subscribe((data: {}) => {

      this.markers = [];
      for (let dataKey in data) {
        var entry = data[dataKey];
        var id:number = Number(dataKey);
        var lat:number = Number(entry['lat']);
        var lng:number = Number(entry['lng']);
        var time:any = entry['time'];

        var date = new Date(time);

        this.markers.push({
          id: id,
          name: entry['deviceName'],
          description: 'lat=' + lat + '; lng=' + lng + '; ' + date.getDate() + '/' + (date.getMonth()+1) + '/' + date.getFullYear() + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds(),
          position: [ lat, lng ]
        });
      }
    });
  }
}
