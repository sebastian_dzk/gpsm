import {Component, ComponentFactoryResolver, ComponentRef, DoCheck, EventEmitter, Injector, ViewChild} from '@angular/core';
import { DatePipe } from '@angular/common';
import {tileLayer, latLng, polyline, marker, Marker, Layer, LatLngExpression, MarkerOptions} from 'leaflet';
declare const L:any;

import { RestService } from '../rest.service';

import {HTMLMarkerComponent} from './html-marker.component';
import {DataService} from './data.service';

interface MarkerMetaData {
  name: String;
  markerInstance: Marker;
  componentInstance: ComponentRef<HTMLMarkerComponent>;
}

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})

export class MapComponent implements DoCheck {
  @ViewChild('positionsActualId') positionsActual: any;
  @ViewChild('positionsHistoricalId') positionsHistorical: any;
  @ViewChild('ejDatePicker') datepicker: any;

   streetMaps = tileLayer(
     'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
    //'https://maps.tilehosting.com/styles/streets/{z}/{x}/{y}.png?key=5jjQ1gdY5zIX30C40swM',
    {
      detectRetina: true
    });

  map;
  markers: MarkerMetaData[] = [];
  options = {
    layers: [ this.streetMaps/*, this.route*/],
    zoom: 10,
    center: latLng(0.0, 0.0)
  };

  private centerSet:boolean = false;
  private refreshIntervalId:any;

  public minDate: Date = new Date('05/01/2019');
  public maxDate: Date = new Date();
  public dateValue: Date = new Date();

  constructor(public rest:RestService, private dataService: DataService, private resolver: ComponentFactoryResolver, private injector: Injector, private datepipe: DatePipe) {
  }

  onMapReady(map) {
    this.map = map;
    this.addMarker(this.rest.getDevicePositionsLast());
    this.startAutrorefreshPositions();
  }

  startAutrorefreshPositions() {
    this.addMarker(this.rest.getDevicePositionsLast());
    this.refreshIntervalId = setInterval(() => this.addMarker(this.rest.getDevicePositionsLast()),10000);
  }

  getMarkerIconByColor(color : string) {
    let url = 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-' + color + '.png';
    return  new L.Icon({iconUrl: url});
  }

  removeMarker(marker) {
    // remove it from the array meta objects
    //const idx = this.markers.indexOf(marker);
    //this.markers.splice(idx, 1);

    // remove the marker from the map
    marker.markerInstance.removeFrom(this.map);

    // destroy the component to avoid memory leaks
 //   marker.componentInstance.destroy();
  }

  // This is a lifecycle method of an Angular component which gets invoked whenever for
  // our component change detection is triggered
  ngDoCheck() {
    // since our components are dynamic, we need to manually iterate over them and trigger
    // change detection on them.

    this.markers.forEach(entry => {
      entry.componentInstance.changeDetectorRef.detectChanges();
    });
  }

  onSelectionRadioChange(event: EventEmitter<any>) {
    if (document.getElementById('positionsHistoricalId')['checked'] === true) {
      document.getElementById('datepicker').style.visibility = 'visible';
      clearInterval(this.refreshIntervalId);
      this.showHistoryMarkers();
    } else {
      document.getElementById('datepicker').style.visibility = 'hidden';
      this.startAutrorefreshPositions();
    }
  }

  onDatePickerChange(event: EventEmitter<any> ) {
    this.showHistoryMarkers();
  }

  private toYYYYMMDD(date) {
    let month = date.getMonth() + 1;
    month = '0' + month;
    month = month.substring(month.length-2, month.length);

    let day = date.getDate();
    day = '0' + day;
    day = day.substring(day.length - 2, day.length);

    return date.getFullYear() + '' + month + '' + day;
  }

  private toHHmmss(date) {
    let hours = date.getHours() + 1;
    hours = '0' + hours;
    hours = hours.substring(hours.length-2, hours.length);

    let minutes = date.getMinutes();
    minutes = '0' + minutes;
    minutes = minutes.substring(minutes.length - 2, minutes.length);

    let seconds = date.getSeconds();
    seconds = '0' + seconds;
    seconds = seconds.substring(seconds.length - 2, seconds.length);

    return hours + '' + minutes + '' + seconds;
  }
all
  private showHistoryMarkers() {
    if (this.datepicker.value) {
      let dateString = this.toYYYYMMDD(this.datepicker.value);
      let timeSinceString = this.toHHmmss(this.datepicker.value);
      let timeTillString= this.toHHmmss(new Date(this.datepicker.value.getTime() + 3600000));
      this.addMarker(this.rest.getDevicePositions(dateString, timeSinceString, timeTillString));
    }
  }

  addMarker(devicePositionsPromise) {
    devicePositionsPromise.subscribe(value => {

      if(!this.centerSet) {
        if (!document.getElementById("devices-legend") && value.devices.length !== 0) {
          var legend = L.control({position: 'topleft'});
          legend.onAdd = function (map) {
            var div = L.DomUtil.create('div', 'info legend');
            div.setAttribute('id', 'devices-legend');
            var labels = ['<strong>Devices:</strong>'];
            for (const device of value.devices) {
              div.innerHTML += labels.push('<i style="background:' + device.iconColor + '">&nbsp;</i>' + device.name);
            }
            div.innerHTML = labels.join('<br>');
            return div;
          };
          legend.addTo(this.map);
        }
      }

      if(this.markers) {
        // remove markers
        this.markers.forEach(m => this.removeMarker(m));
      }

      let divMap = document.getElementById('div-map-id');
      for(const device of value.devices) {
        // dynamically instantiate a HTMLMarkerComponent
        const factory = this.resolver.resolveComponentFactory(HTMLMarkerComponent);

        // we need to pass in the dependency injector
        const component = factory.create(this.injector);

        // wire up the @Input() or plain variables (doesn't have to be strictly an @Input())
        component.instance.data = {};

        // we need to manually trigger change detection on our in-memory component
        // s.t. its template syncs with the data we passed in
        component.changeDetectorRef.detectChanges();

        for(const position of device.positions) {

          if(!this.centerSet) {
            this.map.setView(latLng(position.lat, position.lng));
            this.centerSet = true;
          }

          let m : Marker = marker(position, {icon: this.getMarkerIconByColor(device.iconColor)});

          // pass in the HTML from our dynamic component
          const popupContent = component.location.nativeElement;

          // add popup functionality
          m.bindPopup(popupContent).openPopup();

          // finally add the marker to the map s.t. it is visible
          m.addTo(this.map).bindPopup(position.description);

          // add a metadata object into a local array which helps us
          // keep track of the instantiated markers for removing/disposing them later
          this.markers.push({
            name: device.name,
            markerInstance: m,
            componentInstance: component
          });
        }
      }

      if(value.devices.length === 0) {
        this.map.setView(latLng(52.2207622,21.0484505));
        this.map.setZoom(7);
      }

      if(value.success === true) {
        divMap.style.visibility = 'visible';
      }

    });
  }
}
