package com.sulacosoft.gpsm.performance.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


public class Config {

    private static Properties prop = null;

    public static String getKeycloakUrl() {
        return getProperty().getProperty("keycloak.url");
    }

    public static String getKeycloakRealm() {
        return getProperty().getProperty("keycloak.realm");
    }

    public static String getKeycloakClientId() {
        return getProperty().getProperty("keycloak.clientId");
    }

    public static String getKeycloakUser() {
        return getProperty().getProperty("keycloak.user");
    }

    public static String getKeycloakPassword() {
        return getProperty().getProperty("keycloak.password");
    }

    private static Properties getProperty() {
        if(prop==null) {
            try (InputStream in = new FileInputStream("config.properties")) {
                prop = new Properties();
                prop.load(in);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return prop;
    }
}
