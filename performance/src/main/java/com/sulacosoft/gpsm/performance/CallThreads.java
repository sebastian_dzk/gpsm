package com.sulacosoft.gpsm.performance;

import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.util.List;

public class CallThreads extends Thread {

    private List<String> devices;

    public CallThreads(List<String> devices) {
        this.devices = devices;
    }

    public void run() {
        while(true) {
            for (String uid : devices) {
                sendPosition(uid, 21.0 + Math.random(), 51.0 + Math.random(), 5*Math.random(), 123+10*Math.random());
            }
        }
    }

    void sendPosition(String deviceUid, double lng, double lat, double velocity, double height/*, Date time*/) {
        try {

            ClientRequest request = new ClientRequest(
                    "https://pilnuj.com/app/api/device/apk/add-position");
            request.accept("application/json");

            String input = String.format("{\n" +
                    "  \"deviceUid\": \"%s\",\n" +
                    "  \"gpsPoint\": {\n" +
                    "    \"latitude\": %s,\n" +
                    "    \"longitude\": %s\n" +
                    "  },\n" +
                    "  \"height\": %s,\n" +
                    "  \"measureDate\": \"2019-02-11T17:11:59.373Z\",\n" +
                    "  \"velocity\": %s\n" +
                    "}", deviceUid, lng, lat, velocity, height);
            request.body("application/json", input);

            ClientResponse<String> response = request.post(String.class);

        /*    if (response.getStatus() != 201) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatus());
            }*/

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    new ByteArrayInputStream(response.getEntity().getBytes())));

        /*    String output;
            System.out.println("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                System.out.println(output);
            }*/

        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        } catch (Exception e) {

            e.printStackTrace();

        }
    }

}
