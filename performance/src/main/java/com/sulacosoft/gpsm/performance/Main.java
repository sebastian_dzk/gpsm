package com.sulacosoft.gpsm.performance;

import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

// https://howtoprogram.xyz/2016/07/12/java-rest-client-using-resteasy-client/
public class Main {

     public static void main(String args[]) throws Exception {
         System.out.println("##### main #####");

        /* String token = KeycloakUtil.getToken();
         System.out.println("mamy token : " +  token);
         System.out.println("======================================");


         new Main().get(token);*/

       // new Main().populate();

        new Main().populate2();

     }


    /*private static final String URI_BOOK = "https://pilnuj.com/app/api/test/device/list-all-tst";

    private void get(String token) throws Exception  {

        ClientRequest request = new ClientRequest(
                URI_BOOK);
        request.accept("application/json");
        request.header("Authentication", "Bearer " + token);
        
        
        ClientResponse<String> response = request.get(String.class);

        if (response.getStatus() != 200) {
            throw new RuntimeException("Failed : HTTP error code : "
                    + response.getStatus());
        }

        BufferedReader br = new BufferedReader(new InputStreamReader(
                new ByteArrayInputStream(response.getEntity().getBytes())));

        String output;
        System.out.println("Output from Server .... \n");
        while ((output = br.readLine()) != null) {
            System.out.println(output);
        }
    }*/

    private void populate() {
        List<String> devices = loadDevices();
        System.out.println("### > " + devices.size());

        long t0=System.currentTimeMillis();

        int n = 30; //devices.size();

        for( int i=0; i< n; i++ ) {
            //System.out.println("i="+i);
            sendPosition(devices.get(i));
        }

        long t1 = System.currentTimeMillis();

        long dt = t1-t0;
        System.out.println("dt       = " + dt + "ms");
        System.out.println("dt/point = " + (1.0*dt/n) + "ms");

    }


    private void populate2() {
        List<String> devices = loadDevices();

        List<CallThreads> ths = new ArrayList<>();

        for( int i=0; i<1000; i++) {
            CallThreads th = new CallThreads(devices);
            th.start();
            ths.add(th);
            System.out.println("created " + i + " thread");
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    private List<String> loadDevices() {
        List<String> response = new ArrayList<>();
        //read file into stream, try-with-resources
        try (Stream<String> stream = Files.lines(Paths.get("devices_prod.dat"))) {

            //stream.forEach(System.out::println);
            stream.forEach(p -> response.add(p.trim()));


        } catch (IOException e) {
            e.printStackTrace();
        }

        return response;
    }

    void sendPosition(String deviceUid/*, double lng, double lat, double velocity, double height, Date time*/) {
        try {

            ClientRequest request = new ClientRequest(
                    "https://pilnuj.com/app/api/device/apk/add-position");
                   // "http://127.0.0.1:8080/api/device/apk/add-position");
            request.accept("application/json");

            String input = String.format("{\n" +
                    "  \"deviceUid\": \"%s\",\n" +
                    "  \"gpsPoint\": {\n" +
                    "    \"latitude\": 51.0,\n" +
                    "    \"longitude\": 21.0\n" +
                    "  },\n" +
                    "  \"height\": 123,\n" +
                    "  \"measureDate\": \"2019-02-11T17:11:59.373Z\",\n" +
                    "  \"velocity\": 5.0\n" +
                    "}", deviceUid);
            request.body("application/json", input);

            ClientResponse<String> response = request.post(String.class);

        /*    if (response.getStatus() != 201) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatus());
            }*/

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    new ByteArrayInputStream(response.getEntity().getBytes())));

        /*    String output;
            System.out.println("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                System.out.println(output);
            }*/

        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        } catch (Exception e) {

            e.printStackTrace();

        }
    }
}
