package com.sulacosoft.gpsm.performance;

import java.util.List;

public class DevicesJson {

    private List<String> names;

    public List<String> getNames() {
        return names;
    }

    public void setNames(List<String> names) {
        this.names = names;
    }
}
