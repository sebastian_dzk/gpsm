#!/bin/bash

# Read Password
echo -n Password: 
read -s password
echo

echo "############### budowanie CORE ###############" 
./gradlew clean build
jar_file=$(ls ./build/libs/| awk '{print $1;}')
echo "JAR FILE: $jar_file"

echo "############## kopiowanie  ##############"
sshpass -p "$password" scp -r ./build/libs/$jar_file root@pilnuj.com:/opt/gpsm-core/

echo "############## uruchamianie #############"
sshpass -p "$password" ssh -t root@pilnuj.com 'systemctl stop gpsm-core'
sleep 5
sshpass -p "$password" ssh -t root@pilnuj.com 'rm /opt/gpsm-core/core.jar'
sshpass -p "$password" ssh -t root@pilnuj.com "cp /opt/gpsm-core/$jar_file /opt/gpsm-core/core.jar"
sshpass -p "$password" ssh -t root@pilnuj.com 'chmod +x /opt/gpsm-core/core.jar'
sshpass -p "$password" ssh -t root@pilnuj.com "chown gpsm:gpsm /opt/gpsm-core/$jar_file"
sshpass -p "$password" ssh -t root@pilnuj.com 'chown gpsm:gpsm /opt/gpsm-core/core.jar'
sshpass -p "$password" ssh -t root@pilnuj.com 'systemctl start gpsm-core'
echo "############### SUKCES ###############"

