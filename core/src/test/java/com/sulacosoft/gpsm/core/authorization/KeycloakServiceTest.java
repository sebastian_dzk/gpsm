package com.sulacosoft.gpsm.core.authorization;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.keycloak.representations.idm.UserRepresentation;
import org.keycloak.representations.idm.authorization.AuthorizationResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {KeycloakService.class, KeycloakProperties.class})
public class KeycloakServiceTest {

    @Autowired
    KeycloakService keycloakService;

    @Test
    public void authorizationTest() {
        keycloakService.updateUserPassword("seba", "dksd1234");
        AuthorizationResponse respAuth = keycloakService.authorization("seba", "dksd1234");
    }

    @Test(expected = RuntimeException.class)
    public void authorizationFailTest() {
        keycloakService.authorization("userNotExist", "invalidPassword");
    }

    @Test
    public void createUserAndRemoveUserTest() {
        String username = "junit_test_keycloak";
        String email = username + "@pilnuj.test";
        String password = "qwerty";

        keycloakService.createUser(username, email);
        keycloakService.updateUserPassword(username, password);

        UserRepresentation user = keycloakService.findUserByUsername(username);
        assertThat(user).isNotNull();
        assertThat(user.getEmail()).isEqualTo(email);

        //keycloakService.authorization(username, password);

        keycloakService.removeUserById(user.getId());

        user = keycloakService.findUserByUsername(username);
        assertThat(user).isNull();
    }

    @Test
    public void findUserByIdTest() {
        UserRepresentation user = keycloakService.findUserById("777ce28e-3e6c-46a3-b48b-41d17aaff5fb"); // seba
        assertThat(user).isNotNull();
        assertThat(user.getEmail()).isEqualTo("sebastian.dziak@interia.pl");
    }
}
