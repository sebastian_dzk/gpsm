package com.sulacosoft.gpsm.core.authorization;

import com.sulacosoft.gpsm.core.email.EmailService;
import com.sulacosoft.gpsm.core.json.RegisterJson;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.assertj.core.api.Java6Assertions.assertThat;

/*@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {
        KeycloakController.class,
        KeycloakService.class,
        KeycloakProperties.class,
        EmailService.class,
        JavaMailSender.class
})*/
public class KeycloakControllerTest {

    @Autowired
    private KeycloakController keycloakController;

    @Autowired
    private KeycloakService keycloakService;

    @Test
    public void loginValidTest() {
        LoginJson request = new LoginJson();
        request.setUsername("seba");
        request.setPassword("dksd1234");
        ResponseEntity<String> response = keycloakController.login(request);
        assertThat(response.getStatusCodeValue()).isEqualTo(HttpStatus.OK.value());
        assertThat(response.getBody()).isNotNull();
    }


    @Test
    public void loginInvalidTest() {
        LoginJson request = new LoginJson();
        request.setUsername("seba");
        request.setPassword("xxxxxxx");
        ResponseEntity<String> response = keycloakController.login(request);
        assertThat(response.getStatusCodeValue()).isEqualTo(HttpStatus.UNAUTHORIZED.value());
        assertThat(response.getBody()).isNull();
    }


    @Test
    public void createUserTest() {
        String username = "it_user";
        String password = "it_pass";
        String email = "it_user@pilnuj.com";

        RegisterJson reqister = new RegisterJson();
        reqister.setUsername(username);
        reqister.setPassword(password);
        reqister.setEmail(email);

        ResponseEntity response = keycloakController.create(reqister);
        System.out.println(response);

        keycloakService.confirmEmail(username);
        keycloakService.authorization(username, password);

        UserRepresentation user = keycloakService.findUserByUsername(username);
        keycloakService.removeUserById(user.getId());
    }
}
