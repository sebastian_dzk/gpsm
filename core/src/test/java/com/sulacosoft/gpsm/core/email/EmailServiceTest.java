package com.sulacosoft.gpsm.core.email;

import com.sulacosoft.gpsm.core.EmailConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {EmailService.class, EmailConfig.class})
public class EmailServiceTest {


    @Autowired
    private EmailService emailService;

    @Test
    public void sendEmailTest() {

        emailService.sendSimpleMessage("sebastian.dziak@gmail.com", "Temacik", "Teść http://wp.pl");

    }


}
