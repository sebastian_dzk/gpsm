package com.sulacosoft.gpsm.core.controller;

import lombok.Data;

@Data
public class PaymentLinkJson {
    private String title;
    private String link;
}
