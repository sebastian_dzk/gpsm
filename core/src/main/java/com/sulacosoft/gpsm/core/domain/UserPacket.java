package com.sulacosoft.gpsm.core.domain;

import com.sulacosoft.gpsm.core.enums.Packet;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "user_package")
@Data
public class UserPacket {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @NotNull
    @Column(name="user_uid")
    private String userUid;

    @Column(name = "created_at")
    private Date createdAt;

    private Date since;

    private Date till;

    @Enumerated(EnumType.STRING)
    private Packet packet;

    @Column(name = "dotpay_operation_number", unique = true)
    private String dotpayOperationNumber;

    private String apiVersion;

    @PrePersist
    private void prePersist() {
        this.createdAt = new Date();
    }

}
