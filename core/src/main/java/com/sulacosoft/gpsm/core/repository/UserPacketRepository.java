package com.sulacosoft.gpsm.core.repository;

import com.sulacosoft.gpsm.core.domain.UserPacket;
import com.sulacosoft.gpsm.core.enums.Packet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface UserPacketRepository extends JpaRepository<UserPacket, Long> {

    @Query("SELECT up FROM UserPacket up WHERE up.userUid=:userUid AND :date BETWEEN up.since AND up.till")
    List<UserPacket> findByUserUidAndDate(@Param("userUid") String userUid, @Param("date") Date date);

    @Query("SELECT MAX(up.till) FROM UserPacket up WHERE up.userUid=:userUid AND up.packet=:packet")
    Date findTillByUserUidAndPacket(@Param("userUid") String userUid, @Param("packet") Packet packet);
}
