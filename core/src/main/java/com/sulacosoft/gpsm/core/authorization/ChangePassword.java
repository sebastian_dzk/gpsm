package com.sulacosoft.gpsm.core.authorization;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChangePassword {
    private String code;
    private String password;
}
