package com.sulacosoft.gpsm.core.repository;

import com.sulacosoft.gpsm.core.domain.Device;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DeviceRepository extends JpaRepository<Device, Long> {

    Device findByUid(String deviceUid);

    @Query("SELECT d FROM Device d WHERE d.userUid=:userUid ORDER BY d.createdAt DESC")
    List<Device> findByUserUid(@Param("userUid") String userUid, Pageable page);
}
