package com.sulacosoft.gpsm.core;

public class GpsmException extends RuntimeException {
    public GpsmException(String msg) {
        super(msg);
    }
}
