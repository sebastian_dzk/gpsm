package com.sulacosoft.gpsm.core.repository;

import com.sulacosoft.gpsm.core.domain.DeviceLastPosition;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DeviceLastPositionRepository extends JpaRepository<DeviceLastPosition, Long> {

    @Query("SELECT dlp FROM DeviceLastPosition dlp WHERE dlp.device.userUid=:userUid ORDER BY dlp.createdAt DESC")
    List<DeviceLastPosition> findByUserId(@Param("userUid") String userId);

    @Query("SELECT dlp FROM DeviceLastPosition dlp WHERE dlp.device.uid=:deviceUid")
    List<DeviceLastPosition>  findByDeviceUid(@Param("deviceUid") String deviceUid);

    @Modifying
    @Query("DELETE FROM DeviceLastPosition WHERE device.id = :deviceId")
    void deleteByDeviceId(@Param("deviceId")  Long deviceId);
}
