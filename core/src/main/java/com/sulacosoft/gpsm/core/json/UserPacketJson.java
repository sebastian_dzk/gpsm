package com.sulacosoft.gpsm.core.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.sulacosoft.gpsm.core.enums.Packet;
import lombok.Data;

import java.util.Date;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserPacketJson {
    private Date till;
    private Packet packet;
}
