package com.sulacosoft.gpsm.core.authorization;

import com.sulacosoft.gpsm.core.exceptions.KeycloakReqisterConflictException;
import org.apache.http.HttpStatus;
import com.sulacosoft.gpsm.core.email.EmailService;
import com.sulacosoft.gpsm.core.exception.InvalidAuthorizationException;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.authorization.client.AuthzClient;
import org.keycloak.authorization.client.resource.AuthorizationResource;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.keycloak.representations.idm.authorization.AuthorizationRequest;
import org.keycloak.representations.idm.authorization.AuthorizationResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.Response;
import java.util.List;

// https://github.com/lbroudoux/spring-boot-keycloak-authz/blob/master/src/main/java/com/github/lbroudoux/springbootkeycloakauthz/web/AuthzController.java
// https://stackoverflow.com/questions/33910615/is-there-an-api-call-for-changing-user-password-on-keycloak
// https://stackoverflow.com/questions/44794201/keycloak-rest-api-403-forbidden
// https://www.programcreek.com/java-api-examples/?class=org.keycloak.admin.client.resource.UserResource&method=resetPassword

/*
get properties form keycloak.json
AuthzClient.create().getConfiguration().getRealm()
*/

@Service
public class KeycloakService {

    private final static Logger log = LoggerFactory.getLogger(KeycloakService.class);

    private KeycloakProperties keycloakProperties;

  //  private EmailService emailService;

    public KeycloakService(KeycloakProperties keycloakProperties/*, EmailService emailService*/) {
        this.keycloakProperties =keycloakProperties;
  //      this.emailService = emailService;
    }

    public AuthorizationResponse authorization(String username, String password) {
        try {
            AuthzClient authzClient = AuthzClient.create();
            AuthorizationResource resource = authzClient.authorization(username, password);
            AuthorizationRequest request = new AuthorizationRequest();
            resource.authorize(request);
            return resource.authorize();
        }
        catch(Throwable t) {
            throw new InvalidAuthorizationException();
        }
    }

    public void createUser(String username, String email) {
        UserRepresentation userRepresentation = new UserRepresentation();
        userRepresentation.setUsername(username);
        userRepresentation.setEmail(email);

        // String message = "";
        // emailService.sendSimpleMessage(register.getEmail(), "E-mail confirmation / Potwierdzenie e-maila", message);

        Response response = getRealm().users().create(userRepresentation);

        if (response.getStatus() == HttpStatus.SC_CONFLICT) {
            throw new KeycloakReqisterConflictException();
        }

        log.info("Create keycloak user %s, http_status=", username, response.getStatus());
    }

    public void confirmEmail(String username /*, String email*/) {
       /* UserRepresentation ur = findUserByUsername(username);
        ur.setEmailVerified(true);
        ur.setEnabled(true);*/


        List<UserRepresentation> retrieveUserList = getRealm().users().search(username,
                null,
                null,
                null,
                0, 1);
        if (!retrieveUserList.isEmpty()) {
            UserResource retrievedUser = getRealm().users().get(retrieveUserList.get(0).getId());
           // CredentialRepresentation credential = new CredentialRepresentation();
           // credential.setType(CredentialRepresentation.);
          //  credential.setValue(newPassword);
          //  credential.setTemporary(false);
          //  retrievedUser.resetPassword(credential);

            // Remove the UPDATE_PASSWORD required action
            UserRepresentation userRepresentation = retrievedUser.toRepresentation();
            //userRepresentation.getRequiredActions().remove("UPDATE_PASSWORD");

            userRepresentation.setEmailVerified(true);
            userRepresentation.setEnabled(true);

            retrievedUser.update(userRepresentation);

            log.info("User %s password was changed!", username);
        } else {
            log.error("Updated password for username=" + username + "Requested user not found");
        }


    }

    public UserRepresentation findUserById(String id) {
        return getRealm().users().get(id).toRepresentation();
    }

    public UserRepresentation findUserByUsername(String username) {
        UserRepresentation user = null;
        List<UserRepresentation> ur = getRealm()
                .users()
                .search(username, null, null, null, 0, Integer.MAX_VALUE);
        if (ur.size() == 1) {
            user = ur.get(0);
        }

        if (ur.size() > 1) { // try to be more specific
            for (UserRepresentation rep : ur) {
                if (rep.getUsername().equalsIgnoreCase(username)) {
                    return rep;
                }
            }
        }

        return user;
    }

    public void updateUserPassword(String username, String newPassword) {
        List<UserRepresentation> retrieveUserList = getRealm().users().search(username,
                null,
                null,
                null,
                0, 1);
        if (!retrieveUserList.isEmpty()) {
            UserResource retrievedUser = getRealm().users().get(retrieveUserList.get(0).getId());
            CredentialRepresentation credential = new CredentialRepresentation();
            credential.setType(CredentialRepresentation.PASSWORD);
            credential.setValue(newPassword);
            credential.setTemporary(false);
            retrievedUser.resetPassword(credential);

            // Remove the UPDATE_PASSWORD required action
            UserRepresentation userRepresentation = retrievedUser.toRepresentation();
            userRepresentation.getRequiredActions().remove("UPDATE_PASSWORD");
            retrievedUser.update(userRepresentation);


            log.info("User %s password was changed!", username);
        } else {
            log.error("Updated password for username=" + username + "Requested user not found");
        }
    }

    public void removeUserById(String id) {
        getRealm().users().get(id).remove();
    }

    private RealmResource getRealm() {
        return createKeycloak().realm(keycloakProperties.getRealm());
    }

    private Keycloak createKeycloak() {
        return Keycloak.getInstance(
                keycloakProperties.getUrl(),
                keycloakProperties.getRealm(),
                keycloakProperties.getUsername(),
                keycloakProperties.getPassword(),
                keycloakProperties.getClientId()
        );
    }

    public void logout(String userId) {
        // TODO: only log info
    }
}
