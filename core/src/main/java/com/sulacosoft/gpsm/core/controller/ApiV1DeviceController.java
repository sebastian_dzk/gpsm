package com.sulacosoft.gpsm.core.controller;


import com.sulacosoft.gpsm.core.domain.DevicePoint;
import com.sulacosoft.gpsm.core.converter.DeviceJsonToDeviceConverter;
import com.sulacosoft.gpsm.core.converter.DevicePositionHistoryConverter;
import com.sulacosoft.gpsm.core.converter.DeviceToDeviceJsonConverter;
import com.sulacosoft.gpsm.core.domain.Device;
import com.sulacosoft.gpsm.core.domain.DeviceLastPosition;
import com.sulacosoft.gpsm.core.json.DeviceJson;
import com.sulacosoft.gpsm.core.json.DevicePositionHistoryJson;
import com.sulacosoft.gpsm.core.service.ColorService;
import com.sulacosoft.gpsm.core.service.DeviceLastPositionService;
import com.sulacosoft.gpsm.core.service.GpsDeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("api/v1/device")
public class ApiV1DeviceController {

    @Autowired
    private DeviceLastPositionService deviceLastPositionService;

    @Autowired
    private GpsDeviceService gpsDeviceService;

    @Autowired
    private DevicePositionHistoryConverter devicePositionHistoryConverter;

    @Autowired
    private DeviceToDeviceJsonConverter deviceToDeviceJsonConverter;

    @Autowired
    private DeviceJsonToDeviceConverter deviceJsonToDeviceConverter;

    @Autowired
    private ColorService colorService;

    @DeleteMapping("{id}")
    public ResponseEntity<Void> deleteDevice(@PathVariable Long id) {
        gpsDeviceService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PostMapping()
    public ResponseEntity<DeviceJson> addDevice(@RequestBody DeviceJson deviceJson) {
        int devicesSize = gpsDeviceService.listByUser(SecurityContextHolder.getContext().getAuthentication().getName(),
                Pageable.unpaged()).size();

        String iconColor = colorService.getIconColor(devicesSize);
        deviceJson.setIconColor(iconColor);

        Device device = deviceJsonToDeviceConverter.convert(deviceJson);
        device = gpsDeviceService.add(device);
        DeviceJson response = deviceToDeviceJsonConverter.convert(device);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PutMapping("{id}")
    public ResponseEntity<Void> updateDevice(@PathVariable Long id, @RequestBody DeviceJson deviceJson) {
        gpsDeviceService.update(id, deviceJson);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @GetMapping("positions/last")
    public ResponseEntity<DevicePositionHistoryJson> positionsLast() {
        String userId = SecurityContextHolder.getContext().getAuthentication().getName();
        List<DeviceLastPosition> devicesPositions = deviceLastPositionService.findByUserId(userId);
        DevicePositionHistoryJson response = devicePositionHistoryConverter.convert(devicesPositions);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    //yyyyMMdd/HHmmss/yyyyMMdd/HHmmss
    @GetMapping("positions/{sinceDate}/{sinceTime}/{tillTime}")
    public ResponseEntity<DevicePositionHistoryJson> positionsSinceTill(
            @PathVariable String sinceDate, @PathVariable String sinceTime, @PathVariable String tillTime) {

        String userId = SecurityContextHolder.getContext().getAuthentication().getName();
        Map<Device, List<DevicePoint>> map = new HashMap<>();

        List<Device> devices = gpsDeviceService.listByUser(userId, Pageable.unpaged());
        for (Device device : devices) {
            List<DevicePoint> points = gpsDeviceService.listPositionsByUserAndDeviceAndDateAndSinceTimeAndTillTime(
                    userId, device.getUid(), Integer.valueOf(sinceDate), Integer.valueOf(sinceTime), Integer.valueOf(tillTime));
            map.put(device, points);
        }

        DevicePositionHistoryJson response = devicePositionHistoryConverter.convert(map);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
