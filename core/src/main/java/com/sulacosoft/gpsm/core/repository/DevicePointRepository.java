package com.sulacosoft.gpsm.core.repository;

import com.sulacosoft.gpsm.core.domain.DevicePoint;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface DevicePointRepository extends JpaRepository<DevicePoint, Long> {

    List<DevicePoint> findByDeviceIdAndMeasureDate(@Param("deviceId") long deviceId, @Param("measureDate") int measureDate);

    List<DevicePoint> findByDeviceIdAndMeasureDateAndMeasureTimeGreaterThanEqualAndMeasureTimeLessThanEqual(@Param("deviceId") long deviceId,
                                                                                                            @Param("measureDate") int measureDate,
                                                                                                            @Param("sinceTime") int sinceTime,
                                                                                                            @Param("tillTime") int tillTime);

    @Modifying
    @Query("DELETE FROM DevicePoint dp WHERE dp.deviceId = :deviceId")
    void deleteByDeviceId(@Param("deviceId")  Long deviceId);
}
