package com.sulacosoft.gpsm.core.converter;

import com.sulacosoft.gpsm.core.domain.DevicePoint;
import com.sulacosoft.gpsm.core.domain.Device;
import com.sulacosoft.gpsm.core.domain.DeviceLastPosition;
import com.sulacosoft.gpsm.core.json.DevicePositionHistoryJson;
import com.sulacosoft.gpsm.core.util.DateTimeUtil;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.*;

@Component
public class DevicePositionHistoryConverter {

    public DevicePositionHistoryJson convert(List<DeviceLastPosition> src) {
        DevicePositionHistoryJson response = new DevicePositionHistoryJson();
        response.setSuccess(true);
        response.setDevices(new ArrayList<>());

        for (DeviceLastPosition device : src
        ) {
            DevicePositionHistoryJson.DeviceJson destDevice = new DevicePositionHistoryJson.DeviceJson();
            destDevice.setName(device.getDevice().getName());
            destDevice.setIconColor(device.getDevice().getIconColor());
            destDevice.setPositions(new ArrayList<>());

            DevicePositionHistoryJson.DeviceJson.PositionJson destPosition = new DevicePositionHistoryJson.DeviceJson.PositionJson();
            destPosition.setLat(device.getLatitude());
            destPosition.setLng(device.getLongitude());
            destPosition.setTime(device.getMeasurementAt());
            destPosition.setDescription(createDescription(device.getMeasurementAt(), device.getLatitude(), device.getLongitude()));
            destDevice.getPositions().add(destPosition);
            response.getDevices().add(destDevice);
        }
        return response;
    }

    public DevicePositionHistoryJson convert(Map<Device, List<DevicePoint>> devicesAndPointsMap) {
        DevicePositionHistoryJson response = new DevicePositionHistoryJson();
        response.setSuccess(true);
        response.setDevices(new ArrayList<>());

        Set<Device> srcDevices = devicesAndPointsMap.keySet();
        for (Device srcDevice : srcDevices
        ) {
            DevicePositionHistoryJson.DeviceJson destDevice = new DevicePositionHistoryJson.DeviceJson();
            destDevice.setName(srcDevice.getName());
            destDevice.setIconColor(srcDevice.getIconColor());
            destDevice.setPositions(new ArrayList<>());

            List<DevicePoint> srcPositions = devicesAndPointsMap.get(srcDevice);
            for (DevicePoint srcPosition : srcPositions
            ) {
                DevicePositionHistoryJson.DeviceJson.PositionJson destPosition = new DevicePositionHistoryJson.DeviceJson.PositionJson();
                destPosition.setLat(srcPosition.getLatitude());
                destPosition.setLng(srcPosition.getLongitude());
                destPosition.setTime(DateTimeUtil.intsToDateTime(srcPosition.getMeasureDate(), srcPosition.getMeasureTime()));
                destPosition.setDescription(createDescription(DateTimeUtil.intsToDateTime(srcPosition.getMeasureDate(), srcPosition.getMeasureTime()), srcPosition.getLatitude(), srcPosition.getLongitude()));
                destDevice.getPositions().add(destPosition);
            }
            response.getDevices().add(destDevice);
        }

        return response;
    }

    private String createDescription(Date dateTime, double lat, double lng) {
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return f.format(dateTime) + "<br>lat=" + lat + ";lng=" + lng;
    }
}
