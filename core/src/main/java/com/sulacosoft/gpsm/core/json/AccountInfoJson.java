package com.sulacosoft.gpsm.core.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AccountInfoJson {
    private String userUid;
    private String email;
    private UserPacketJson packet;
}
