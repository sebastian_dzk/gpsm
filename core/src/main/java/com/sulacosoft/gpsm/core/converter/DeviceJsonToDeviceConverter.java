package com.sulacosoft.gpsm.core.converter;

import com.sulacosoft.gpsm.core.domain.Device;
import com.sulacosoft.gpsm.core.json.DeviceJson;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class DeviceJsonToDeviceConverter implements Converter<DeviceJson, Device> {

    @Override
    public Device convert(DeviceJson source) {
        Device dest = new Device();
        dest.setUid(source.getUid());
        dest.setName(source.getName());
        dest.setSource(source.getSource());
        dest.setIconColor(source.getIconColor());
        return dest;
    }
}
