package com.sulacosoft.gpsm.core.authorization;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class LoginJson {
    private String username;
    private String password;
}
