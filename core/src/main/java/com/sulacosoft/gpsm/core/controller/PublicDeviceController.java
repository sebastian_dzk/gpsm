package com.sulacosoft.gpsm.core.controller;

import com.sulacosoft.gpsm.core.domain.Device;
import com.sulacosoft.gpsm.core.json.DeviceApkPositionJson;
import com.sulacosoft.gpsm.core.json.DeviceJson;
import com.sulacosoft.gpsm.core.service.GpsDeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/public/device")
public class PublicDeviceController {

    @Autowired
    private GpsDeviceService gpsDeviceService;

    @Deprecated
    @GetMapping("list")
    public ResponseEntity<List<DeviceJson>> list() {
        List<Device> devices = gpsDeviceService.deviceList();
        List<DeviceJson> response = devices.stream().map(p->convert(p)).collect(Collectors.toList());
        return new ResponseEntity<List<DeviceJson>>(response, HttpStatus.OK);
    }

    private DeviceJson convert(Device src) {
        DeviceJson dest = new DeviceJson();
        dest.setId(src.getId());
        dest.setName(src.getName());
        dest.setCreatedAt(src.getCreatedAt());
        return dest;
    }
}
