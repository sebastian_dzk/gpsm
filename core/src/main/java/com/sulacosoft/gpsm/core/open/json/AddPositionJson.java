package com.sulacosoft.gpsm.core.open.json;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Getter
@Setter
@ApiModel(value="AddPosition")
public class AddPositionJson {

    @ApiModelProperty(value = "IMEI device", required = true)
    private String imei;

    @ApiModelProperty(value = "Latitude", required = true)
    private double lat;

    @ApiModelProperty(value = "Longitude", required = true)
    private double lng;

    @ApiModelProperty(value = "Altitude above sea level [meters]")
    private Double altitude;

    @ApiModelProperty(value = "Velocity [meters/second]")
    private Double velocity;

    @ApiModelProperty(value = "Battery charge [%]")
    @Min(0)
    @Max(100)
    private Integer battery;
}
