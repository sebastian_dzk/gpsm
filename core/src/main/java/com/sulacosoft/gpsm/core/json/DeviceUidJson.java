package com.sulacosoft.gpsm.core.json;


import javax.validation.constraints.NotNull;

public class DeviceUidJson {

    @NotNull
    private String uid;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    @Override
    public String toString() {
        return "DeviceUidJson{" +
                "uid='" + uid + '\'' +
                '}';
    }
}
