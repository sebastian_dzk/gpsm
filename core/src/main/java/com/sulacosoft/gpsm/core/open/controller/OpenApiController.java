package com.sulacosoft.gpsm.core.open.controller;

import com.sulacosoft.gpsm.core.json.DeviceApkPositionJson;
import com.sulacosoft.gpsm.core.open.json.AddPositionJson;
import com.sulacosoft.gpsm.core.service.DeviceLastPositionService;
import com.sulacosoft.gpsm.core.service.GpsDeviceService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
@RequestMapping("open-api")
public class OpenApiController {

    @Autowired
    private DeviceLastPositionService deviceLastPositionService;

    @Autowired
    private GpsDeviceService gpsDeviceService;

    @PostMapping("position")
    @ApiOperation(value = "Details device postions",
            notes = "Adds GPS last position data")
    public ResponseEntity<Void> positionAdd(
            @ApiParam("GPS last position")
            @RequestBody AddPositionJson addPosition
    ) {
        DeviceApkPositionJson deviceApkPositionJson = convert(addPosition);
        deviceLastPositionService.updateLastPosition(deviceApkPositionJson);
        gpsDeviceService.addDevicePoint(deviceApkPositionJson);
        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }

    private DeviceApkPositionJson convert(AddPositionJson src) {
        DeviceApkPositionJson dest = new DeviceApkPositionJson();
        dest.setDeviceUid(src.getImei());
        dest.setSource("OPEN");
        dest.setMeasureDate(new Date());
        dest.setVelocity(src.getVelocity());
        dest.setHeight(src.getAltitude());
        dest.setLatitude(src.getLat());
        dest.setLongitude(src.getLng());
        dest.setBatteryLevel(src.getBattery());
        return dest;
    }
}
