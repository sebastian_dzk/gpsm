package com.sulacosoft.gpsm.core.converter;

import com.sulacosoft.gpsm.core.domain.Device;
import com.sulacosoft.gpsm.core.domain.DeviceLastPosition;
import com.sulacosoft.gpsm.core.json.DeviceDetailsJson;
import org.springframework.stereotype.Component;

@Component
public class DeviceAndDeviceLastPositionToDeviceDetailsJsonConverter {

    public DeviceDetailsJson convert(Device src, DeviceLastPosition dp) {
        DeviceDetailsJson dest = new DeviceDetailsJson();
        dest.setId(src.getId());
        dest.setName(src.getName());
        dest.setUid(src.getUid());
        dest.setCreatedAt(src.getCreatedAt());
        dest.setSource(src.getSource());
        dest.setIconColor(src.getIconColor());

        if (dp != null) {
            dest.setLatitude(dp.getLatitude());
            dest.setLongitude(dp.getLongitude());
            dest.setLastPositionTime(dp.getMeasurementAt().toInstant());
            dest.setBatteryLevel(dp.getBatteryLevel());
        }

        return dest;
    }
}
