package com.sulacosoft.gpsm.core.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
public class DeviceDetailsJson extends DeviceJson {
    private double latitude, longitude;
    private Integer batteryLevel;
    private Instant lastPositionTime;
}
