package com.sulacosoft.gpsm.core.service;

import com.sulacosoft.gpsm.core.GpsmException;
import com.sulacosoft.gpsm.core.converter.DeviceJsonToDeviceConverter;
import com.sulacosoft.gpsm.core.domain.Device;
import com.sulacosoft.gpsm.core.domain.DeviceLog;
import com.sulacosoft.gpsm.core.domain.DevicePoint;
import com.sulacosoft.gpsm.core.json.DeviceApkPositionJson;
import com.sulacosoft.gpsm.core.json.DeviceJson;
import com.sulacosoft.gpsm.core.json.DevicePositionJson;
import com.sulacosoft.gpsm.core.json.DeviceStatusJson;
import com.sulacosoft.gpsm.core.repository.DeviceLastPositionRepository;
import com.sulacosoft.gpsm.core.repository.DeviceLogRepository;
import com.sulacosoft.gpsm.core.repository.DeviceRepository;
import com.sulacosoft.gpsm.core.repository.DevicePointRepository;
import com.sulacosoft.gpsm.core.util.DateTimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class GpsDeviceService {

    @Autowired
    private DeviceJsonToDeviceConverter deviceJsonToDeviceConverter;

    @Autowired
    private DeviceRepository deviceRepository;

    @Autowired
    private DevicePointRepository devicePointRepository;

    @Autowired
    private DeviceLogRepository deviceLogRepository;

    @Autowired
    private DeviceLastPositionRepository deviceLastPositionRepository;

    @Autowired
    private ColorService colorService;

    @Transactional
    public void delete(Long id) {
        Optional<Device> deviceOptional = deviceRepository.findById(id);
        if(!deviceOptional.isPresent()) {
            return;
        }

        Device device = deviceOptional.get();
        if(!device.getUserUid().equals(SecurityContextHolder.getContext().getAuthentication().getName())) {
            throw new GpsmException("Dont't available access to device " + device.getId() + "!");
        }

        devicePointRepository.deleteByDeviceId(device.getId());
        deviceLastPositionRepository.deleteByDeviceId(device.getId());
        deviceRepository.deleteById(device.getId());
    }

    public void update(Long id, DeviceJson deviceJson) {
        Optional<Device> deviceOptional = deviceRepository.findById(id);
        if(!deviceOptional.isPresent()) {
            return;
        }

        Device device = deviceOptional.get();
        if(!device.getUserUid().equals(SecurityContextHolder.getContext().getAuthentication().getName())) {
            throw new GpsmException("Dont't available access to device " + device.getId() + "!");
        }

        device.setName(deviceJson.getName());
        deviceRepository.save(device);
    }

    public DeviceStatusJson deviceStatus(String deviceUid) {
        Device device = deviceRepository.findByUid(deviceUid);
        DeviceStatusJson deviceStatus = new DeviceStatusJson();
        deviceStatus.setDeviceExist(device != null);
        return deviceStatus;
    }

    public void addDevicePoint(DeviceApkPositionJson gpsPositionJson) throws GpsmException {
        Device device = deviceRepository.findByUid(gpsPositionJson.getDeviceUid());
        if (device == null)
            throw new GpsmException("Device " + gpsPositionJson.getDeviceUid() + " not exist!");

        Date now = new Date();
        DevicePoint devicePoint = new DevicePoint();
        devicePoint.setCreateDate(DateTimeUtil.dateToDateInteger(now));
        devicePoint.setCreateTime(DateTimeUtil.dateToTimeInteger(now));
        devicePoint.setMeasureDate(DateTimeUtil.dateToDateInteger(gpsPositionJson.getMeasureDate()));
        devicePoint.setMeasureTime(DateTimeUtil.dateToTimeInteger(gpsPositionJson.getMeasureDate()));
        devicePoint.setDeviceId(device.getId());
        devicePoint.setLongitude(gpsPositionJson.getLongitude());
        devicePoint.setLatitude(gpsPositionJson.getLatitude());
        devicePoint.setVelocity(gpsPositionJson.getVelocity());
        devicePoint.setHeight(gpsPositionJson.getHeight());
        devicePoint.setBatteryLevel(gpsPositionJson.getBatteryLevel());
        devicePoint.setAlarm(gpsPositionJson.isAlarm());

        devicePointRepository.save(devicePoint);
    }

    @Transactional
    public void bindDevice(String userId, DeviceJson apkDeviceInfoJson) throws GpsmException {
        String uid = apkDeviceInfoJson.getUid();
        Device device = deviceRepository.findByUid(uid);
        if (device != null)
            throw new GpsmException("Device " + uid + " already exist!");

        device = deviceJsonToDeviceConverter.convert(apkDeviceInfoJson);
        device.setUserUid(userId);

        int devicesSize = listByUser(SecurityContextHolder.getContext().getAuthentication().getName(),
                Pageable.unpaged()).size();

        String iconColor = colorService.getIconColor(devicesSize);
        device.setIconColor(iconColor);

        deviceRepository.save(device);
    }

    public void addLog(String deviceUid, String message) throws GpsmException {
        Device device = deviceRepository.findByUid(deviceUid);
        if (device == null)
            throw new GpsmException("Device " + deviceUid + " not exist!");

        DeviceLog deviceLog = new DeviceLog();
        deviceLog.setGpsDevice(device);
        deviceLog.setMessage(message);

        deviceLogRepository.save(deviceLog);
    }

    public List<Device> deviceList() {
        return deviceRepository.findAll();
    }

    public List<Device> listByUser(String userId, Pageable page) {
        return deviceRepository.findByUserUid(userId, page);
    }

    public List<DevicePoint>  listPositionsByUserAndDeviceAndDateAndSinceTimeAndTillTime(String userId, String deviceUid, int sinceDate, int sinceTime, int tillTime) {
        Device device = deviceRepository.findByUid(deviceUid);
        if(device==null)
            throw new GpsmException("Device not exist!");

        if(!device.getUserUid().equals(userId))
            throw  new GpsmException("Not privileges to device!");
                return devicePointRepository.findByDeviceIdAndMeasureDateAndMeasureTimeGreaterThanEqualAndMeasureTimeLessThanEqual(device.getId().intValue(), sinceDate, sinceTime, tillTime);
    }

    @Deprecated
    public List<DevicePositionJson> listPositionsByUserAndDeviceAndDateOld(String userId, String deviceUid, Date date) throws GpsmException {
              Device device = deviceRepository.findByUid(deviceUid);
        if(device==null)
            throw new GpsmException("Device not exist!");

        if(!device.getUserUid().equals(userId))
            throw  new GpsmException("Not privileges to device!");

        List<DevicePoint> points = devicePointRepository.findByDeviceIdAndMeasureDate(device.getId().intValue(), DateTimeUtil.dateToDateInteger(date));

        List<DevicePositionJson> response = new ArrayList<>();
        for(DevicePoint gp: points) {
            DevicePositionJson p = new DevicePositionJson();
            p.setTime(DateTimeUtil.intsToDateTime(gp.getMeasureDate(), gp.getMeasureTime()));
            p.setLat(gp.getLatitude());
            p.setLng(gp.getLongitude());
            response.add(p);
        }

        return response;
    }

    public Device add(Device device) {
        device.setUserUid(SecurityContextHolder.getContext().getAuthentication().getName());
        return deviceRepository.save(device);
    }
}
