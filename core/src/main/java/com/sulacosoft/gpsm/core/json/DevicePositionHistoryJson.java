package com.sulacosoft.gpsm.core.json;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class DevicePositionHistoryJson {

    private boolean success;
    private boolean allRecords;
    private List<DeviceJson> devices;

    @Data
    public static class DeviceJson {
        private String name;
        private String iconColor;
        private List<PositionJson> positions;

        @Data
        public static class PositionJson {
            private Date time;
            private String description;
            private double lat;
            private double lng;
        }
    }
}
