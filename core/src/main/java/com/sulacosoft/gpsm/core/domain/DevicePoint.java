package com.sulacosoft.gpsm.core.domain;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "device_point", indexes = {
        @Index(name = "index_device_point_1", columnList = "device_id, create_date")
})
@Data
public class DevicePoint {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "create_date")
    private int createDate;

    @Column(name = "create_time")
    private int createTime;

    @Column(name = "measure_date")
    private int measureDate;

    @Column(name = "measure_time")
    private int measureTime;

    @Column(name = "device_id")
    private Long deviceId;

    private double longitude;

    private double latitude;

    private double velocity;

    private double height;

    @Column(name = "battery_level", columnDefinition = "integer default -1")
    private Integer batteryLevel;

    Boolean alarm;
}
