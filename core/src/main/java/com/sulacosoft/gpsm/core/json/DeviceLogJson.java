package com.sulacosoft.gpsm.core.json;


import javax.validation.constraints.NotNull;

public class DeviceLogJson {

    @NotNull
    private String uid;

    private String message;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
