package com.sulacosoft.gpsm.core.authorization;

import com.sulacosoft.gpsm.core.exception.InvalidAuthorizationException;
import com.sulacosoft.gpsm.core.json.RegisterJson;
import com.sulacosoft.gpsm.core.util.UserUtil;
import org.keycloak.representations.idm.authorization.AuthorizationResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

//@RestController
//@RequestMapping("api/authorization")
public class KeycloakController {

    private KeycloakService keycloakService;

    public KeycloakController(KeycloakService keycloakService) {
        this.keycloakService = keycloakService;
    }

    @PostMapping("login")
    public ResponseEntity<String> login(@RequestBody @Valid LoginJson register) {
        try {
            AuthorizationResponse auth = keycloakService.authorization(register.getUsername(), register.getPassword());
            return new ResponseEntity<>(auth.getToken(), HttpStatus.OK);
        }
        catch(InvalidAuthorizationException e) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
    }

    @PostMapping("logout")
    public ResponseEntity<Void> logout() {
        keycloakService.logout(UserUtil.getUserId());
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("register")
    public ResponseEntity<Void> create(@RequestBody @Valid RegisterJson register) {
        keycloakService.createUser(register.getUsername(), register.getEmail());
        keycloakService.updateUserPassword(register.getUsername(), register.getPassword());
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("confirm-email")
    public ResponseEntity<Void> confirmEmail(@RequestParam("code")Long code) {
        //TODO
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @PostMapping("password-send-link")
    public ResponseEntity<Void> passwordSendLink() {
        //TODO
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @PostMapping("password-change")
    public ResponseEntity<Void> passwordChange(ChangePassword changePassword) {
        //TODO
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

}
