package com.sulacosoft.gpsm.core.domain;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "device", indexes = {
        @Index(name="device_uid", columnList = "uid")
})
@Data
public class Device {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "created_at")
    private Date createdAt;

    @Column(unique = true)
    @NotNull
    private String uid;

    @NotNull
    @Column(name="user_uid")
    private String userUid;

    @NotNull
    private String name;

    private String source;

    @Column(name = "icon_color")
    private String iconColor;

    @PrePersist
    private void prePersist() {
        this.createdAt = new Date();
    }

}
