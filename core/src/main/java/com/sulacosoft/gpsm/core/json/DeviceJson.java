package com.sulacosoft.gpsm.core.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
public class DeviceJson extends DeviceUidJson {

    private Long id;

    @NotNull
    private String name;
    private Date createdAt;
    private String source;
    private String iconColor;
}
