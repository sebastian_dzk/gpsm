package com.sulacosoft.gpsm.core.reciver;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.annotation.Transformer;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.ip.tcp.TcpInboundGateway;
import org.springframework.integration.ip.tcp.connection.AbstractServerConnectionFactory;
import org.springframework.integration.ip.tcp.connection.TcpNetServerConnectionFactory;
import org.springframework.integration.ip.tcp.serializer.ByteArrayRawSerializer;
import org.springframework.messaging.MessageChannel;

/**
 * Created by kdo on 16. 8. 30.
 */
@EnableIntegration
@IntegrationComponentScan
@Configuration
public class ReciverConfguration {

    private int port = 15002;

    @Bean
    public TcpInboundGateway tcpInGate(AbstractServerConnectionFactory connectionFactory) {
        TcpInboundGateway inGate = new TcpInboundGateway();
        inGate.setConnectionFactory(connectionFactory);
        inGate.setRequestChannel(fromTcp());
        inGate.setErrorChannel(errorTcp());
        return inGate;
    }

    @Bean
    public MessageChannel fromTcp() {
        return new DirectChannel();
    }

    @Bean
    public MessageChannel errorTcp() {
        return new DirectChannel();
    }

    @MessageEndpoint
    public static class Echo {

        @Transformer(inputChannel = "fromTcp", outputChannel = "toEcho")
        public String convert(byte[] bytes) {
            String s = new String(bytes);
            return s;
        }

        @ServiceActivator(inputChannel = "toEcho")
        public String upCase(String in) {
            System.out.println("IN  : " + in);
            return in.toUpperCase();
        }

        @Transformer(inputChannel = "resultToString")
        public String convertResult(byte[] bytes) {
            String s = new String(bytes);
            return s;
        }

        @Transformer(inputChannel = "errorTcp")
        public String errorHandler(byte[] bytes) {
            String s = new String(bytes);
            return s;
        }
    }

    @Bean
    public AbstractServerConnectionFactory serverCF() {
        TcpNetServerConnectionFactory factory = new TcpNetServerConnectionFactory(this.port);
        factory.setDeserializer(new ByteArrayRawSerializer());
        return factory;
    }
}
