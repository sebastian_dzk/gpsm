package com.sulacosoft.gpsm.core.json;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class DeviceApkPositionJson {

    @NotNull
    private String deviceUid;

    private String source;

    @JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss")
    private Date measureDate;

    @NotNull
    private Double velocity, height;

    private Double longitude, latitude;

    private Integer batteryLevel;

    private boolean alarm;
}
