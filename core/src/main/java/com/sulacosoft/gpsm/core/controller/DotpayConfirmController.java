package com.sulacosoft.gpsm.core.controller;

import com.sulacosoft.gpsm.core.enums.Packet;
import com.sulacosoft.gpsm.core.service.UserPacketService;
import com.sulacosoft.gpsm.core.util.Dotpay;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.UUID;

@Controller
@RequestMapping("dotpay_tech_channel_confirm")
public class DotpayConfirmController {

    private final org.slf4j.Logger logPayments = LoggerFactory.getLogger("gpsm.payments");

    @Value("${dotpay.pin}") String PIN;

    @Autowired
    private UserPacketService userPacketService;

    @PostMapping
    public ResponseEntity<String> dotpayTechChannelConfirm(
            HttpServletRequest request,
            @RequestParam Map<String,String> requestParams
    ) throws Exception {
        String uid = UUID.randomUUID().toString();
        String remoteHost = getRemoteHost(request);

        logPayments.info("X-Forwarded-For : " + request.getHeader("X-Forwarded-For"));

        logPayments.info(uid+";DOTPAY CONFIRM REQUEST; host=" + remoteHost + "; " + requestParams);

        Dotpay dotpay = new Dotpay(requestParams);
        if(!dotpay.checkSignature(PIN)) {
            logPayments.info(uid+";DOTPAY CONFIRM ERROR; checkSignature! host=" + remoteHost + "; " + dotpay);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        if(!Dotpay.checkIP(remoteHost)) {
            logPayments.info(uid+";DOTPAY CONFIRM ERROR; checkIP! host=" + remoteHost + "; " + dotpay);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        String[] controlParams = dotpay.getControl().split("#");
        if(dotpay.getOperation_type()== Dotpay.OperationType.PAYMENT && dotpay.getOperation_status()== Dotpay.OperationStatus.COMPLETED) {
            if (controlParams.length == 4 & controlParams[0].equals("V1")) {
                String userUid = controlParams[1];
                long days = Long.valueOf(controlParams[2]);

                userPacketService.addPacket(userUid, Packet.PREMIUM, days, dotpay.getOperation_number(), controlParams[3]);
                logPayments.info(uid+";DOTPAY CONFIRM SUCCESS; host=" + remoteHost + "; " + dotpay);
            } else {
                logPayments.info(uid+";DOTPAY CONFIRM ERROR; control signature! host=" + remoteHost + "; " + dotpay);
            }
        }

        return new ResponseEntity<>("OK",HttpStatus.OK);
    }

    private String getRemoteHost(HttpServletRequest request) {
        String host = request.getHeader("X-Forwarded-For");
        if (host == null) {
            host = request.getRemoteHost();
        }
        return host;
    }
}
