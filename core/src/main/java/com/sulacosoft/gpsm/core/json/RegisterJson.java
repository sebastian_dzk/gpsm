package com.sulacosoft.gpsm.core.json;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RegisterJson {

    private String username;
    private String password;
    private String email;

}
