package com.sulacosoft.gpsm.core.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "device_log")
public class DeviceLog {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "created_at")
    private Date createdAt;

    @ManyToOne
    @JoinColumn(nullable = false)
    private Device gpsDevice;

    @Column(length = 1023)
    private String message;

    @PrePersist
    private void prePersist() {
        this.createdAt = new Date();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Device getGpsDevice() {
        return gpsDevice;
    }

    public void setGpsDevice(Device gpsDevice) {
        this.gpsDevice = gpsDevice;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
