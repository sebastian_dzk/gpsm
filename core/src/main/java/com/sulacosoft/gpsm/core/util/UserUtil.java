package com.sulacosoft.gpsm.core.util;

import org.keycloak.KeycloakPrincipal;
import org.keycloak.representations.AccessToken;
import org.springframework.security.core.context.SecurityContextHolder;

public class UserUtil {

    public static AccessToken getAccessToken() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal instanceof KeycloakPrincipal) {
            KeycloakPrincipal context = (KeycloakPrincipal) principal;
            return context.getKeycloakSecurityContext().getToken();
        }

        return null;
    }

    public static String getUserId() {
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }
}
