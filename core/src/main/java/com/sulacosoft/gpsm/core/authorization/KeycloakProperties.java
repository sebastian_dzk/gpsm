package com.sulacosoft.gpsm.core.authorization;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "gpsm.keycloak")
@Getter
@Setter
public class KeycloakProperties {

    private String url;
    private String realm;
    private String clientId;
    private String username;
    private String password;

}