package com.sulacosoft.gpsm.core.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateTimeUtil {

    private final static Logger log = LoggerFactory.getLogger(DateTimeUtil.class);

    public static Integer dateToDateInteger(Date date) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        String stringDate = formatter.format(date);
        return Integer.valueOf(stringDate);
    }

    public static Integer dateToTimeInteger(Date date) {
        SimpleDateFormat formatter = new SimpleDateFormat("HHmmss");
        String stringDate = formatter.format(date);
        return Integer.valueOf(stringDate);
    }

    public static Date intsToDateTime(Integer date, Integer time) {
        String dateStr = date.toString();

        String timeStr = "000000" + time.toString();
        timeStr = timeStr.substring(timeStr.length()-6, timeStr.length());

        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd HHmmss");
        try {
            return formatter.parse(dateStr + " " + timeStr);
        } catch (ParseException e) {
            log.error(String.format("Parsing error (%s, %s)", date, time), e);
            return null;
        }
    }

    public static Date intToDate(Integer date) {
        String dateStr = date.toString();

        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd HHmmss");
        try {
            return formatter.parse(dateStr + " 000000");
        } catch (ParseException e) {
            log.error(String.format("Parsing error (%s, %s)", date, "000000"), e);
            return null;
        }
    }
}
