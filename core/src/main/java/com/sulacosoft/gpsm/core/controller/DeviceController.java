package com.sulacosoft.gpsm.core.controller;

import com.sulacosoft.gpsm.core.GpsmException;
import com.sulacosoft.gpsm.core.converter.DeviceAndDeviceLastPositionToDeviceDetailsJsonConverter;
import com.sulacosoft.gpsm.core.converter.DeviceToDeviceJsonConverter;
import com.sulacosoft.gpsm.core.domain.Device;
import com.sulacosoft.gpsm.core.domain.DeviceLastPosition;
import com.sulacosoft.gpsm.core.json.DeviceDetailsJson;
import com.sulacosoft.gpsm.core.json.DeviceJson;
import com.sulacosoft.gpsm.core.json.DevicePositionJson;
import com.sulacosoft.gpsm.core.service.DeviceLastPositionService;
import com.sulacosoft.gpsm.core.service.GpsDeviceService;
import com.sulacosoft.gpsm.core.util.DateTimeUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/devices")
public class DeviceController {

    @Autowired
    private GpsDeviceService gpsDeviceService;

    @Autowired
    private DeviceToDeviceJsonConverter deviceToDeviceJsonConverter;

    @Autowired
    private DeviceLastPositionService deviceLastPositionService;

    @Autowired
    private DeviceAndDeviceLastPositionToDeviceDetailsJsonConverter deviceAndDeviceLastPositionToDeviceDetailsJsonConverter;

    @GetMapping("user-devices/list")
    public ResponseEntity<List<DeviceDetailsJson>> userDevicesList() {
        String userId = SecurityContextHolder.getContext().getAuthentication().getName();

        List<DeviceLastPosition> devicesLastPosition = deviceLastPositionService.findByUserId(userId);

        Map<Long, DeviceLastPosition> pos = devicesLastPosition
                .stream()
                .collect(Collectors.toMap(d -> d.getDevice().getId(), d -> d));

        List<DeviceDetailsJson> devices = gpsDeviceService
                .listByUser(userId, new PageRequest(0, Integer.MAX_VALUE))
                .stream()
                .map(p->deviceAndDeviceLastPositionToDeviceDetailsJsonConverter.convert(p, pos.get(p.getId())))
                .collect(Collectors.toList());
        return new ResponseEntity<>(devices, HttpStatus.OK);
    }

    @GetMapping("user-devices/last-positions")
    public ResponseEntity<List<DevicePositionJson>> userDevicesLastPositions() {
        String userId = SecurityContextHolder.getContext().getAuthentication().getName();
        List<DeviceLastPosition> devicesPositions = deviceLastPositionService.findByUserId(userId);
        List<DevicePositionJson> response = devicesPositions.stream().map(p->convert(p)).collect(Collectors.toList());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    private DevicePositionJson convert(DeviceLastPosition src) {
        DevicePositionJson des = new DevicePositionJson();
        des.setDeviceUid(src.getDevice().getUid());
        des.setDeviceName(src.getDevice().getName());
        des.setTime(src.getMeasurementAt());
        des.setLat(src.getLatitude());
        des.setLng(src.getLongitude());
        return des;
    }

    @PostMapping("device/{deviceUid}/points/{date}")
    public ResponseEntity<List<DevicePositionJson>> positions(@PathVariable String deviceUid, @PathVariable int date) throws GpsmException {
        String userId = SecurityContextHolder.getContext().getAuthentication().getName();
        List<DevicePositionJson> devicesPositions = gpsDeviceService.listPositionsByUserAndDeviceAndDateOld(userId, deviceUid, DateTimeUtil.intToDate(date) );
        return new ResponseEntity<>(devicesPositions, HttpStatus.OK);
    }

}
