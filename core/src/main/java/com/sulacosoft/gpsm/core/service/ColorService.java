package com.sulacosoft.gpsm.core.service;

import org.springframework.stereotype.Service;

@Service
public class ColorService {

    private final String colors[] = new String[]{
            "blue",
            "green",
            "orange",
            "red",
            "violet",
            "yellow",
            "black",
            "grey"
    };


    public String getIconColor(int i) {
        int colorsLimit = colors.length - 1;

        if (i > colorsLimit) {
            i = colorsLimit;
        }

        return colors[i];
    }
}
