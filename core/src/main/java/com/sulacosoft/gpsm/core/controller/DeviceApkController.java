package com.sulacosoft.gpsm.core.controller;

import com.sulacosoft.gpsm.core.GpsmException;
import com.sulacosoft.gpsm.core.json.*;
import com.sulacosoft.gpsm.core.service.DeviceLastPositionService;
import com.sulacosoft.gpsm.core.service.GpsDeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Date;

@RestController
@RequestMapping("api/device/apk")
public class DeviceApkController {

    @Autowired
    private GpsDeviceService gpsDeviceService;

    @Autowired
    private DeviceLastPositionService deviceLastPositionService;

    @PostMapping("get-status")
    public ResponseEntity<DeviceStatusJson> getStatus(@RequestBody @Valid DeviceUidJson apkDeviceIdJson) {
        DeviceStatusJson resp = gpsDeviceService.deviceStatus(apkDeviceIdJson.getUid());
        return new ResponseEntity<>(resp, HttpStatus.OK);
    }

    @PostMapping("add-log")
    public ResponseEntity<Void> addLog(@RequestBody @Valid DeviceLogJson deviceLogJson) throws GpsmException {
        gpsDeviceService.addLog(deviceLogJson.getUid(), deviceLogJson.getMessage());
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @PostMapping("add-position")
    @Transactional(isolation= Isolation.READ_COMMITTED)
    public ResponseEntity<Void> addPosition(@RequestBody @Valid DeviceApkPositionJson gpsPositionJson) throws GpsmException {
        gpsPositionJson.setMeasureDate(new Date()); //TODO: temporary solution
        deviceLastPositionService.updateLastPosition(gpsPositionJson);
        gpsDeviceService.addDevicePoint(gpsPositionJson);
        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }

    @PostMapping("bind-device")
    public ResponseEntity<Void> bindDevice(@RequestBody @Valid DeviceJson apkDeviceInfoJson) throws GpsmException {
        String userId = SecurityContextHolder.getContext().getAuthentication().getName();
        gpsDeviceService.bindDevice(userId, apkDeviceInfoJson);
        return new ResponseEntity<Void>(HttpStatus.ACCEPTED);
    }

}
