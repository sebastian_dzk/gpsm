package com.sulacosoft.gpsm.core.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class DotpayUtil {

    public static String buildLinkWithChk(String pin, String sellerId, String apiVersion, String amount, String currency, String control, String description, String url, String urlc) throws UnsupportedEncodingException {
        String type = "4";
        String amountStr = amount;
        String link = buildLink(sellerId, apiVersion, amountStr, currency, control, description, url, type, urlc);
        String concatParams = buildConcatedParams(pin, sellerId, apiVersion, amount, currency, control, description, url, type, urlc);
        String hashJava = hashJava(concatParams);
        return "chk=" + hashJava + "&" + link;
    }

    private static String buildLink(String sellerId, String apiVersion, String amount, String currency, String control, String description, String url, String type, String urlc) throws UnsupportedEncodingException {
        String link = String.format("id=%s&api_version=%s&amount=%s&currency=%s&description=%s&control=%s&URL=%s&type=%s&URLC=%s",
                URLEncoder.encode( sellerId, "UTF-8"),
                URLEncoder.encode( apiVersion, "UTF-8"),
                URLEncoder.encode( amount, "UTF-8"),
                URLEncoder.encode( currency, "UTF-8"),
                URLEncoder.encode( description, "UTF-8"),
                URLEncoder.encode( control, "UTF-8"),
                URLEncoder.encode( url, "UTF-8"),
                URLEncoder.encode( type, "UTF-8"),
                URLEncoder.encode( urlc, "UTF-8")
                );
        return link;
    }

    private static String buildConcatedParams(String pin, String sellerId, String apiVersion, String amount, String currency, String control, String description, String url, String type, String urlc) {
        return pin + apiVersion + sellerId + amount + currency + description + control + url + type + urlc;
    }

    private static String hashJava(String text) {
        StringBuilder sb = new StringBuilder(text);

        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
        md.update(sb.toString().getBytes());
        byte byteData[] = md.digest();
        StringBuilder sbResp = new StringBuilder();
        for (byte aByteData : byteData) {
            sbResp.append(Integer.toString((aByteData & 0xff) + 0x100, 16).substring(1));
        }

        return sbResp.toString();
    }
}
