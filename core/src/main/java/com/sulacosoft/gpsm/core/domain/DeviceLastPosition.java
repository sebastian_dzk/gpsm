package com.sulacosoft.gpsm.core.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "device_last_position")
@Data
public class DeviceLastPosition {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "created_at")
    private Date createdAt;

    @ManyToOne
    @JoinColumn(nullable = false, unique = true)
    private Device device;

    @Column(name = "measurement_at")
    private Date measurementAt;

    private double latitude, longitude;

    private double velocity;

    private double height;

    @Column(name = "battery_level", columnDefinition = "integer default -1")
    private Integer batteryLevel;

    private Boolean alarm;

    @PrePersist
    private void prePersist() {
        this.createdAt = new Date();
    }

}
