package com.sulacosoft.gpsm.core.controller;

import com.sulacosoft.gpsm.core.enums.Packet;
import com.sulacosoft.gpsm.core.service.UserPacketService;
import com.sulacosoft.gpsm.core.util.DotpayUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("api/payments")
public class DotpayPaymentController {

    @Value("${dotpay.base_url}") String BASE_URL;
    @Value("${dotpay.pin}") String PIN;
    @Value("${dotpay.seller_id}") Integer SELLER_ID;
    @Value("${dotpay.api_version}") String API_VERSION;
    @Value("${dotpay.url}") String URL;
    @Value("${dotpay.urlc}") String URLC;

    @Autowired
    private UserPacketService userPacketService;

    @GetMapping
    List<PaymentLinkJson> getPaymentLinks() throws Exception {
        String userUid = SecurityContextHolder.getContext().getAuthentication().getName();
        List<PaymentLinkJson> payments = new ArrayList<PaymentLinkJson>();

        Packet currentPacket = userPacketService.findCurrenctPacket(userUid);
        if(currentPacket==Packet.FREE) {
            payments.add(createLink("30.00", "PLN", "Pakiet 30 dni", "V1#" + userUid + "#30#" + API_VERSION));
            payments.add(createLink("99.00", "PLN", "Pakiet 180 dni", "V1#" + userUid + "#180#" + API_VERSION));
        }

        return payments;
    }

    private PaymentLinkJson createLink(String amount, String currency, String description, String controlUserUID) throws Exception {
        PaymentLinkJson paymentLinkJson = new PaymentLinkJson();

        String link = DotpayUtil.buildLinkWithChk(PIN, SELLER_ID.toString(), API_VERSION, amount, currency, controlUserUID, description, URL, URLC);

        paymentLinkJson.setLink( BASE_URL + link );
        paymentLinkJson.setTitle(description);

        return paymentLinkJson;
    }
}
