package com.sulacosoft.gpsm.core.converter;

import com.sulacosoft.gpsm.core.domain.Device;
import com.sulacosoft.gpsm.core.json.DeviceJson;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class DeviceToDeviceJsonConverter implements Converter<Device, DeviceJson> {

    @Override
    public DeviceJson convert(Device src) {
        DeviceJson dest = new DeviceJson();
        dest.setId(src.getId());
        dest.setName(src.getName());
        dest.setUid(src.getUid());
        dest.setCreatedAt(src.getCreatedAt());
        dest.setSource(src.getSource());
        dest.setIconColor(src.getIconColor());
        return dest;
    }
}
