package com.sulacosoft.gpsm.core.json;


public class DeviceStatusJson {

    private boolean deviceExist;

    public boolean isDeviceExist() {
        return deviceExist;
    }

    public void setDeviceExist(boolean deviceExist) {
        this.deviceExist = deviceExist;
    }

    @Override
    public String toString() {
        return "DeviceStatusJson{" +
                "deviceExist=" + deviceExist +
                '}';
    }
}
