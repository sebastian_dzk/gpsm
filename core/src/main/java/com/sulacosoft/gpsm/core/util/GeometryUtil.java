package com.sulacosoft.gpsm.core.util;

import com.sulacosoft.gpsm.core.json.GpsPointJson;
import org.geolatte.geom.G2D;
import org.geolatte.geom.Geometries;
import org.geolatte.geom.Point;
import org.geolatte.geom.Position;
import org.geolatte.geom.crs.CoordinateReferenceSystems;

public class GeometryUtil {

    public static Point toPoint(double longitude, double latitude) {
        return Geometries.mkPoint(new G2D(longitude, latitude), CoordinateReferenceSystems.WGS84);
    }

    public static Point toPoint(GpsPointJson pointJson) {
        return toPoint(pointJson.getLongitude(), pointJson.getLatitude());
    }

    public static GpsPointJson convert(Point point) {
        Position position = point.getPosition();

        GpsPointJson gpsPointJson = new GpsPointJson();
        gpsPointJson.setLongitude(position.getCoordinate(0));
        gpsPointJson.setLatitude(position.getCoordinate(1));
        return gpsPointJson;
    }
}