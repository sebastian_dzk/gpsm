package com.sulacosoft.gpsm.core.controller;

import com.sulacosoft.gpsm.core.domain.UserPacket;
import com.sulacosoft.gpsm.core.enums.Packet;
import com.sulacosoft.gpsm.core.json.AccountInfoJson;
import com.sulacosoft.gpsm.core.json.UserPacketJson;
import com.sulacosoft.gpsm.core.service.UserPacketService;
import com.sulacosoft.gpsm.core.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/user")
public class UserController {

    @Autowired
        private UserPacketService userPacketService;

        @GetMapping("info")
        public ResponseEntity<AccountInfoJson> userDevicesList() {
            AccountInfoJson response = new AccountInfoJson();

            String userId = SecurityContextHolder.getContext().getAuthentication().getName();
            UserPacket packet = userPacketService.findCurrenctUserPacket(userId);

            response.setUserUid(userId);
            response.setEmail(UserUtil.getAccessToken().getEmail());

            UserPacketJson userPacketJson = new UserPacketJson();
            if(packet!=null) {
                userPacketJson.setPacket(packet.getPacket());
                userPacketJson.setTill(packet.getTill());
            }
            else {
                userPacketJson.setPacket(Packet.FREE);
            }

            response.setPacket(userPacketJson);

            return new ResponseEntity<>(response, HttpStatus.OK);
        }
}
