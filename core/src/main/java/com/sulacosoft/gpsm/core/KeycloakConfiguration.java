package com.sulacosoft.gpsm.core;

import com.sulacosoft.gpsm.core.authorization.KeycloakProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class KeycloakConfiguration {

    @Bean
    public KeycloakProperties keycloakProperties() {
        return new KeycloakProperties();
    }

}
