package com.sulacosoft.gpsm.core.internal.endpoint.controller;

import com.sulacosoft.gpsm.core.internal.endpoint.json.InternalEndpointPosition;
import com.sulacosoft.gpsm.core.json.DeviceApkPositionJson;
import com.sulacosoft.gpsm.core.json.GpsPointJson;
import com.sulacosoft.gpsm.core.service.DeviceLastPositionService;
import com.sulacosoft.gpsm.core.service.GpsDeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Date;

@RestController
@RequestMapping("internal/endpoint")
public class InternalEndpointController {

    /*
    {
    "imei" : "22334455667788",
    "lng" : 34.34,
    "lat" : 51.34,
    "speed" : 97.45
}
     */

    @Autowired
    private GpsDeviceService gpsDeviceService;

    @Autowired
    private DeviceLastPositionService deviceLastPositionService;

    @PostMapping("add-position")
    public ResponseEntity<Void> addPosition(@RequestBody @Valid InternalEndpointPosition body, HttpServletRequest request) {
        if(!"127.0.0.1".equals(request.getRemoteHost())) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        DeviceApkPositionJson gpsPositionJson = new DeviceApkPositionJson();
        gpsPositionJson.setSource("TK103");
        gpsPositionJson.setMeasureDate(new Date());
        gpsPositionJson.setDeviceUid(body.getImei());
        gpsPositionJson.setVelocity(body.getSpeed());
        gpsPositionJson.setLatitude(body.getLat());
        gpsPositionJson.setLongitude(body.getLng());
        gpsPositionJson.setHeight(-1000000.0);

        deviceLastPositionService.updateLastPosition(gpsPositionJson);
        gpsDeviceService.addDevicePoint(gpsPositionJson);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
