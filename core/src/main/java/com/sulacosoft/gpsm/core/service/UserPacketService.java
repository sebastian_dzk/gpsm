package com.sulacosoft.gpsm.core.service;

import com.sulacosoft.gpsm.core.domain.UserPacket;
import com.sulacosoft.gpsm.core.enums.Packet;
import com.sulacosoft.gpsm.core.repository.UserPacketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Component
public class UserPacketService {

    @Autowired
    private UserPacketRepository userPacketRepository;

    public Packet findCurrenctPacket(String userUid) {
        List<UserPacket> resp = userPacketRepository.findByUserUidAndDate(userUid, new Date());

        for(UserPacket p: resp) {
            if(p.getPacket()==Packet.PREMIUM) {
                return Packet.PREMIUM;
            }
        }

        return Packet.FREE;
    }

    public UserPacket findCurrenctUserPacket(String userUid) {
        List<UserPacket> resp = userPacketRepository.findByUserUidAndDate(userUid, new Date());

        if(resp.size()>0) {
            return resp.get(0);
        }

        return null;
    }

    public void addPacket(String userUid, Packet packet, long days, String dotpayOperationNumber, String apiVersion) {
        UserPacket p = new UserPacket();
        p.setUserUid(userUid);
        p.setPacket(packet);

        Date since = new Date();
        Date currentTill = userPacketRepository.findTillByUserUidAndPacket(userUid, Packet.PREMIUM);
        if(currentTill!=null && currentTill.getTime()>since.getTime()) since = currentTill;

        p.setSince(since);

        Date till = new Date(since.getTime() + (days * 24 * 60 * 60 * 1000));
        p.setTill(till);

        p.setDotpayOperationNumber(dotpayOperationNumber);
        p.setApiVersion(apiVersion);

        userPacketRepository.save(p);
    }
}
