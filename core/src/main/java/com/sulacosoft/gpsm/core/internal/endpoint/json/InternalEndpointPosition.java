package com.sulacosoft.gpsm.core.internal.endpoint.json;

import lombok.Data;

@Data
public class InternalEndpointPosition {
    private String imei;
    private double lng;
    private double lat;
    private double speed;
}
