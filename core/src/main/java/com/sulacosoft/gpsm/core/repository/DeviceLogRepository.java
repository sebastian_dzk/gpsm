package com.sulacosoft.gpsm.core.repository;

import com.sulacosoft.gpsm.core.domain.DeviceLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeviceLogRepository extends JpaRepository<DeviceLog, Long> {

}
