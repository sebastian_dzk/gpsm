package com.sulacosoft.gpsm.core.service;

import com.sulacosoft.gpsm.core.GpsmException;
import com.sulacosoft.gpsm.core.domain.Device;
import com.sulacosoft.gpsm.core.domain.DeviceLastPosition;
import com.sulacosoft.gpsm.core.json.DeviceApkPositionJson;
import com.sulacosoft.gpsm.core.repository.DeviceLastPositionRepository;
import com.sulacosoft.gpsm.core.repository.DeviceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DeviceLastPositionService {

    @Autowired
    private DeviceRepository deviceRepository;

    @Autowired
    private DeviceLastPositionRepository deviceLastPositionRepository;

    public void updateLastPosition(DeviceApkPositionJson gpsPositionJson) throws GpsmException {
        Device device = deviceRepository.findByUid(gpsPositionJson.getDeviceUid());
        if (device == null)
            throw new GpsmException("Device " + gpsPositionJson.getDeviceUid() + " not exist!");

        List<DeviceLastPosition> points = deviceLastPositionRepository.findByDeviceUid(gpsPositionJson.getDeviceUid());
        if (points.isEmpty()) {
            DeviceLastPosition deviceLastPosition = new DeviceLastPosition();
            deviceLastPosition.setDevice(device);
            deviceLastPosition.setMeasurementAt(gpsPositionJson.getMeasureDate());
            deviceLastPosition.setLatitude(gpsPositionJson.getLatitude());
            deviceLastPosition.setLongitude(gpsPositionJson.getLongitude());
            deviceLastPosition.setVelocity(gpsPositionJson.getVelocity());
            deviceLastPosition.setHeight(gpsPositionJson.getHeight());
            deviceLastPosition.setBatteryLevel(gpsPositionJson.getBatteryLevel());
            deviceLastPosition.setAlarm(gpsPositionJson.isAlarm());
            deviceLastPositionRepository.save(deviceLastPosition);
        } else {
            DeviceLastPosition point = points.get(0);
            point.setMeasurementAt(gpsPositionJson.getMeasureDate());
            point.setLatitude(gpsPositionJson.getLatitude());
            point.setLongitude(gpsPositionJson.getLongitude());
            point.setVelocity(gpsPositionJson.getVelocity());
            point.setHeight(gpsPositionJson.getHeight());
            point.setBatteryLevel(gpsPositionJson.getBatteryLevel());
            point.setAlarm(gpsPositionJson.isAlarm());
            deviceLastPositionRepository.save(point);
        }
    }

    public List<DeviceLastPosition> findByUserId(String userId) {
        return deviceLastPositionRepository.findByUserId(userId);
    }
}
