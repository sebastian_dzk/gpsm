install curl php module:
apt update && apt install php-curl

set as service:
https://stackoverflow.com/questions/2036654/run-php-script-as-daemon-process

in gpsm tk103 module:
1) copy endpoint-tk103.php file to dir /opt/gpsm-core-php
2) copy gpsm-tk103.service to dir /etc/systemd/system
3) run:
    systemctl start gpsm-tk103.service
    systemctl enable gpsm-tk103.service


examples data format:
1) TK102
imei:864895032450833,tracker,151006012336,,F,172337.000,A,5105.9792,N,11404.9599,W,0.01,322.56,,0,0,,,
2) TK103
imei:864895032450833,acc off,191014062555,,F,062555.00,A,5205.49794,N,02102.99972,E,0.036,0;
