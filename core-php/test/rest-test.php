<?php
echo "tk103 endoint";

sendPosition('ec113dce-80ad-4383-a46d-4d338772c29d', '19.201', '49.82', '75.01');


function sendPosition($imei, $lat, $lng, $speed)
{
    $postData = array(
        'imei' => $imei,
        'lat' => $lat,
        'lng' => $lng,
        'speed' => $speed
    );

    $ch = curl_init('http://127.0.0.1:8080/internal/endpoint/add-position');

    curl_setopt_array($ch, array(
        CURLOPT_POST => TRUE,
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
        ),
        CURLOPT_POSTFIELDS => json_encode($postData)
    ));

    // Send the request
    $response = curl_exec($ch);

    // Check for errors
    if ($response === FALSE) {
        die(curl_error($ch));
    }

    // Decode the response
    $responseData = json_decode($response, TRUE);
}

?>

