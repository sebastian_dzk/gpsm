<?php

// $addr = gethostbyname("www.websmithing.com");
// $addr = "127.0.0.1";

$addr = "127.0.0.1";
$port = "5002";

$client = stream_socket_client("tcp://$addr:$port", $errno, $errorMessage);

if ($client === false) {
    throw new UnexpectedValueException("Failed to connect: $errorMessage");
}

/*
Oct 14 08:25:56 vps612342 php[14979]: TK103 data: imei:864895032450833,acc off,191014062555,,F,062555.00,A,5205.49794,N,02102.99972,E,0.036,0;
Oct 14 20:14:37 vps612342 php[14979]: TK103 data: imei:864895032450833,tracker,151006012336,,F,172337.000,A,5105.9792,N,11404.9599,W,0.01,322.56,,0,0,,,
*/


// tk102 data
//$data = "imei:359710049095095,tracker,151006012336,,F,172337.000,A,5105.9792,N,11404.9599,W,0.01,322.56,,0,0,,,";

// tk103 data
$data = "imei:864895032450833,acc off,191014062555,,F,062555.00,A,5205.49794,N,02102.99972,E,0.036,0;";



fwrite($client, $data);

while (!feof($client)) {
    echo fgets($client, 128);
}
    
fclose($client);
