<section class="page-title bg-title overlay-dark">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<div class="title">
					<h3>Kontakt</h3>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section contact-form">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="section-title">
					<h3><span class="alternate">Napisz do mnie</span> wiadomość</h3>
					<p>Potrzebujesz nowej fukcjonalności, masz pomysł na działanie serwisu, może coś nie działa tak jak oczkujesz, widzisz błąd?
                                           Chcesz współpracować?
                                           Napisz do mnie.
                                           Żadnego maila nie zostawię bez odpowiedzi.</p>
					<div class="title" style="margin-top: 40px">
						<h5><i class="fa fa-envelope"> <a href="mailto:sebastian.dziak@gmail.com">sebastian.dziak@gmail.com</a></i></h5>
						<h5><i class="fa fa-skype"> <a href="skype:sebastian_dzk?call">sebastian_dzk</a></i></h5>				
					</div>
				</div>				
			</div>
		</div>
	</div>
</section>

