(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/account/account.component.html":
/*!************************************************!*\
  !*** ./src/app/account/account.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-view\">\n  <div class=\"row\">\n    <div class=\"col-2\"></div>\n    <div class=\"col-8\">\n\n      <table class=\"table\">\n        <tbody>\n        <tr>\n          <td>{{'screens.account.email' | translate}}</td>\n          <th scope=\"row\">{{userInfo.email}}</th>\n        </tr>\n        <tr>\n          <td>{{ 'screens.account.package' | translate }}</td>\n          <th scope=\"row\">{{userInfo.packet.packet}}</th>\n        </tr>\n        <tr>\n          <td>{{ 'screens.account.expirationTime' | translate }}</td>\n          <th *ngIf=\"userInfo.packet.till\" scope=\"row\">{{userInfo.packet.till | date : 'yyyy.MM.dd HH:mm'}}</th>\n          <th *ngIf=\"!userInfo.packet.till\" scope=\"row\">{{ 'screens.account.noLimit' | translate }}</th>\n        </tr>\n        </tbody>\n      </table>\n\n    </div>\n    <div class=\"col-2\"></div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/account/account.component.scss":
/*!************************************************!*\
  !*** ./src/app/account/account.component.scss ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FjY291bnQvYWNjb3VudC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/account/account.component.ts":
/*!**********************************************!*\
  !*** ./src/app/account/account.component.ts ***!
  \**********************************************/
/*! exports provided: AccountComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountComponent", function() { return AccountComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../rest.service */ "./src/app/rest.service.ts");



var AccountComponent = /** @class */ (function () {
    function AccountComponent(rest) {
        this.rest = rest;
    }
    AccountComponent.prototype.ngOnInit = function () {
        this.getUserInfo();
    };
    AccountComponent.prototype.getUserInfo = function () {
        var _this = this;
        this.rest.getUserInfo().subscribe(function (data) {
            console.log(data);
            _this.userInfo = data;
        });
    };
    AccountComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-account',
            template: __webpack_require__(/*! ./account.component.html */ "./src/app/account/account.component.html"),
            styles: [__webpack_require__(/*! ./account.component.scss */ "./src/app/account/account.component.scss")]
        }),
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_rest_service__WEBPACK_IMPORTED_MODULE_2__["RestService"]])
    ], AccountComponent);
    return AccountComponent;
}());



/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _map_map_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./map/map.component */ "./src/app/map/map.component.ts");
/* harmony import */ var _devices_devices_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./devices/devices.component */ "./src/app/devices/devices.component.ts");
/* harmony import */ var _account_account_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./account/account.component */ "./src/app/account/account.component.ts");
/* harmony import */ var _payment_payment_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./payment/payment.component */ "./src/app/payment/payment.component.ts");







var routes = [];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot([
                    {
                        path: 'devices',
                        component: _devices_devices_component__WEBPACK_IMPORTED_MODULE_4__["DevicesComponent"]
                    },
                    {
                        path: 'map',
                        component: _map_map_component__WEBPACK_IMPORTED_MODULE_3__["MapComponent"]
                    },
                    {
                        path: 'account',
                        component: _account_account_component__WEBPACK_IMPORTED_MODULE_5__["AccountComponent"]
                    },
                    {
                        path: 'payment',
                        component: _payment_payment_component__WEBPACK_IMPORTED_MODULE_6__["PaymentComponent"]
                    },
                    {
                        path: '**',
                        component: _devices_devices_component__WEBPACK_IMPORTED_MODULE_4__["DevicesComponent"]
                    }
                ])
            ],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\n<app-navbar></app-navbar>\n\n  <router-outlet></router-outlet>\n\n<!--\n<div class=\"main-view\">\n  <alert type=\"success\">hello</alert>\n</div>\n-->\n"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var keycloak_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! keycloak-angular */ "./node_modules/keycloak-angular/fesm5/keycloak-angular.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");




var AppComponent = /** @class */ (function () {
    function AppComponent(translate, keycloakService) {
        this.translate = translate;
        this.keycloakService = keycloakService;
        this.title = 'web-fo';
        //let userLang = navigator.language;
        translate.setDefaultLang('pl');
    }
    AppComponent.prototype.ngOnInit = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var _a;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_b) {
                switch (_b.label) {
                    case 0: return [4 /*yield*/, this.keycloakService.isLoggedIn()];
                    case 1:
                        if (!_b.sent()) return [3 /*break*/, 3];
                        _a = this;
                        return [4 /*yield*/, this.keycloakService.loadUserProfile()];
                    case 2:
                        _a.userDetails = _b.sent();
                        _b.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ngx_translate_core__WEBPACK_IMPORTED_MODULE_3__["TranslateService"], keycloak_angular__WEBPACK_IMPORTED_MODULE_2__["KeycloakService"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule, HttpLoaderFactory */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpLoaderFactory", function() { return HttpLoaderFactory; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _navbar_navbar_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./navbar/navbar.component */ "./src/app/navbar/navbar.component.ts");
/* harmony import */ var keycloak_angular__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! keycloak-angular */ "./node_modules/keycloak-angular/fesm5/keycloak-angular.js");
/* harmony import */ var _utils_app_init__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./utils/app-init */ "./src/app/utils/app-init.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _devices_devices_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./devices/devices.component */ "./src/app/devices/devices.component.ts");
/* harmony import */ var _map_map_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./map/map.component */ "./src/app/map/map.component.ts");
/* harmony import */ var _account_account_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./account/account.component */ "./src/app/account/account.component.ts");
/* harmony import */ var _payment_payment_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./payment/payment.component */ "./src/app/payment/payment.component.ts");
/* harmony import */ var _asymmetrik_ngx_leaflet__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @asymmetrik/ngx-leaflet */ "./node_modules/@asymmetrik/ngx-leaflet/dist/index.js");
/* harmony import */ var _map_html_marker_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./map/html-marker.component */ "./src/app/map/html-marker.component.ts");
/* harmony import */ var _map_data_service__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./map/data.service */ "./src/app/map/data.service.ts");
/* harmony import */ var ngx_bootstrap__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ngx-bootstrap */ "./node_modules/ngx-bootstrap/esm5/ngx-bootstrap.js");
/* harmony import */ var _syncfusion_ej2_angular_calendars__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @syncfusion/ej2-angular-calendars */ "./node_modules/@syncfusion/ej2-angular-calendars/@syncfusion/ej2-angular-calendars.es5.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _ngx_translate_http_loader__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @ngx-translate/http-loader */ "./node_modules/@ngx-translate/http-loader/fesm5/ngx-translate-http-loader.js");
























var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"],
                _navbar_navbar_component__WEBPACK_IMPORTED_MODULE_7__["NavbarComponent"],
                _devices_devices_component__WEBPACK_IMPORTED_MODULE_11__["DevicesComponent"],
                _map_map_component__WEBPACK_IMPORTED_MODULE_12__["MapComponent"],
                _account_account_component__WEBPACK_IMPORTED_MODULE_13__["AccountComponent"],
                _payment_payment_component__WEBPACK_IMPORTED_MODULE_14__["PaymentComponent"],
                _map_html_marker_component__WEBPACK_IMPORTED_MODULE_16__["HTMLMarkerComponent"],
                _account_account_component__WEBPACK_IMPORTED_MODULE_13__["AccountComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _syncfusion_ej2_angular_calendars__WEBPACK_IMPORTED_MODULE_19__["DatePickerModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_5__["AppRoutingModule"],
                keycloak_angular__WEBPACK_IMPORTED_MODULE_8__["KeycloakAngularModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_10__["HttpClientModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _asymmetrik_ngx_leaflet__WEBPACK_IMPORTED_MODULE_15__["LeafletModule"].forRoot(),
                ngx_bootstrap__WEBPACK_IMPORTED_MODULE_18__["AlertModule"].forRoot(),
                ngx_bootstrap__WEBPACK_IMPORTED_MODULE_18__["ButtonsModule"].forRoot(),
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_20__["TranslateModule"].forRoot({
                    loader: {
                        provide: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_20__["TranslateLoader"],
                        useFactory: HttpLoaderFactory,
                        deps: [_angular_common_http__WEBPACK_IMPORTED_MODULE_10__["HttpClient"]]
                    }
                })
            ],
            providers: [
                _map_data_service__WEBPACK_IMPORTED_MODULE_17__["DataService"],
                {
                    provide: _angular_core__WEBPACK_IMPORTED_MODULE_2__["APP_INITIALIZER"],
                    useFactory: _utils_app_init__WEBPACK_IMPORTED_MODULE_9__["initializer"],
                    multi: true,
                    deps: [keycloak_angular__WEBPACK_IMPORTED_MODULE_8__["KeycloakService"]],
                },
                _angular_common__WEBPACK_IMPORTED_MODULE_3__["DatePipe"],
                { provide: _angular_common__WEBPACK_IMPORTED_MODULE_3__["LocationStrategy"], useClass: _angular_common__WEBPACK_IMPORTED_MODULE_3__["HashLocationStrategy"] }
            ],
            entryComponents: [_map_html_marker_component__WEBPACK_IMPORTED_MODULE_16__["HTMLMarkerComponent"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());

function HttpLoaderFactory(http) {
    return new _ngx_translate_http_loader__WEBPACK_IMPORTED_MODULE_21__["TranslateHttpLoader"](http, './assets/i18n/', '.json');
}


/***/ }),

/***/ "./src/app/devices/devices.component.html":
/*!************************************************!*\
  !*** ./src/app/devices/devices.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-view\">\n\n<div class=\"panel-group\">\n  <div class=\"panel panel-default\">\n    <div class=\"panel-body\">\n      <table class=\"table\">\n        <thead>\n        <tr>\n          <th scope=\"col\">{{ 'screens.devices.table.header.no' | translate }}</th>\n          <th scope=\"col\">{{ 'screens.devices.table.header.name' | translate }}</th>\n          <th scope=\"col\">{{ 'screens.devices.table.header.imei' | translate }}</th>\n          <th scope=\"col\">{{ 'screens.devices.table.header.time' | translate }}</th>\n          <th scope=\"col\">{{ 'screens.devices.table.header.position' | translate }}</th>\n          <th scope=\"col\">{{ 'screens.devices.table.header.battery' | translate }}</th>\n          <th scope=\"col\"></th>\n        </tr>\n        </thead>\n        <tbody>\n        <tr *ngFor=\"let device of devices; index as i\">\n          <th scope=\"row\">{{i + 1}}</th>\n          <td>{{device.name}}</td>\n          <td>{{device.uid}}</td>\n          <td>{{device.lastPositionTime | date:\"yyyy.MM.dd HH:mm\"}}</td>\n          <td>{{device.latitude}};{{device.longitude}}</td>\n          <td><i title=\"{{device.batteryLevel}}%\" class=\"fa\" [ngClass]=\"{\n            'fa-battery-full':device.batteryLevel > 90,\n            'fa-battery-three-quarters':device.batteryLevel > 60 && device.batteryLevel <= 90,\n            'fa-battery-half':device.batteryLevel > 35 && device.batteryLevel <= 60,\n            'fa-battery-quarter':device.batteryLevel <= 35 && device.batteryLevel > 7,\n            'fa-battery-empty':device.batteryLevel <= 7}\"></i> </td>\n          <td><button type=\"button\" class=\"btn btn-primary\" data-dismiss=\"modal\" (click)=\"deleteDevice(device.id)\">{{ 'screens.devices.btn.remove' | translate }}</button></td>\n        </tr>\n        </tbody>\n      </table>\n    </div>\n  </div>\n</div>\n  <a href=\"#\" class=\"btn btn-lg btn-success\" data-toggle=\"modal\" data-target=\"#addDevice\">{{ 'screens.devices.btn.addDevice' | translate }}</a>\n</div>\n\n<div #addDevicePanel class=\"modal fade\" id=\"addDevice\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"addDevice\" aria-hidden=\"true\">\n  <div class=\"modal-dialog\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title\" id=\"myModalLabel\">{{ 'screens.devices.boxAdd.title' | translate }}</h4>\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>\n      <div class=\"modal-body\">\n        <div class=\"md-form md-outline form-lg\">\n          <input #inputName id=\"form-lg\" class=\"form-control form-control-lg\" type=\"text\" mdbInput>\n          <label for=\"form-lg\">{{ 'screens.devices.boxAdd.name' | translate }}</label>\n        </div>\n        <div class=\"md-form md-outline form-lg\">\n          <input #inputImei id=\"form-lg\" class=\"form-control form-control-lg\" type=\"text\" mdbInput>\n          <label for=\"form-lg\">{{ 'screens.devices.boxAdd.noImei' | translate }}</label>\n        </div>\n      </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">{{ 'screens.devices.boxAdd.cancel' | translate }}</button>\n        <button type=\"button\" class=\"btn btn-primary\" data-dismiss=\"modal\" (click)=\"addImei()\">{{ 'screens.devices.boxAdd.save' | translate }}</button>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/devices/devices.component.scss":
/*!************************************************!*\
  !*** ./src/app/devices/devices.component.scss ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2RldmljZXMvZGV2aWNlcy5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/devices/devices.component.ts":
/*!**********************************************!*\
  !*** ./src/app/devices/devices.component.ts ***!
  \**********************************************/
/*! exports provided: DevicesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DevicesComponent", function() { return DevicesComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../rest.service */ "./src/app/rest.service.ts");



var DevicesComponent = /** @class */ (function () {
    function DevicesComponent(rest) {
        this.rest = rest;
        this.devices = [];
    }
    DevicesComponent.prototype.ngOnInit = function () {
        this.getDevices();
    };
    DevicesComponent.prototype.getDevices = function () {
        var _this = this;
        this.devices = [];
        this.rest.getDevices().subscribe(function (data) {
            console.log(data);
            _this.devices = data;
        });
    };
    DevicesComponent.prototype.addImei = function () {
        var _this = this;
        this.rest.addDevice(this.inputName.nativeElement.value, this.inputImei.nativeElement.value).subscribe(function (data) {
            _this.inputName.nativeElement.value = '';
            _this.inputImei.nativeElement.value = '';
            _this.getDevices();
        });
    };
    DevicesComponent.prototype.deleteDevice = function (id) {
        var _this = this;
        this.rest.deleteDevice(id).subscribe(function (data) {
            _this.getDevices();
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('inputName'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], DevicesComponent.prototype, "inputName", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('inputImei'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], DevicesComponent.prototype, "inputImei", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('addDevicePanel'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], DevicesComponent.prototype, "addDevicePanel", void 0);
    DevicesComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-devices',
            template: __webpack_require__(/*! ./devices.component.html */ "./src/app/devices/devices.component.html"),
            styles: [__webpack_require__(/*! ./devices.component.scss */ "./src/app/devices/devices.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_rest_service__WEBPACK_IMPORTED_MODULE_2__["RestService"]])
    ], DevicesComponent);
    return DevicesComponent;
}());



/***/ }),

/***/ "./src/app/gpsm-common.service.ts":
/*!****************************************!*\
  !*** ./src/app/gpsm-common.service.ts ***!
  \****************************************/
/*! exports provided: GpsmCommonService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GpsmCommonService", function() { return GpsmCommonService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");



var GpsmCommonService = /** @class */ (function () {
    function GpsmCommonService(translate) {
        this.translate = translate;
    }
    GpsmCommonService.prototype.useLanguage = function (language) {
        this.translate.use(language);
    };
    GpsmCommonService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__["TranslateService"]])
    ], GpsmCommonService);
    return GpsmCommonService;
}());



/***/ }),

/***/ "./src/app/map/data.service.ts":
/*!*************************************!*\
  !*** ./src/app/map/data.service.ts ***!
  \*************************************/
/*! exports provided: Marker, DataService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Marker", function() { return Marker; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DataService", function() { return DataService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../rest.service */ "./src/app/rest.service.ts");



var Marker = /** @class */ (function () {
    function Marker() {
    }
    return Marker;
}());

var DataService = /** @class */ (function () {
    function DataService(rest) {
        this.rest = rest;
        this.markers = [];
    }
    DataService.prototype.getMarkers = function () {
        this.getDevicesLastPositions();
        return this.markers;
    };
    DataService.prototype.getMarkerById = function (id) {
        return this.markers.filter(function (entry) { return entry.id === id; })[0];
    };
    DataService.prototype.changeMarkerData = function () {
        for (var _i = 0, _a = this.markers; _i < _a.length; _i++) {
            var marker = _a[_i];
            // just add a random number at the end
            marker.description = "Some random value " + Math.random() * 100;
        }
    };
    DataService.prototype.getPointsByDeviceAndDate = function (deviceUid, date) {
        return this.rest.getPointsByDeviceAndDate(deviceUid, date);
    };
    DataService.prototype.getDevicesLastPositions = function () {
        var _this = this;
        this.rest.getDevicesLastPositions().subscribe(function (data) {
            _this.markers = [];
            for (var dataKey in data) {
                var entry = data[dataKey];
                var id = Number(dataKey);
                var lat = Number(entry['lat']);
                var lng = Number(entry['lng']);
                var time = entry['time'];
                var date = new Date(time);
                _this.markers.push({
                    id: id,
                    name: entry['deviceName'],
                    description: 'lat=' + lat + '; lng=' + lng + '; ' + date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear() + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds(),
                    position: [lat, lng]
                });
            }
        });
    };
    DataService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_rest_service__WEBPACK_IMPORTED_MODULE_2__["RestService"]])
    ], DataService);
    return DataService;
}());



/***/ }),

/***/ "./src/app/map/html-marker.component.ts":
/*!**********************************************!*\
  !*** ./src/app/map/html-marker.component.ts ***!
  \**********************************************/
/*! exports provided: HTMLMarkerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HTMLMarkerComponent", function() { return HTMLMarkerComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var HTMLMarkerComponent = /** @class */ (function () {
    function HTMLMarkerComponent() {
    }
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], HTMLMarkerComponent.prototype, "data", void 0);
    HTMLMarkerComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'html-marker',
            template: "\n    <h3>{{ data.name }}</h3>\n    <p>\n      {{ data.description }}\n    </p>\n  "
        })
    ], HTMLMarkerComponent);
    return HTMLMarkerComponent;
}());



/***/ }),

/***/ "./src/app/map/map.component.html":
/*!****************************************!*\
  !*** ./src/app/map/map.component.html ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<span class=\"center\">\n  <div class=\"row-fluid\" style=\"margin-left: 20px\">\n        <div class=\"input-group date text-center\" data-provide=\"datepicker\">\n          <div class=\"form-check\">\n            <input class=\"form-check-input\" type=\"radio\" name=\"exampleRadios\" id=\"positionsActualId\" value=\"positionsActual\" checked (change)=\"onSelectionRadioChange($event)\">\n            <label class=\"form-check-label\" for=\"positionsActualId\">{{ 'screens.map.currentPositions' | translate }}</label>\n          </div>\n          <div class=\"form-check\">\n            <input class=\"form-check-input\" type=\"radio\" name=\"exampleRadios\" id=\"positionsHistoricalId\" value=\"positionsHistorical\" (change)=\"onSelectionRadioChange($event)\">\n            <label class=\"form-check-label\" for=\"positionsHistoricalId\">{{ 'screens.map.historicData' | translate }}</label>\n          </div>\n\n           <ejs-datepicker #ejDatePicker id='datepicker' format='      dd-MM-yyyy' placeholder=\"{{ 'screens.map.selectDay' | translate }}\" [value]='dateValue' style=\"text-align: right; visibility: hidden; float; margin-left: 20px\"\n                           [min]='minDate' [max]='maxDate' (change)='onDatePickerChange($event)'></ejs-datepicker>\n\n         </div>\n  </div>\n</span>\n<div id=\"div-map-id\" style=\"height: 90%; width: 100%; visibility: hidden\"\n     leaflet\n     [leafletOptions]=\"options\"\n     (leafletMapReady)=\"onMapReady($event)\">\n</div>\n"

/***/ }),

/***/ "./src/app/map/map.component.scss":
/*!****************************************!*\
  !*** ./src/app/map/map.component.scss ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".map {\n  height: 100%;\n  padding: 0; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL3NlYmEvZGV2ZWxvcC93b3Jrc3BhY2UvZ3BzbS93ZWItZm8vc3JjL2FwcC9tYXAvbWFwLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsWUFBWTtFQUNaLFVBQVUsRUFBQSIsImZpbGUiOiJzcmMvYXBwL21hcC9tYXAuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubWFwIHtcbiAgaGVpZ2h0OiAxMDAlO1xuICBwYWRkaW5nOiAwO1xufVxuIl19 */"

/***/ }),

/***/ "./src/app/map/map.component.ts":
/*!**************************************!*\
  !*** ./src/app/map/map.component.ts ***!
  \**************************************/
/*! exports provided: MapComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapComponent", function() { return MapComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var leaflet__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! leaflet */ "./node_modules/leaflet/dist/leaflet-src.js");
/* harmony import */ var leaflet__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(leaflet__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _rest_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../rest.service */ "./src/app/rest.service.ts");
/* harmony import */ var _html_marker_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./html-marker.component */ "./src/app/map/html-marker.component.ts");
/* harmony import */ var _data_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./data.service */ "./src/app/map/data.service.ts");







var MapComponent = /** @class */ (function () {
    function MapComponent(rest, dataService, resolver, injector, datepipe) {
        this.rest = rest;
        this.dataService = dataService;
        this.resolver = resolver;
        this.injector = injector;
        this.datepipe = datepipe;
        this.streetMaps = Object(leaflet__WEBPACK_IMPORTED_MODULE_3__["tileLayer"])('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', 
        //'https://maps.tilehosting.com/styles/streets/{z}/{x}/{y}.png?key=5jjQ1gdY5zIX30C40swM',
        {
            detectRetina: true
        });
        this.markers = [];
        this.options = {
            layers: [this.streetMaps /*, this.route*/],
            zoom: 10,
            center: Object(leaflet__WEBPACK_IMPORTED_MODULE_3__["latLng"])(0.0, 0.0)
        };
        this.centerSet = false;
        this.minDate = new Date('05/01/2019');
        this.maxDate = new Date();
        this.dateValue = new Date();
    }
    MapComponent.prototype.onMapReady = function (map) {
        this.map = map;
        this.addMarker(this.rest.getDevicePositionsLast());
        this.startAutrorefreshPositions();
    };
    MapComponent.prototype.startAutrorefreshPositions = function () {
        var _this = this;
        this.addMarker(this.rest.getDevicePositionsLast());
        this.refreshIntervalId = setInterval(function () { return _this.addMarker(_this.rest.getDevicePositionsLast()); }, 10000);
    };
    MapComponent.prototype.getMarkerIconByColor = function (color) {
        var url = 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-' + color + '.png';
        return new L.Icon({ iconUrl: url });
    };
    MapComponent.prototype.removeMarker = function (marker) {
        // remove it from the array meta objects
        //const idx = this.markers.indexOf(marker);
        //this.markers.splice(idx, 1);
        // remove the marker from the map
        marker.markerInstance.removeFrom(this.map);
        // destroy the component to avoid memory leaks
        //   marker.componentInstance.destroy();
    };
    // This is a lifecycle method of an Angular component which gets invoked whenever for
    // our component change detection is triggered
    MapComponent.prototype.ngDoCheck = function () {
        // since our components are dynamic, we need to manually iterate over them and trigger
        // change detection on them.
        this.markers.forEach(function (entry) {
            entry.componentInstance.changeDetectorRef.detectChanges();
        });
    };
    MapComponent.prototype.onSelectionRadioChange = function (event) {
        if (document.getElementById('positionsHistoricalId')['checked'] === true) {
            document.getElementById('datepicker').style.visibility = 'visible';
            clearInterval(this.refreshIntervalId);
            this.showHistoryMarkers();
        }
        else {
            document.getElementById('datepicker').style.visibility = 'hidden';
            this.startAutrorefreshPositions();
        }
    };
    MapComponent.prototype.onDatePickerChange = function (event) {
        this.showHistoryMarkers();
    };
    MapComponent.prototype.toYYYYMMDD = function (date) {
        var month = date.getMonth() + 1;
        month = '0' + month;
        month = month.substring(month.length - 2, month.length);
        var day = date.getDate();
        day = '0' + day;
        day = day.substring(day.length - 2, day.length);
        return date.getFullYear() + '' + month + '' + day;
    };
    MapComponent.prototype.toHHmmss = function (date) {
        var hours = date.getHours() + 1;
        hours = '0' + hours;
        hours = hours.substring(hours.length - 2, hours.length);
        var minutes = date.getMinutes();
        minutes = '0' + minutes;
        minutes = minutes.substring(minutes.length - 2, minutes.length);
        var seconds = date.getSeconds();
        seconds = '0' + seconds;
        seconds = seconds.substring(seconds.length - 2, seconds.length);
        return hours + '' + minutes + '' + seconds;
    };
    MapComponent.prototype.showHistoryMarkers = function () {
        if (this.datepicker.value) {
            var dateString = this.toYYYYMMDD(this.datepicker.value);
            var timeSinceString = this.toHHmmss(this.datepicker.value);
            var timeTillString = this.toHHmmss(new Date(this.datepicker.value.getTime() + 3600000));
            this.addMarker(this.rest.getDevicePositions(dateString, timeSinceString, timeTillString));
        }
    };
    MapComponent.prototype.addMarker = function (devicePositionsPromise) {
        var _this = this;
        devicePositionsPromise.subscribe(function (value) {
            if (!_this.centerSet) {
                if (!document.getElementById("devices-legend") && value.devices.length !== 0) {
                    var legend = L.control({ position: 'topleft' });
                    legend.onAdd = function (map) {
                        var div = L.DomUtil.create('div', 'info legend');
                        div.setAttribute('id', 'devices-legend');
                        var labels = ['<strong>Devices:</strong>'];
                        for (var _i = 0, _a = value.devices; _i < _a.length; _i++) {
                            var device = _a[_i];
                            div.innerHTML += labels.push('<i style="background:' + device.iconColor + '">&nbsp;</i>' + device.name);
                        }
                        div.innerHTML = labels.join('<br>');
                        return div;
                    };
                    legend.addTo(_this.map);
                }
            }
            if (_this.markers) {
                // remove markers
                _this.markers.forEach(function (m) { return _this.removeMarker(m); });
            }
            var divMap = document.getElementById('div-map-id');
            for (var _i = 0, _a = value.devices; _i < _a.length; _i++) {
                var device = _a[_i];
                // dynamically instantiate a HTMLMarkerComponent
                var factory = _this.resolver.resolveComponentFactory(_html_marker_component__WEBPACK_IMPORTED_MODULE_5__["HTMLMarkerComponent"]);
                // we need to pass in the dependency injector
                var component = factory.create(_this.injector);
                // wire up the @Input() or plain variables (doesn't have to be strictly an @Input())
                component.instance.data = {};
                // we need to manually trigger change detection on our in-memory component
                // s.t. its template syncs with the data we passed in
                component.changeDetectorRef.detectChanges();
                for (var _b = 0, _c = device.positions; _b < _c.length; _b++) {
                    var position = _c[_b];
                    if (!_this.centerSet) {
                        _this.map.setView(Object(leaflet__WEBPACK_IMPORTED_MODULE_3__["latLng"])(position.lat, position.lng));
                        _this.centerSet = true;
                    }
                    var m = Object(leaflet__WEBPACK_IMPORTED_MODULE_3__["marker"])(position, { icon: _this.getMarkerIconByColor(device.iconColor) });
                    // pass in the HTML from our dynamic component
                    var popupContent = component.location.nativeElement;
                    // add popup functionality
                    m.bindPopup(popupContent).openPopup();
                    // finally add the marker to the map s.t. it is visible
                    m.addTo(_this.map).bindPopup(position.description);
                    // add a metadata object into a local array which helps us
                    // keep track of the instantiated markers for removing/disposing them later
                    _this.markers.push({
                        name: device.name,
                        markerInstance: m,
                        componentInstance: component
                    });
                }
            }
            if (value.devices.length === 0) {
                _this.map.setView(Object(leaflet__WEBPACK_IMPORTED_MODULE_3__["latLng"])(52.2207622, 21.0484505));
                _this.map.setZoom(7);
            }
            if (value.success === true) {
                divMap.style.visibility = 'visible';
            }
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('positionsActualId'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], MapComponent.prototype, "positionsActual", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('positionsHistoricalId'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], MapComponent.prototype, "positionsHistorical", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('ejDatePicker'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], MapComponent.prototype, "datepicker", void 0);
    MapComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-map',
            template: __webpack_require__(/*! ./map.component.html */ "./src/app/map/map.component.html"),
            styles: [__webpack_require__(/*! ./map.component.scss */ "./src/app/map/map.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_rest_service__WEBPACK_IMPORTED_MODULE_4__["RestService"], _data_service__WEBPACK_IMPORTED_MODULE_6__["DataService"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ComponentFactoryResolver"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["Injector"], _angular_common__WEBPACK_IMPORTED_MODULE_2__["DatePipe"]])
    ], MapComponent);
    return MapComponent;
}());



/***/ }),

/***/ "./src/app/navbar/navbar.component.html":
/*!**********************************************!*\
  !*** ./src/app/navbar/navbar.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-expand-lg navbar-dark bg-dark\">\n  <a class=\"navbar-brand\" routerLink=\"#\">\n    <img src=\"./assets/img/logo.png\" style=\"display: inline-block;\">\n  </a>\n  <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarNavAltMarkup\" aria-controls=\"navbarNavAltMarkup\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\n    <span class=\"navbar-toggler-icon\"></span>\n  </button>\n  <div class=\"collapse navbar-collapse\" id=\"navbarNavAltMarkup\">\n    <div class=\"navbar-nav\">\n      <li class=\"nav-item\"><a class=\"nav-link\" routerLink=\"/map\">{{ 'menu.map' | translate }}</a></li>\n      <li class=\"nav-item\"><a class=\"nav-link\" routerLink=\"/devices\">{{ 'menu.devices' | translate }}</a></li>\n      <li class=\"nav-item\"><a class=\"nav-link\" routerLink=\"/account\">{{ 'menu.account' | translate }}</a></li>\n    </div>\n  </div>\n\n  <div class=\"navbar-text\">\n    <div class=\"userDetails\" *ngIf=\"userDetails\">\n      <button class=\"btn flag-icon flag-icon-pl\" style=\"margin-right: 5px\" (click)=\"gpsmCommon.useLanguage('pl')\"></button>\n      <button class=\"btn flag-icon flag-icon-gb\" style=\"margin-right: 20px\" (click)=\"gpsmCommon.useLanguage('en')\"></button>\n      {{userDetails.username}} &nbsp;&nbsp;&nbsp; <button type=\"button\" class=\"btn btn-light\" (click)=\"doLogout()\">{{ 'menu.logout' | translate }}</button>\n    </div>\n  </div>\n</nav>\n"

/***/ }),

/***/ "./src/app/navbar/navbar.component.scss":
/*!**********************************************!*\
  !*** ./src/app/navbar/navbar.component.scss ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL25hdmJhci9uYXZiYXIuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/navbar/navbar.component.ts":
/*!********************************************!*\
  !*** ./src/app/navbar/navbar.component.ts ***!
  \********************************************/
/*! exports provided: NavbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavbarComponent", function() { return NavbarComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var keycloak_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! keycloak-angular */ "./node_modules/keycloak-angular/fesm5/keycloak-angular.js");
/* harmony import */ var _gpsm_common_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../gpsm-common.service */ "./src/app/gpsm-common.service.ts");




var NavbarComponent = /** @class */ (function () {
    function NavbarComponent(gpsmCommon, keycloakService) {
        this.gpsmCommon = gpsmCommon;
        this.keycloakService = keycloakService;
        //  route.params.subscribe(params => console.log("side menu id parameter",params['id']));
    }
    NavbarComponent.prototype.ngOnInit = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var _a;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_b) {
                switch (_b.label) {
                    case 0: return [4 /*yield*/, this.keycloakService.isLoggedIn()];
                    case 1:
                        if (!_b.sent()) return [3 /*break*/, 3];
                        _a = this;
                        return [4 /*yield*/, this.keycloakService.loadUserProfile()];
                    case 2:
                        _a.userDetails = _b.sent();
                        _b.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    NavbarComponent.prototype.doLogout = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.keycloakService.logout('https://pilnuj.com/web-fo')];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    NavbarComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-navbar',
            template: __webpack_require__(/*! ./navbar.component.html */ "./src/app/navbar/navbar.component.html"),
            styles: [__webpack_require__(/*! ./navbar.component.scss */ "./src/app/navbar/navbar.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_gpsm_common_service__WEBPACK_IMPORTED_MODULE_3__["GpsmCommonService"], keycloak_angular__WEBPACK_IMPORTED_MODULE_2__["KeycloakService"]])
    ], NavbarComponent);
    return NavbarComponent;
}());



/***/ }),

/***/ "./src/app/payment/payment.component.html":
/*!************************************************!*\
  !*** ./src/app/payment/payment.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-view\">\n\n  <div class=\"row\">\n    <div class=\"col-2\"></div>\n\n    <div class=\"col-8\">\n\n      <table class=\"table\">\n        <thead>\n        <tr>\n          <th scope=\"col\"></th>\n          <th scope=\"col\">Basic</th>\n          <th scope=\"col\">Permium 30</th>\n          <th scope=\"col\">Premium 180</th>\n        </tr>\n        </thead>\n        <tbody>\n\n        <tr>\n          <th scope=\"row\">Ilość śledzonych urządzeń</th>\n          <td>1</td>\n          <td>3</td>\n          <td>3</td>\n        </tr>\n\n        <tr>\n          <th scope=\"row\">Automatyczne odświeżenie mapy</th>\n          <td>tak</td>\n          <td>tak</td>\n          <td>tak</td>\n        </tr>\n\n        <tr>\n          <th scope=\"row\">Przeglądanie historycznych danych</th>\n          <td>nie</td>\n          <td>tak</td>\n          <td>tak</td>\n        </tr>\n\n        <tr>\n          <th scope=\"row\">Ważność</th>\n          <td>bezterminowo</td>\n          <td>30 dni</td>\n          <td>180 dni</td>\n        </tr>\n\n        <tr>\n          <th scope=\"row\">Cena</th>\n          <td>darmowy</td>\n          <td>30 zł</td>\n          <td>99 zł</td>\n        </tr>\n        <tr *ngIf=\"links.length == 2\">\n          <th scope=\"row\"></th>\n          <td></td>\n         <!-- <td><a class=\"btn btn-primary\" href=\"{{links[0].link}}\">Kup Premium 30</a></td>\n          <td><a class=\"btn btn-primary\" href=\"{{links[1].link}}\">Kup Premium 180</a></td>-->\n          <td><a class=\"btn btn-primary\">Kup Premium 30</a></td>\n          <td><a class=\"btn btn-primary\">Kup Premium 180</a></td>\n        </tr>\n        </tbody>\n      </table>\n\n      <!--\n      <h3>Strona jest jeszcze w budowie. Obecnie dostępne są płatności testowe, bez rzeczywistego pobierania pieniędzy z konta.</h3>\n      -->\n\n    </div>\n    <div class=\"col-2\"></div>\n  </div>\n\n</div>\n"

/***/ }),

/***/ "./src/app/payment/payment.component.scss":
/*!************************************************!*\
  !*** ./src/app/payment/payment.component.scss ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BheW1lbnQvcGF5bWVudC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/payment/payment.component.ts":
/*!**********************************************!*\
  !*** ./src/app/payment/payment.component.ts ***!
  \**********************************************/
/*! exports provided: PaymentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaymentComponent", function() { return PaymentComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../rest.service */ "./src/app/rest.service.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");




var PaymentComponent = /** @class */ (function () {
    function PaymentComponent(rest) {
        this.rest = rest;
        this.env = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"];
        this.links = [];
    }
    PaymentComponent.prototype.ngOnInit = function () {
        this.getLinks();
    };
    PaymentComponent.prototype.getLinks = function () {
        var _this = this;
        this.rest.getPaymentLinks().subscribe(function (data) {
            console.log(data);
            _this.links = data;
        });
    };
    PaymentComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-payment',
            template: __webpack_require__(/*! ./payment.component.html */ "./src/app/payment/payment.component.html"),
            styles: [__webpack_require__(/*! ./payment.component.scss */ "./src/app/payment/payment.component.scss")]
        }),
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_rest_service__WEBPACK_IMPORTED_MODULE_2__["RestService"]])
    ], PaymentComponent);
    return PaymentComponent;
}());



/***/ }),

/***/ "./src/app/rest.service.ts":
/*!*********************************!*\
  !*** ./src/app/rest.service.ts ***!
  \*********************************/
/*! exports provided: RestService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RestService", function() { return RestService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../environments/environment */ "./src/environments/environment.ts");






var endpoint = _environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].serverEndpoint;
var httpOptions = {
    headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
        'Content-Type': 'application/json'
    })
};
var RestService = /** @class */ (function () {
    function RestService(http) {
        this.http = http;
    }
    RestService.prototype.extractData = function (res) {
        var body = res;
        return body || {};
    };
    RestService.prototype.handleError = function (operation, result) {
        if (operation === void 0) { operation = 'operation'; }
        return function (error) {
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead
            // TODO: better job of transforming error for user consumption
            console.log(operation + " failed: " + error.message);
            // Let the app keep running by returning an empty result.
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(result);
        };
    };
    RestService.prototype.getDevices = function () {
        return this.http.get(endpoint + 'devices/user-devices/list').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData));
    };
    RestService.prototype.getDevicesLastPositions = function () {
        return this.http.get(endpoint + 'devices/user-devices/last-positions').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData));
    };
    RestService.prototype.getPublicDevicesList = function () {
        return this.http.get(endpoint + 'public/device/list').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData));
    };
    RestService.prototype.getPaymentLinks = function () {
        return this.http.get(endpoint + 'payments').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData));
    };
    RestService.prototype.getUserInfo = function () {
        return this.http.get(endpoint + 'user/info').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData));
    };
    RestService.prototype.getPointsByDeviceAndDate = function (deviceUid, date) {
        return this.http.get(endpoint + '/api/devices/device/' + deviceUid + '/points/' + date).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData));
    };
    RestService.prototype.getDevicePositionsLast = function () {
        return this.http.get(endpoint + 'v1/device/positions/last').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData));
    };
    RestService.prototype.getDevicePositions = function (sinceDate, sinceTime, tillTime) {
        return this.http.get(endpoint + 'v1/device/positions/' + sinceDate + '/000000/235959').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData));
    };
    RestService.prototype.addDevice = function (name, imei) {
        var options = { headers: { 'Content-Type': 'application/json' } };
        return this.http.post(endpoint + 'v1/device', JSON.stringify({ 'name': name, 'uid': imei }), options).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData));
    };
    RestService.prototype.deleteDevice = function (id) {
        return this.http.delete(endpoint + 'v1/device/' + id).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(this.extractData));
    };
    RestService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], RestService);
    return RestService;
}());



/***/ }),

/***/ "./src/app/utils/app-init.ts":
/*!***********************************!*\
  !*** ./src/app/utils/app-init.ts ***!
  \***********************************/
/*! exports provided: initializer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "initializer", function() { return initializer; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");


function initializer(keycloak) {
    var _this = this;
    return function () {
        return new Promise(function (resolve, reject) { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var error_1;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, keycloak.init({
                                config: _environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].keycloak,
                                initOptions: {
                                    onLoad: 'login-required',
                                    checkLoginIframe: false
                                },
                                bearerExcludedUrls: ['/app/api/public/device/list']
                            })];
                    case 1:
                        _a.sent();
                        resolve();
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _a.sent();
                        reject(error_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        }); });
    };
}


/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
// Add here your keycloak setup infos
var keycloakConfig = {
    url: 'https://pilnuj.com/auth',
    realm: 'pilnuj.com',
    clientId: 'application-cli'
};
var environment = {
    production: false,
    keycloak: keycloakConfig,
    serverEndpoint: 'https://pilnuj.com/app/api/'
    //serverEndpoint: 'http://localhost:4200/api/'
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/seba/develop/workspace/gpsm/web-fo/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map