<section class="page-title bg-title overlay-dark">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<div class="title">
					<h3>System</h3>
					<p>Jak działa monitorowanie GPS?</p>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section about">
	<div class="container">
		<div class="row">
			<div class="col-lg-1 col-md-1 align-self-center">
				<div class="image-block bg-about">
					
				</div>
			</div>
			<div class="col-lg-10 col-md-10 align-self-center">
				<div class="content-block">
					<img src="/images/system/system-diagram-with-api.png"/>
				</div>
			</div>
			<div class="col-lg-1 col-md-1 align-self-center">
				<div class="image-block bg-about">
					
				</div>
			</div>
		</div>
	</div>
</section>

