<section class="page-title bg-title overlay-dark">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<div class="title">
					<h3>Home</h3>
				</div>
			</div>
		</div>
	</div>
</section>


<section class="section pricing">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="section-title">
					<h3><span class="alternate">Supported</span> devices,</h3>
                                        <p>whose positions you can track</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-6">				
				<div class="pricing-item featured">
					<div class="pricing-heading">
						<!-- Title -->
						<div class="title">
							<h6>Android OS</h6>
						</div>
                                                <div style="color: white">Android devices with a GPS module and ability to communicate via the Internet, i.e. almost all smartphones and tablets.</div>
					</div>
					<div class="pricing-body">
						<!-- Feature List -->
						<ul class="feature-list m-0 p-0">
<li><p><span class="fa fa-check-circle available"></span><a href="https://pilnuj.com/auth/realms/pilnuj.com/protocol/openid-connect/registrations?response_type=code&client_id=application-cli&redirect_uri=https%3A%2F%2Fpilnuj.com">Register</a></p>
<li><p><span class="fa fa-check-circle available"></span>Download the free application <a href="https://play.google.com/store/apps/details?id=com.pilnuj.android.gpstx&gl=PL">GPS-Tx</a> na Android OS</p>
<li><p><span class="fa fa-check-circle available"></span>On the device with application installed, go to "Settings> Battery> Startup" and for the application "GPS-Tx" enable "Manual management" of the application</p>
<li><p><span class="fa fa-check-circle available"></span>Run GPS-Tx, enter the login and password you used to register on the site. Then click "ENABLE GPS TRACKING" (Polish version "WŁĄCZ ŚLEDZENIE GPS")</p>
<li><p><span class="fa fa-check-circle available"></span>Open site <a href="https://pilnuj.com/web-fo/#/map">pilnuj.com</a> and after logging in, you can monitor locations of your devices</p>	
					</ul>
					</div>					
				</div>
			</div>
			<div class="col-6">				
				<div class="pricing-item featured">
					<div class="pricing-heading">
						<!-- Title -->
						<div class="title">
							<h6>Trackers TK-102, TK-103 and compatible</h6>
                                                </div>
                                                <div style="color: white">Popular trackers from the Tk-102 and Tk-103 series supporting GPRS mode.<br></div>
					</div>
					<div class="pricing-body">
						<!-- Feature List -->
						<ul class="feature-list m-0 p-0">
							<li><p><span class="fa fa-check-circle available"></span><a href="https://pilnuj.com/auth/realms/pilnuj.com/protocol/openid-connect/registrations?response_type=code&client_id=application-cli&redirect_uri=https%3A%2F%2Fpilnuj.com">Register</a></p>
<li><p><span class="fa fa-check-circle available"></span>Buy and insert SIM card into tracker</p>
<li><p><span class="fa fa-check-circle available"></span>Open site <a href="https://pilnuj.com/web-fo/#/device">https://pilnuj.com/web-fo/#/device</a> and add your device</p>
<li><p><span class="fa fa-check-circle available"></span>Send commands to tracker, Wait for confirmation each time:
<ul>
<li><b>begin123456</b> - command begin123456 (123456 is default password) 
<li><b>apn123456 internet</b> - the parameter depends on SIM card operator, additional parameters may be required
<li><b>adminip123456 51.68.139.189 5002</b> 
<li><b>gprs123456</b> 
<li><b>fix030s***n123456</b> 
</ul>
</p>
<li><p><span class="fa fa-check-circle available"></span>Open site <a href="https://pilnuj.com/web-fo/#/map">pilnuj.com</a> and after logging in, you can monitor locations of your devices</p>
						</ul>
					</div>					
				</div>
			</div>
		</div>
	</div>
</section>

