<section class="page-title bg-title overlay-dark">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<div class="title">
					<h3>DIY GPS tracker</h3>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section about">
	<div class="container">
		<div class="row">
			<div class="col-lg-1 col-md-1 align-self-center">
				<div class="bg-about">
					
				</div>
			</div>
			<div class="col-lg-10 col-md-10 align-self-center">
				<div class="content-block">
		
		<h5>Arduino Uno</h5>
		<br>
		
<div>

<div>
Previous step: <a href="/blog/01-introduction">How to build your own GPS/GSM tracker?</a>
<br>
<br>
</div>

<div>
To start programming Arduino we need:
<ul>
     <li>Ardunio with USB cable to connect to PC, in the case of Arduino Uno it will be a cable with A and B type connectors
     <li>free development environment (IDE), which we download from <a href="https://www.arduino.cc/en/main/software">https://www.arduino.cc/en/main/software</a> and install on our computer
</ul>
</div>

<div>
We connect our Arduino to the PC. At least the diode marked "ON" should light up.
</div>


<div>
<br>
<center>
<img src="/blog/02-arduino/arduino-uno-connected.jpg" width="60%" height="60%" />
</center>
<br>
<i>Arduino Uno</i>
<br>
<br>
<br>
</div>



<div>
Next we run Arduino IDE, call command <i>arduino</i>:<br>
<br>
<center>
<img src="/blog/02-arduino/arduino-ide.png" width="60%" height="60%" />
</center>
<br>

and configure the connection with Arduino board, for this purpose from the menu select<br>
device: <i>Tools -> Board -> Arduino Uno</i><br>
connection port: <i>Tools -> Port -> [valid port name]</i><br>
Port name depends on operating system and configuration<br>
<br>
</div>

<div>
Now, we can start write first code, we'll:
<ul>
<li>write a few lines of code, we'll use example form Arduino IDE ;)
<li>compile code
<li>upload code in Arduino ROM
</ul>

<br>
How can we check our Arduino is working? The board has an LED connected with 13 pin. On the original Arduino board Uno is marked with the letter "L". By giving a high voltage to pin 13, the diode lights up, the low state turns the diode off.<br>
<br>

Where will we get the sample code from? Open IDE environment contains a whole set of examples. We'll open the code needs by choosing from the menu
<i>File -> Examples -> 01.Basics -> Blink</i>
<br>
<br> 
A new window will open with a code:


<!-- HTML generated using hilite.me --><div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><table><tr><td><pre style="margin: 0; line-height: 125%"> 1
 2
 3
 4
 5
 6
 7
 8
 9
10
11
12
13
14
15
16
17
18
19
20
21
22
23
24</pre></td><td><pre style="margin: 0; line-height: 125%"><span style="color: #888888">/*</span>
<span style="color: #888888">  Blink</span>
<span style="color: #888888">  Turns on an LED on for one second, then off for one second, repeatedly.</span>
<span style="color: #888888"> </span>
<span style="color: #888888">  This example code is in the public domain.</span>
<span style="color: #888888"> */</span>
 
<span style="color: #888888">// Pin 13 has an LED connected on most Arduino boards.</span>
<span style="color: #888888">// give it a name:</span>
<span style="color: #333399; font-weight: bold">int</span> led <span style="color: #333333">=</span> <span style="color: #0000DD; font-weight: bold">13</span>;

<span style="color: #888888">// the setup routine runs once when you press reset:</span>
<span style="color: #333399; font-weight: bold">void</span> <span style="color: #0066BB; font-weight: bold">setup</span>() {                
  <span style="color: #888888">// initialize the digital pin as an output.</span>
  pinMode(led, OUTPUT);     
}

<span style="color: #888888">// the loop routine runs over and over again forever:</span>
<span style="color: #333399; font-weight: bold">void</span> <span style="color: #0066BB; font-weight: bold">loop</span>() {
  digitalWrite(led, HIGH);   <span style="color: #888888">// turn the LED on (HIGH is the voltage level)</span>
  delay(<span style="color: #0000DD; font-weight: bold">1000</span>);               <span style="color: #888888">// wait for a second</span>
  digitalWrite(led, LOW);    <span style="color: #888888">// turn the LED off by making the voltage LOW</span>
  delay(<span style="color: #0000DD; font-weight: bold">1000</span>);               <span style="color: #888888">// wait for a second</span>
}
</pre></td></tr></table></div>

<br>
By changing the values delay(...) we can change LED blinks on and off.<br>
<br>
We compile our program using the button:<br>
<center>
<img src="/blog/02-arduino/arduino-btn-verify.png" />
</center>
<br>
If we didn't make any mistakes then the compilation was successful and then we can send the compiled code to the Arduino ROM by clicking on:<br>
<center>
<img src="/blog/02-arduino/arduino-btn-upload.png" />
</center>
<br>

If everything went correctly, after a few seconds our LED should start flashing in accordance with the times programmed by us, and the uploaded program will be remembered even after turning Arduino off and on again.<br>
<br>

<div>
<br>
<br>
<div>
Links:
<ul>
<li>Arduino Uno - <a href="https://store.arduino.cc/arduino-uno-rev3">https://store.arduino.cc/arduino-uno-rev3</a>
<li>Arduino IDE - <a href="https://www.arduino.cc/en/Main/Software">https://www.arduino.cc/en/Main/Software</a>
</ul>

<br>
<br>
Next step: <a href="/blog/03-gps-module/index.php">GPS module, NMEA protocol</a>

</div>


</div>
			</div>
			
		</div>
	</div>
</section>

