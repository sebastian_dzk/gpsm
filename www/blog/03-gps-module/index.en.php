<section class="page-title bg-title overlay-dark">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<div class="title">
					<h3>DIY GPS tracker</h3>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section about">
	<div class="container">
		<div class="row">
			<div class="col-lg-1 col-md-1 align-self-center">
				<div class="bg-about">
					
				</div>
			</div>
			<div class="col-lg-10 col-md-10 align-self-center">
				<div class="content-block">
		<h5>GPS module</h5>
		<br>
<div>

<div>
Previous step: <a href="/blog/01-introduction/index.php">Arduino board</a>
<br>
<br>
</div>


<div>
To assemble the system you need a GPS module, e.g. NEO-7M and Arduino. The receiver, as shown, can be ordered on AliExpress for around 25 PLN.
</div>


<br>
<center>
<img src="/blog/03-gps-module/neo-7m.jpg" width="80%" height="80%" /><br>
<i>GPS NEO-7M with antena</i>
</center>
<br>

<div>We prepare the system as shown in the diagram below.</div>

<br>
<center>
<img src="/blog/03-gps-module/neo-7m-schema.png" width="80%" height="80%" /><br>
<i>Connections schema</i>
</center>
<br>

<br>
<center>
<img src="/blog/03-gps-module/neo-7m-connections-photo.jpg" width="80%" height="80%" /><br>
<i>Real connections</i>
</center>
<br>

<br>
<div>
Next, we can run the Arduino IDE and write a simple test program:
</div>
<br>
<?php include('source-gps-example1.html'); ?>
<br>

<div>
In line 3 we give the port numbers for communication with the GPS module.<br>
<br>
In line 15 we send all received data to a serial port connected to our PC. In this way we will be able to observe all messages on our PC.<br>
<br>
To view the received data in the IDE. We send the program to Arduino and turn on the serial port monitor selected from menu <i>Tools -> Serial Monitor</i>.
</div>


<br>
<center>
<img src="/blog/03-gps-module/serial-console.png" width="100%" height="100%" border="0" /><br>
<i>Preview NMEA messages on Serial Monitor</i>
</center>
<br>

<br>
<div>
We should see NMEA messages. At this point, it would be good to place our GPS module near the window, otherwise the module may have a problem with determining the GPS position. After a few minutes in the message strings we should see digits describing the latitude and longitude of our location.<br>
<br>
For us, the most interesting messages are GGA and GLL, which contain coordinate information.<br>
<br>
Example message GGA:
<b><pre>$GPGGA,182901.00,5105.90464,N,02052.40801,E,1,06,1.35,120.2,M,34.5,M,,*5C</pre></b>
decodes:<br>
<ul>
<li><b>182901.00</b> - mesaurement position UTC time: 18:29:01 UTC
<li><b>5105.90464,N</b> - latitude: 51°05.90464' N
<li><b>02052.40801,E</b> - longitude: 20°52.40801' E
<li><b>1</b> - fix quality: GPS fix (SPS)
<li><b>06</b> - number of satellites being tracked: 6
<li><b>1.35</b> - horizontal dilution of position
<li><b>120.2,M</b> - altitude, meters, above mean sea level
<li><b>34.5,M</b> - height of geoid above WGS84
<li><b>[empty field]</b> - not used
<li><b>*5C</b> - checksum data
</ul>
<br>
Message GLL is simplest:
<b><pre>$GPGLL,5105.90544,N,02052.40829,E,182903.00,A,A*60</pre></b>
decodes:<br>
<ul>
<li><b>5105.90544,N</b> - latitude: 51°05.90544' N
<li><b>02052.40829,E</b> - longitude: 20°52.40829' E
<li><b>182903.00</b> - mesaurement position UTC time: 18:29:03 UTC
<li><b>A</b> - valid data
<li><b>A*60</b> - checksum data
</ul>
<br>

Do we have to decode it ourselves? Of course not, we can use a ready-made module, e.g. TinyGPSPlus. For this purpose, we download a package with a plugin from <a href="https://github.com/mikalhart/TinyGPSPlus">https://github.com/mikalhart/TinyGPSPlus</a>, unpack in <i>Arduino/libraries</i> directory, restart the IDE and we can proceed to modify our program:<br>
<br>
<?php include('source-gps-example2.html'); ?>
<br>
<br>
Now our program will only display location updates in a decoded form.
</div>
<br>

<br>
<br>
<div>
Sources:
<ul>
<li><a href="https://gitlab.com/sebastian_dzk/pilnuj.com/-/blob/master/example/arduino/base/gps-example1/gps-example1.ino">gps-example1.ino</a>
<li><a href="https://gitlab.com/sebastian_dzk/pilnuj.com/-/blob/master/example/arduino/base/gps-example2/gps-example2.ino">gps-example2.ino</a>
</ul>
</div>
<div>
Links:
<ul>
<li><a href="https://www.u-blox.com/sites/default/files/products/documents/NEO-7_DataSheet_%28UBX-13003830%29.pdf">https://www.u-blox.com/sites/default/files/products/documents/NEO-7_DataSheet_(UBX-13003830).pdf</a>
<li><a href="https://en.wikipedia.org/wiki/NMEA_0183">https://en.wikipedia.org/wiki/NMEA_0183</a>
<li><a href="https://www.gpsinformation.org/dale/nmea.htm">https://www.gpsinformation.org/dale/nmea.htm</a>
<li><a href="https://github.com/mikalhart/TinyGPSPlus">https://github.com/mikalhart/TinyGPSPlus</a>
</ul>
</div>

<br>
<br>
<div>
Next step: <a href="/blog/04-1-gsm-module/index.php">GSM module, first run, sending and receiving SMS</a>
</div>
			</div>
			
		</div>
	</div>
</section>

