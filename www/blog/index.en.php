<section class="page-title bg-title overlay-dark">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<div class="title">
					<h3>DIY GPS tracker</h3>
					<p>How to build my own tracker?</p>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section about">
	<div class="container">
		<div class="row">
			<div class="col-lg-1 col-md-1 align-self-center">
				<div class="image-block bg-about">
					
				</div>
			</div>
			<div class="col-lg-10 col-md-10 align-self-center">
				<div class="content-block">
		
		<div>
		Below I present a series of short articles describing how to build your own GPS tracker.
		</div>		
		<br>
		<br>
		
		
<img src="https://pilnuj.com/blog/01-introduction/tracker-schema.png" />
		
<ul>
	<li><a href="01-introduction/index.php">How to build your own GPS/GSM tracker?</a>
	<li><a href="02-arduino/index.php">Arduino board</a>
	<li><a href="03-gps-module/index.php">GPS module, NMEA protocol</a>
	<li><a href="04-1-gsm-module/index.php">GSM module, first run, sending and receiving SMS</a>
	<li><a href="04-2-gsm-module/index.php">GSM module, GPRS mode and HTTP requests</a>
	<li><a href="05-complete-device/index.php">Connection of all modules into the whole device</a>
	<li><a href="06-rest-comminucation-with-pilnuj.com/index.php">Communication via REST protocol with pilnuj.com</a>

	<li><a href="50-rest-example/index.php">Example REST server based on SpringBoot</a>
</ul>

					
				</div>
			</div>
			
		</div>
	</div>
</section>

