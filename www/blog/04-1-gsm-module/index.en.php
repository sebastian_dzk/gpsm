<section class="page-title bg-title overlay-dark">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<div class="title">
					<h3>DIY GPS tracker</h3>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section about">
	<div class="container">
		<div class="row">
			<div class="col-lg-1 col-md-1 align-self-center">
				<div class="bg-about">
					
				</div>
			</div>
			<div class="col-lg-10 col-md-10 align-self-center">
				<div class="content-block">
		<h5>GSM module, first run, sending and receiving SMS</h5>
		<br>
<div>

<div>
Previous step: <a href="/blog/03-gps-module/index.php">GPS module, NMEA protocol</a>
<br>
<br>
</div>


For GSM communication we need:
<ul>
<li>GSM module, e.g. SIM800L, which can be purchased for 35-45 PLN
<li>SIM cards with login without PIN
<li>Arduino acting as a controller
</ul>
<br>

<br>
<center>
<img src="/blog/04-1-gsm-module/gsm-module.jpg" width="80%" height="80%" /><br>
<i>GSM SIM800L module</i>
</center>
<br>

<br>
<center>
<img src="/blog/04-1-gsm-module/arduino-uno.jpg" width="80%" height="80%" /><br>
<i>Arduino Uno</i>
</center>
<br>

First, make sure your SIM card has a disabled PIN and is active. It would be worth checking on your phone whether you can send SMS or GPRS communication. Only if everything is OK do we insert the SIM card into our GPS module. You should carefully look at the way of inserting the card, because in the modules I know, the cut out corner of the card should protrude slightly from the phone, it is unintuitive, and the card easily enters the wrong way. The board have drawings showing how to insert a card.<br>
<br>

We set the system as below. It is very important to power the GSM module from an efficient current source, minimum 2A.<br>
<br>


<br>
<center>
<img src="/blog/04-1-gsm-module/gsm-schema.png" width="80%" height="80%" /><br>
<i>GSM module connections schema</i>
</center>
<br>

<br>
<center>
<img src="/blog/04-1-gsm-module/gsm-real-photo.jpg" width="80%" height="80%" /><br>
<i>GSM module real connection photo</i>
</center>
<br>

<br>
The GSM module has two LEDs:<br>
<br>
<b>D2 RING</b>
<ul>
     <li>lights up but goes out once every few seconds - incorrect power supply or insufficient power source
     <li>lights constantly - power supply approx
     </ul>
     
<b>D6 NET</b>
<ul>
     <li>flashing every 1 second - network search
     <li>flashing once every 3 seconds - the card is connected to the GSM network
     <li>very fast flashing - GPRS mode
</ul>

<br>
<br>
An example of the simplest program that sends an SMS:<br>
<br>
<?php include('source-gsm-example1.html'); ?>



<br>
<br>
Example of a code receiving SMSs:<br>
<br>
<?php include('source-gsm-example2.html'); ?>



<br>
<br>
<div>
Sources:
<ul>
<li><a href="https://gitlab.com/sebastian_dzk/pilnuj.com/-/blob/master/example/arduino/base/gsm-example1/gsm-example1.ino">gsm-example1.ino</a>
<li><a href="https://gitlab.com/sebastian_dzk/pilnuj.com/-/blob/master/example/arduino/base/gsm-example2/gsm-example2.ino">gsm-example2.ino</a>
</ul>
</div>
<div>
Links:
<ul>
<li><a href="https://img.filipeflop.com/files/download/Datasheet_SIM800L.pdf">https://img.filipeflop.com/files/download/Datasheet_SIM800L.pdf</a>
<li><a href="https://steemit.com/utopian-io/@koroglu/control-led-using-sim800l-sim-card-module">https://steemit.com/utopian-io/@koroglu/control-led-using-sim800l-sim-card-module</a>
<li><a href="https://www.teachmemicro.com/sim800-network-test-troubleshoot/">https://www.teachmemicro.com/sim800-network-test-troubleshoot/</a>
<li><a href="http://www.circuitstoday.com/interface-gsm-module-with-arduino">http://www.circuitstoday.com/interface-gsm-module-with-arduino</a>
</ul>
</div>

<br>
<br>
<div>
Next step: <a href="/blog/04-2-gsm-module/index.php">GSM module, GPRS mode and HTTP requests</a>
</div>
			</div>
			
		</div>
	</div>
</section>

