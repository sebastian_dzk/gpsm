<section class="page-title bg-title overlay-dark">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<div class="title">
					<h3>DIY GPS tracker</h3>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section about">
	<div class="container">
		<div class="row">
			<div class="col-lg-1 col-md-1 align-self-center">
				<div class="bg-about">
					
				</div>
			</div>
			<div class="col-lg-10 col-md-10 align-self-center">
				<div class="content-block">
		<h5>Example REST server based on SpringBoot</h5>
		<br>
<div>

<!--
<div>
Previous step: <a href="/blog//index.php"></a>
<br>
<br>
</div>
-->


How to create your own REST server? The choice of technology is huge, I will present it based on SpringBoot.<br>
<br>

At the beginning we need:
<ul>
<li><a href="https://www.oracle.com/java/technologies/javase-jdk8-downloads.html">JDK8</a> - it can also be OpenJDK, it's important that in version 8, Oracle a little bit of trouble in the next incompatible versions
<li>text editor or entire IDE. e.g. <a href="https://www.jetbrains.com/idea/download/">Intellij</a>, <a href="https://www.eclipse.org/">Eclipse</a>, <a href="https://netbeans.org/">NetBeans</a> or <a href="https://www.vim.org/">Vim</a> for console fans;)
<li><a href="https://www.postman.com/downloads/">Postman</a> or another tool that lets you call REST methods more complex than GET
</ul>
<a href="https://git-scm.com/downloads/guis">Git</a> can be useful for downloading examples from the repository.<br>
<br>

How will we start? We go to <a href="https://start.spring.io/">https://start.spring.io/</a> and in a few steps we will generate a skeleton of a working application.<br>
<br>
Check the options:<br>
Project: <i>Gradle Project</i><br>
change <i>Group</i> to our own package, e.g. <i>com.pilnuj.examples</i><br>
change <i>Artifact</i> to <i>rest</i><br>
<br>
In the <i>Dependencies</i> section, click <i>Add ...</i>, enter <i>REST</i> and choose <i>Spring Web</i> from the selected modules.<br>
<br>


<br>
<center>
<img src="/blog/50-rest-example/start-spring.png" width="80%" height="80%" /><br>
</center>
<br>


We should see something similar to the picture above. Click <i>Generate</i> and download the generated application skeleton.<br>
<br>

from the command line we build and run a project on Windows:<br>
<br>
<!-- HTML generated using hilite.me --><div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%">gradlew bootRun
</pre></div>
<br>
or on Linux:<br>
<br>
<!-- HTML generated using hilite.me --><div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%">./gradlew bootRun
</pre></div>
<br>

If everything went well, we should see something similar:<br>
<center>
<img src="/blog/50-rest-example/application-first-log.png" width="80%" height="80%" /><br>
</center>
<br>


Next, open browser and enter the address <a href="http://localhost:8080">http://localhost:8080</a><br>
We should see the page with the message <i>Whitelabel Error Page</i>, it is a sign that everything is working properly:<br>
<br>
<center>
<img src="/blog/50-rest-example/browser-first-page.png" width="80%" height="80%" /><br>
</center>
<br>

In subdirectory <i>src/main/java/com/pilnuj/examples/server/rest/controller/</i> add controller <i>ExampleController.java</i> with the GET method returning server current time:<br>
<br>
<?php include('source-1.html'); ?>
<br>

We rebuild and launch the application again. Enter <a href="http://localhost:8080/time">http://localhost:8080/time</a> in the browser.
If everything went well, we should see in the browser the date with the time:<br>
<br>
<center>
<img src="/blog/50-rest-example/browser-time.png" width="80%" height="80%" /><br>
<i></i>
</center>
<br>


Now we will prepare a more complex method, which we will be able to send data to the server. We will add a method to our <i>ExampleController.java</i> controller:<br>
<br>
<?php include('source-2.html'); ?>
<br>
and add DTO object reperenting the received data:<br>
<br>
<?php include('source-3.html'); ?>
<br>

We are rebuilding and launching our application again. Now we need to use Postman for the test, set the POST method, address <a href="http://localhost:8080/add">http://localhost:8080/add</a>, set the data type to JSON and select raw, as the data we insert JSON:<br>
<br>
<?php include('source-4.html'); ?>
<br>

Click Send and we should receive a response with the status 201:<br>
<br>
<center>
<img src="/blog/50-rest-example/postman.png" width="100%" height="100%" /><br>
<i></i>
</center>
<br>

On SpringBoot console should display information with the data we have sent:<br>
<br>
<!-- HTML generated using hilite.me --><div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%">LocationDto<span style="color: #333333">{</span><span style="color: #996633">imei</span><span style="color: #333333">=</span><span style="background-color: #fff0f0">&#39;123456789012345&#39;</span>, <span style="color: #996633">lat</span><span style="color: #333333">=</span>52.123, <span style="color: #996633">lng</span><span style="color: #333333">=</span>21.345<span style="color: #333333">}</span>
</pre></div>

<br>
<center>
<img src="/blog/50-rest-example/application-log-with-position.png" width="80%" height="80%" /><br>
<i></i>
</center>
<br>
Our server does not save received information. At the moment, this is not important.




<br>
<br>
<div>
Sources:
<ul>
<li><a href="https://gitlab.com/sebastian_dzk/pilnuj.com/-/tree/master/example/server-side/server.rest">https://gitlab.com/sebastian_dzk/pilnuj.com/-/tree/master/example/server-side/server.rest</a>
</ul>
</div>
<div>
Links:
<ul>
<li><a href="https://www.oracle.com/java/technologies/javase-jdk8-downloads.html">https://www.oracle.com/java/technologies/javase-jdk8-downloads.html</a>
<li><a href="https://gradle.org/">https://gradle.org/</a>
<li><a href="https://start.spring.io/">https://start.spring.io/</a>
<li><a href="https://spring.io/guides/gs/spring-boot/">https://spring.io/guides/gs/spring-boot/</a>
<li><a href="https://www.codecademy.com/articles/what-is-rest">https://www.codecademy.com/articles/what-is-rest</a>
<li><a href="https://www.postman.com/downloads/">https://www.postman.com/downloads/</a>
<li><a href="https://git-scm.com/downloads/guis">https://git-scm.com/downloads/guis</a>
<li><a href="https://www.eclipse.org/">https://www.eclipse.org/</a>
<li><a href="https://netbeans.org/">https://netbeans.org/</a>
<li><a href="https://www.jetbrains.com/idea/download/">https://www.jetbrains.com/idea/download/</a>
<li><a href="https://www.vim.org/">https://www.vim.org/</a>
</ul>
</div>

<br>
<br>

<!--
<div>
Next step: <a href="/blog//index.php"></a>
</div>
-->


			</div>
			
		</div>
	</div>
</section>

