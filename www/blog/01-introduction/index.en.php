<section class="page-title bg-title overlay-dark">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<div class="title">
					<h3>DIY GPS tracker</h3>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section about">
	<div class="container">
		<div class="row">
			<div class="col-lg-1 col-md-1 align-self-center">
				<div class="image-block bg-about">
					
				</div>
			</div>
			<div class="col-lg-10 col-md-10 align-self-center">
				<div class="content-block">
		
		<h5>How to build your own GPS tracker?</h5>
		<br>
		
		<div>
		Contrary to appearances, building your own GPS locator is not particularly difficult. What I'm going to show on this blog.
		</div>
		
		<div>
		The tracker will consist of three basic modules:
		<ul>
			<li><b>GPS module</b> - that will decode GPS satellites signal to readable NMEA format
			<li><b>GSM module</b> - which will use us to communication via GPRS to the Internet and send location information to server. SMS communication is also possible.
			<li><b>Arduino Uno</b> - controller in which we implement logic connecting GPS, GSM and controlling communication with the geolocation server. For our purposes I will use the most popular and best documented Arduino Uno platform. We can use something smaller, e.g. Arduino Nano.
		</ul>
		Our locator will also need some power supply system but we'll leave it for dessert.
		</div>
		
		
		
<div>
<br>
<center>
<img src="https://pilnuj.com/blog/01-introduction/tracker-schema.png" /><br>
<i>Tracker scheme</i>
</center>
<br>
</div>

<div>
<br>
<center>
<img src="https://pilnuj.com/blog/01-introduction/GSM-NEO.jpg" width="60%" height="60%" /><br>
<i>NEO-7M GPS module with antena</i>
</center>
<br>
</div>


<div>
<br>
<center>
<img src="https://pilnuj.com/blog/01-introduction/SIM800L.jpg" width="60%" height="60%" /><br>
<i>SIM800L GSM module with antena</i>
</center>
<br>
</div>

<div>
<br>
<center>
<img src="https://pilnuj.com/blog/01-introduction/ArduinoUno.jpg" width="60%" height="60%" /><br>
<i>Arduino Uno, our central computer :)</i>
</center>
<br>
</div>


<br>
<br>
<br>
<div>
</div>

<br>
<br>
<div>
Links:
<ul>
<li>NEO-7M GPS module - <a href="https://www.u-blox.com/sites/default/files/products/documents/NEO-7_DataSheet_%28UBX-13003830%29.pdf">https://www.u-blox.com/sites/default/files/products/documents/NEO-7_DataSheet_%28UBX-13003830%29.pdf</a>
<li>SIM800L GSM module - <a href="https://img.filipeflop.com/files/download/Datasheet_SIM800L.pdf">https://img.filipeflop.com/files/download/Datasheet_SIM800L.pdf</a>
<li>Arduino Uno - <a href="https://store.arduino.cc/arduino-uno-rev3">https://store.arduino.cc/arduino-uno-rev3</a>
</ul>

<br>
<br>
Next step: <a href="/blog/02-arduino/index.php">Arduino board</a>

</div>



</div>
			</div>
			
		</div>
	</div>
</section>

