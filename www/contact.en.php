<section class="page-title bg-title overlay-dark">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<div class="title">
					<h3>Contact</h3>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section contact-form">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="section-title">
					<h3><span class="alternate">Send to me</span> message</h3>
					<p>You need a new functionality, you have an idea for a website, maybe something doesn't work as expected, you see an error? Do you want to cooperate? Write to me.
                                           I will not leave any email without an answer.</p>
					<div class="title" style="margin-top: 40px">
						<h5><i class="fa fa-envelope"> <a href="mailto:sebastian.dziak@gmail.com">sebastian.dziak@gmail.com</a></i></h5>
						<h5><i class="fa fa-skype"> <a href="skype:sebastian_dzk?call">sebastian_dzk</a></i></h5>				
					</div>
				</div>				
			</div>
		</div>
	</div>
</section>

