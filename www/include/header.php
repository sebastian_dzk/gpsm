<?php include_once(dirname(__FILE__).'/lang.php'); ?>

<html>
<head>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-140434302-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-140434302-1');
</script>

  <!-- SITE TITTLE -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="keywords" content="gps, monitoring, lokalizator, tracker, free, darmowy, gps tracker, open api, api, tk-102, tk-103. tk102, tk103">
  <title>pilnuj.com</title>
  
  <!-- PLUGINS CSS STYLE -->
  <!-- Bootstrap -->
  <link href="/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Font Awesome -->
  <link href="/plugins/font-awsome/css/font-awesome.min.css" rel="stylesheet">
  <!-- Magnific Popup -->
  <link href="/plugins/magnific-popup/magnific-popup.css" rel="stylesheet">
  <!-- Slick Carousel -->
  <link href="/plugins/slick/slick.css" rel="stylesheet">
  <link href="/plugins/slick/slick-theme.css" rel="stylesheet">
  <!-- CUSTOM CSS -->
  <link href="/css/style.css" rel="stylesheet">

  <!-- flags -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.1.0/css/flag-icon.min.css" rel="stylesheet">

  <!-- FAVICON -->
  <link href="/images/favicon.png" rel="shortcut icon">

  <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/Wruczek/Bootstrap-Cookie-Alert@gh-pages/cookiealert.css">

</head>
<body>

<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/pl_PL/sdk.js#xfbml=1&version=v7.0&appId=476909766032896&autoLogAppEvents=1"></script>

<?php include '/var/www/html/include/menu.php';?>

<div class="alert text-center cookiealert" role="alert">Używamy plików cookie, aby zapewnić Ci najlepszą jakość na naszej stronie internetowej. Czy zgadzasz się na użycie cookies?
    <button type="button" class="btn btn-primary btn-sm acceptcookies" aria-label="Close">
        Zgadzam się
    </button>
</div>

