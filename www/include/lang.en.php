<?php
$language['MENU.HOME'] = 'Home';
$language['MENU.SYSTEM'] = 'System';
$language['MENU.REGULATIONS'] = 'Regulations';
$language['MENU.DOWNLOAD_GPS_TX'] = 'Download GPS-Tx';
$language['MENU.CONTACT'] = 'Contact';

$language['MENU.LOGIN'] = 'Login ';
$language['MENU.REGISTER'] = 'Register';

$language['FOOTER.PRIVACY_POLICY'] = 'Privacy policy';
?>
