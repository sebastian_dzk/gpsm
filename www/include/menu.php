<nav class="navbar main-nav border-less fixed-top navbar-expand-lg p-0">

  <a class="navbar-brand" href="index.php">
    <img src="/images/logo.png" alt="logo">
  </a>

  <div class="container-fluid p-0">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="fa fa-bars"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav mx-auto">
        <li class="nav-item">
          <a class="nav-link" href="/index.php"><?php echo $language['MENU.HOME']; ?></a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="/system.php"><?php echo $language['MENU.SYSTEM']; ?></a>
        </li>

	    <li class="nav-item">
          <a class="nav-link" href="https://play.google.com/store/apps/details?id=com.pilnuj.android.gpstx&gl=PL"><?php echo $language['MENU.DOWNLOAD_GPS_TX']; ?></a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="https://pilnuj.com/app/swagger-ui.html">Open API</a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="/blog/index.php">DIY GPS tracker</a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="/contact.php"><?php echo $language['MENU.CONTACT']; ?></a>
        </li>

        <li>
          <button class="btn flag-icon flag-icon-pl" style="margin-right: 5px; height: 20px" onclick="window.location.href='<?php echo 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']; ?>?lang=pl';"></button>
        </li>
        
        <li>
          <button class="btn flag-icon flag-icon-gb" style="margin-right: 20px; height: 20px" onclick="window.location.href='<?php echo 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']; ?>?lang=en';"></button>
        </li>
      </ul>

<div class="fb-like" data-href="https://pilnuj.com" data-width="" data-layout="box_count" data-action="like" data-size="large" data-share="true" style="margin-right: 10px" ></div>

      <a style="width: 190px" href="https://pilnuj.com/auth/realms/pilnuj.com/protocol/openid-connect/registrations?response_type=code&client_id=application-cli&redirect_uri=https%3A%2F%2Fpilnuj.com" class="ticket">        
        <span><?php echo $language['MENU.REGISTER']; ?></span>
      </a>
      <a style="width: 190px" href="https://pilnuj.com/web-fo/" class="ticket">      
        <span><?php echo $language['MENU.LOGIN']; ?></span>
      </a>

      </div>
  </div>
</nav>

