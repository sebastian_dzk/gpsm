<?php
$language['MENU.HOME'] = 'Home';
$language['MENU.SYSTEM'] = 'System';
$language['MENU.REGULATIONS'] = 'Regulamin';
$language['MENU.DOWNLOAD_GPS_TX'] = 'Pobierz GPS-Tx';
$language['MENU.CONTACT'] = 'Kontakt';

$language['MENU.LOGIN'] = 'Logowanie';
$language['MENU.REGISTER'] = 'Rejestracja';

$language['FOOTER.PRIVACY_POLICY'] = 'Polityka prywatności';
?>
