<footer class="footer-main">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="block text-center">
            <ul class="social-links-footer list-inline">                      
              <li class="list-inline-item">
                <a href="https://www.facebook.com/Pilnujcom-113191266797190/"><i class="fa fa-facebook"></i></a>
              </li>
              <li class="list-inline-item">
                <a href="https://www.youtube.com/channel/UCjjldvlLeJa_mbLW6sBdsFw"><i class="fa fa-youtube-square"></i></a>
              </li>
            </ul>
          </div>          
        </div>
      </div>
    </div>
</footer>
<!-- Subfooter -->
<footer class="subfooter">
  <div class="container">
    <div class="row">
      <div class="col-md-6 align-self-center">
        <div class="copyright-text">
          <p style="color: #eeeeee">&#169; 2019-2020 All Right Reserved | <a href="https://pilnuj.com/privacy-policy.php"><?php echo $language['FOOTER.PRIVACY_POLICY']; ?></a></p>
        </div>
      </div>
      <div class="col-md-6">
          <a href="#" class="to-top"><i class="fa fa-angle-up"></i></a>
      </div>
    </div>
  </div>
</footer>

  <!-- JAVASCRIPTS -->
  <!-- jQuey -->
  <script src="/plugins/jquery/jquery.js"></script>
  <!-- Popper js -->
  <script src="/plugins/popper/popper.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="/plugins/bootstrap/js/bootstrap.min.js"></script>
  <!-- Smooth Scroll -->
  <script src="/plugins/smoothscroll/SmoothScroll.min.js"></script>  
  <!-- Isotope -->
  <script src="/plugins/isotope/mixitup.min.js"></script>  
  <!-- Magnific Popup -->
  <script src="/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
  <!-- Slick Carousel -->
  <script src="/plugins/slick/slick.min.js"></script>  
  <!-- SyoTimer -->
  <script src="/plugins/syotimer/jquery.syotimer.min.js"></script>
  <!-- Google Mapl -->
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCC72vZw-6tGqFyRhhg5CkF2fqfILn2Tsw"></script>
  <script type="text/javascript" src="/plugins/google-map/gmap.js"></script>
  <!-- Custom Script -->
  <script src="/js/custom.js"></script>

  <script src="https://cdn.jsdelivr.net/gh/Wruczek/Bootstrap-Cookie-Alert@gh-pages/cookiealert.js"></script>

</body>
</html>

