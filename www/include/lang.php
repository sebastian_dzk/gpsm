<?php

$lang = $_REQUEST['lang'];
if(isset($lang)) {
   setcookie('lang', $lang);
}

include_once(dirname(__FILE__).'/lang.'.lang().'.php'); 

function lang() {
  global $lang;    

  if(isset($lang)) {
    return $lang;
  }

  if(isset($_COOKIE['lang'])) {
     return $_COOKIE['lang'];
  }

  if(isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
     $tmpLang = strtolower($_SERVER['HTTP_ACCEPT_LANGUAGE']);
     if($tmpLang != 'pl' && $tmpLang != 'en') {
        return 'en';
     }

     return $_SERVER['HTTP_ACCEPT_LANGUAGE'];
  }

  return 'en';
}

