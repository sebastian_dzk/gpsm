<section class="page-title bg-title overlay-dark">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<div class="title">
					<h3>Strona główna</h3>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section pricing">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="section-title">
					<h3><span class="alternate">Obsługiwane</span> urządzenia,</h3>
                                        <p>których pozycje możesz monitorować</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-6">
				<!-- Pricing Item -->
				<div class="pricing-item featured">
					<div class="pricing-heading">
						<!-- Title -->
						<div class="title">
							<h6>Android OS</h6>
						</div>
                                                <div style="color: white">Urządzenia z systemem Android posiadające moduł GPS i możliwość komunikacji przez Internet, czyli prawie wszystkie współczesne smartfony i tablety.</div>
					</div>
					<div class="pricing-body">
						<!-- Feature List -->
						<ul class="feature-list m-0 p-0">
<li><p><span class="fa fa-check-circle available"></span><a href="https://pilnuj.com/auth/realms/pilnuj.com/protocol/openid-connect/registrations?response_type=code&client_id=application-cli&redirect_uri=https%3A%2F%2Fpilnuj.com">Zarejestruj się</a></p>
<li><p><span class="fa fa-check-circle available"></span>Pobierz darmową aplikację <a href="https://play.google.com/store/apps/details?id=com.pilnuj.android.gpstx&gl=PL">GPS-Tx</a> na Android OS</p>
<li><p><span class="fa fa-check-circle available"></span>Na urządzeniu z zainstalowana aplikacją przejdź do "Ustawienia > Bateria > Uruchamianie" i dla aplikacji "GPS-Tx" włącz "Zarządzanie ręczne" aplikacją</p>
<li><p><span class="fa fa-check-circle available"></span>Uruchom GPS-Tx, podaj login i hasło użyte podczas rejestracji w serwisie. Następnie kliknij przycisk "WŁĄCZ ŚLEDZENIE GPS"</p>
<li><p><span class="fa fa-check-circle available"></span>Przejdź do serwisu <a href="https://pilnuj.com/web-fo/#/map">pilnuj.com</a> i po zalogowaniu monitoruj położenie urządzenia</p>	
					</ul>
					</div>					
				</div>
			</div>
			<div class="col-6">
				<!-- Pricing Item -->
				<div class="pricing-item featured">
					<div class="pricing-heading">
						<!-- Title -->
						<div class="title">
							<h6>Trackery TK-102, TK-103 i kompatybilne</h6>
                                                </div>
                                                <div style="color: white">Popularne trackery z serii Tk-102 i Tk-103 wspierające tryb pracy GPRS.<br><br></div>
					</div>
					<div class="pricing-body">
						<!-- Feature List -->
						<ul class="feature-list m-0 p-0">
							<li><p><span class="fa fa-check-circle available"></span><a href="https://pilnuj.com/auth/realms/pilnuj.com/protocol/openid-connect/registrations?response_type=code&client_id=application-cli&redirect_uri=https%3A%2F%2Fpilnuj.com">Zarejestruj się</a></p>
<li><p><span class="fa fa-check-circle available"></span>We własnym zakresie kup i włóż kartę SIM do trackera</p>
<li><p><span class="fa fa-check-circle available"></span>Przejdź do <a href="https://pilnuj.com/web-fo/#/device">https://pilnuj.com/web-fo/#/device</a> i dodaj urządzenie</p>
<li><p><span class="fa fa-check-circle available"></span>Wyślij zestaw komend na numer karty SIM, za każdym razem czekając na potwierdzeniem:
<ul>
<li><b>begin123456</b> - polecenie begin123456 (gdzie 123456 jest domyślnym hasłem) 
<li><b>apn123456 internet</b> - parametr zależy od operatora karty SIM, mogą być wymagane dodatkowe parametry 
<li><b>adminip123456 51.68.139.189 5002</b> 
<li><b>gprs123456</b> 
<li><b>fix030s***n123456</b> 
</ul>
</p>
<li><p><span class="fa fa-check-circle available"></span>Przejdź do serwisu <a href="https://pilnuj.com/web-fo/#/map">pilnuj.com</a> i po zalogowaniu monitoruj położenie urządzenia</p>
						</ul>
					</div>					
				</div>
			</div>
		</div>
	</div>
</section>

