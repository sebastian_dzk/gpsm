package com.sulacosoft.keycloak;


import com.sulacosoft.keycloak.adapter.DeviceStatusJson;
import com.sulacosoft.keycloak.adapter.GpsDeviceService;
import com.sulacosoft.keycloak.auth.KeycloakConfig;
import com.sulacosoft.keycloak.auth.RestApiFactory;

public class Main {

    public static void main(String args[]) throws Exception {
        System.out.println("### start keycloak cxf rest api auth test ###");

        KeycloakConfig cfg = new KeycloakConfig();
        cfg.setUrl("https://pilnuj.com:8443/");
        cfg.setRealm("gpsm");
        cfg.setClientId("gpsm-apk2");

        String login = "seba";
        String password = "dksd1234";

        GpsDeviceService api = RestApiFactory.create("https://pilnuj.com/app", GpsDeviceService.class, cfg, login, password);

        long t0 = System.currentTimeMillis();

        while (true) {
            long t1 = System.currentTimeMillis();
            DeviceStatusJson status = api.deviceStatus("222");

            long dt = Math.round(0.001 * (t1 - t0));

            System.out.println("# dt = " + (dt) + " sek; status : " + status);
            Thread.sleep(10000);
        }
    }
}
