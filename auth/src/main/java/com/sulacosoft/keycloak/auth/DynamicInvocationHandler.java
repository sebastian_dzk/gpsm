package com.sulacosoft.keycloak.auth;

import org.apache.cxf.jaxrs.client.Client;
import org.apache.cxf.jaxrs.client.WebClient;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class DynamicInvocationHandler<T> implements InvocationHandler {

    private T api;
    private KeycloakConfig config;
    private String refreshToken;

    public DynamicInvocationHandler(T api, KeycloakConfig config, String refreshToken) {
        this.api = api;
        this.config = config;
        this.refreshToken = refreshToken;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args)
            throws Throwable {
        try {
            return method.invoke(api, args);
        } catch (java.lang.reflect.InvocationTargetException e) {
            if (javax.ws.rs.NotAuthorizedException.class == e.getTargetException().getClass()) {
                System.out.println("Mamy blad bezpieczenstwa i odświeżamy token!");
                System.out.println(refreshToken);
                String authToken = KeycloakUtils.resfreshToken(config, refreshToken).getAccessToken();
                Client client = WebClient.client(api);
                client.reset();
                client.header("Authorization", "Bearer " + authToken);
                return method.invoke(api, args);
            }
            throw e;
        }
    }
}
