package com.sulacosoft.keycloak.auth;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sulacosoft.keycloak.auth.dto.KeycloakTokenResponseDto;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;

public class KeycloakUtils {

    public static KeycloakTokenResponseDto authenticate(KeycloakConfig config, String login, String password) throws Exception {
        String url = String.format("%sauth/realms/%s/protocol/openid-connect/token", config.getUrl(), config.getRealm());
        String data = String.format("username=%s&password=%s&grant_type=password&client_id=%s&grant_type=%s", login, password, config.getClientId(), config.getGrantType());
        String response = sendPost(url, data);
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(response, KeycloakTokenResponseDto.class);
    }

    public static KeycloakTokenResponseDto resfreshToken(KeycloakConfig config, String refreshToken) throws Exception {
        String url = String.format("%sauth/realms/%s/protocol/openid-connect/token", config.getUrl(), config.getRealm());
        String data = String.format("grant_type=refresh_token&client_id=%s&refresh_token=%s", config.getClientId(), refreshToken);
        String response = sendPost(url, data);
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(response, KeycloakTokenResponseDto.class);
    }

    private static String sendPost(String url, String data) throws Exception {
        System.out.println("URL : " + url);
        System.out.println(url);
        System.out.println(data);
        URL obj = new URL(url);
        HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());

        if (data != null)
            wr.writeBytes(data);

        wr.flush();
        wr.close();

        // int responseCode = con.getResponseCode();
        //    System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        //System.out.println(response);
        in.close();

        return response.toString();
    }
}
