package com.sulacosoft.keycloak.auth;


import com.sulacosoft.keycloak.auth.dto.KeycloakTokenResponseDto;
import org.apache.cxf.jaxrs.client.Client;
import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.apache.cxf.jaxrs.client.WebClient;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.util.Collections;


public class RestApiFactory<T> {

    public static <T> T create(String apiUrl, Class clazz, KeycloakConfig config, String login, String password) throws Exception {
        KeycloakTokenResponseDto resp = KeycloakUtils.authenticate(config, login, password);
        //  KeycloakTokenResponseDto resp2 = KeycloakUtils.resfreshToken(config, resp.getRefreshToken()); //
        T api = (T) JAXRSClientFactory.create(apiUrl, clazz, Collections.singletonList(new CustomProvider()));
        Client client = WebClient.client(api);
        client.header("Authorization", "Bearer " + resp.getAccessToken());
        return createProxy(api, clazz, config, resp.getRefreshToken());
    }

    private static <T> T createProxy(T api, Class clazz, KeycloakConfig config, String refreshToken) {
        InvocationHandler handler = new DynamicInvocationHandler(api, config, refreshToken);
        return (T) Proxy.newProxyInstance(RestApiFactory.class.getClassLoader(), new Class[]{clazz}, handler);
    }
}
