package com.sulacosoft.keycloak.adapter;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;



import java.security.Principal;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.UUID;

//import javax.inject.Inject;
//import javax.persistence.EntityManager;
//import javax.persistence.Query;
//import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.keycloak.KeycloakSecurityContext;
import org.keycloak.authorization.client.AuthzClient;
import org.keycloak.authorization.client.ClientAuthorizationContext;
import org.keycloak.authorization.client.resource.ProtectionResource;
//import org.keycloak.example.photoz.ErrorResponse;
//import org.keycloak.example.photoz.entity.Album;
//import org.keycloak.example.photoz.util.Transaction;
import org.keycloak.representations.idm.authorization.PermissionTicketRepresentation;
import org.keycloak.representations.idm.authorization.ResourceRepresentation;
import org.keycloak.representations.idm.authorization.ScopeRepresentation;


@Path("api/device")
@Produces({MediaType.APPLICATION_JSON})
public interface GpsDeviceService {

    @GET
    @Path("{deviceUid}/status")
    DeviceStatusJson deviceStatus(@PathParam(value = "deviceUid") String deviceUid);
}
