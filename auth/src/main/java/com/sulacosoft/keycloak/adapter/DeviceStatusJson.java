package com.sulacosoft.keycloak.adapter;

public class DeviceStatusJson {

    private boolean deviceExist;
    private boolean alarmEnabled;

    public boolean isDeviceExist() {
        return deviceExist;
    }

    public void setDeviceExist(boolean deviceExist) {
        this.deviceExist = deviceExist;
    }

    public boolean isAlarmEnabled() {
        return alarmEnabled;
    }

    public void setAlarmEnabled(boolean alarmEnabled) {
        this.alarmEnabled = alarmEnabled;
    }

    @Override
    public String toString() {
        return "DeviceStatusJson{" +
                "deviceExist=" + deviceExist +
                ", alarmEnabled=" + alarmEnabled +
                '}';
    }
}
